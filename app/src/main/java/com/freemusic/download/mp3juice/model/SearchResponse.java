package com.freemusic.download.mp3juice.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class SearchResponse {

    @SerializedName("results")
    private List<MediaItem> results;

    @SerializedName("status")
    private int status;

    public List<MediaItem> getResults() {
        return results;
    }

    public void setResults(List<MediaItem> results) {
        this.results = results;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "ResponseMedia{" +
                        "results = '" + results + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}