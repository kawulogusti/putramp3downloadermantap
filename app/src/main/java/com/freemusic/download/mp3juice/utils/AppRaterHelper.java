package com.freemusic.download.mp3juice.utils;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;

import com.freemusic.download.mp3juice.activity.MainActivity;
import com.freemusic.download.mp3juice.api.ConfApp;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;


public class AppRaterHelper {

    private final static int DAYS_UNTIL_PROMPT = 1;//Min number of days
    private final static int LAUNCHES_UNTIL_PROMPT = 2;//Min number of launches

    public static void app_launched(Context mContext) {


        SharedPreferences prefs = mContext.getSharedPreferences("apprater", 0);
        if (prefs.getBoolean("dontshowagain", false)) {
            return;
        }

        SharedPreferences.Editor editor = prefs.edit();

        // Increment launch counter
        long launch_count = prefs.getLong("launch_count", 0) + 1;
        editor.putLong("launch_count", launch_count);

        // Get date of first launch
        Long date_firstLaunch = prefs.getLong("date_firstlaunch", 0);
        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong("date_firstlaunch", date_firstLaunch);
        }

        // Wait at least n days before opening
        if (launch_count >= LAUNCHES_UNTIL_PROMPT) {
            if (System.currentTimeMillis() >= date_firstLaunch +
                    (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000)) {
                showRateDialog(mContext);
            }
        }

        //showRateDialog(mContext, editor);

        editor.commit();
    }

    public static void showForceDialog(final Context mContext, final ConfApp confApp) {
        MaterialAlertDialogBuilder builderSingle = new MaterialAlertDialogBuilder(mContext);
        builderSingle.setCancelable(false);
        builderSingle.setTitle("Info");
        builderSingle.setMessage(confApp.getForce_dialog_message());
        builderSingle.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String urlApp = "http://play.google.com/store/apps/details?id=" + confApp.getNew_url();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlApp));
                mContext.startActivity(browserIntent);
            }
        });

        builderSingle.show();

    }

    public static void showRateDialog(final Context mContext) {
        MaterialAlertDialogBuilder builderSingle = new MaterialAlertDialogBuilder(mContext);
        builderSingle.setCancelable(true);
        builderSingle.setTitle("Rate App ⭐⭐⭐⭐⭐");
        try {
            PackageInfo pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            String version = pInfo.versionName;
            builderSingle.setMessage("Thanks for using our app!\nWould you like to rate our app?");
        } catch (PackageManager.NameNotFoundException e) {

        }


        builderSingle.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String urlApp = "http://play.google.com/store/apps/details?id=" + mContext.getPackageName();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlApp));
                mContext.startActivity(browserIntent);
            }
        });

        builderSingle.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        /*builderSingle.setNeutralButton("Dont show again", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (editor != null) {
                    editor.putBoolean("dontshowagain", true);
                    editor.commit();
                }
                dialog.dismiss();
            }
        });*/


        builderSingle.show();

    }

    /*public static void showForceDialog(final Context mContext, ConfApp confApp) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(mContext);
        builderSingle.setCancelable(false);
        builderSingle.setTitle("Info");
        builderSingle.setMessage(confApp.getForce_dialog_message());
        builderSingle.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String urlApp = "http://play.google.com/store/apps/details?id=" + confApp.getNew_url();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlApp));
                mContext.startActivity(browserIntent);
            }
        });

        builderSingle.show();

    }*/

    public static void showRestartDialog(final Context mContext, int alreadyPurchased) {
        MaterialAlertDialogBuilder builderSingle = new MaterialAlertDialogBuilder(mContext);
        builderSingle.setCancelable(false);
        builderSingle.setTitle("Info");
        if (alreadyPurchased == 0) {
            builderSingle.setMessage("Remove ads task is successfully. Restart to take the effects?");
        } else if (alreadyPurchased == 1) {
            builderSingle.setMessage("You are a user who has purchased a remove ads item. Restart to take the effects?");
        }


        builderSingle.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                triggerRebirth(mContext);
            }
        });

        builderSingle.show();

    }

    public static void triggerRebirth(Context context) {
        PackageManager packageManager = context.getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage(context.getPackageName());
        ComponentName componentName = intent.getComponent();
        Intent mainIntent = Intent.makeRestartActivityTask(componentName);
        context.startActivity(mainIntent);
        Runtime.getRuntime().exit(0);
    }


   /* public static void showRateDialogAfterAction(final Context mContext) {
        showReviewDialog(mContext);
    }*/

    public static void showReviewDialog(final Context mContext) {

        if (!SharedPref.getBol(Constants.RATING_OPTIONS)) {
            MaterialAlertDialogBuilder builderSingle = new MaterialAlertDialogBuilder(mContext);
            builderSingle.setCancelable(true);
            builderSingle.setTitle("Rate App ⭐⭐⭐⭐⭐");
            builderSingle.setMessage("Thanks for using our app!\nWould you like to rate our app?");

            builderSingle.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String urlApp = "http://play.google.com/store/apps/details?id=" + mContext.getPackageName();
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlApp));
                    mContext.startActivity(browserIntent);
                    SharedPref.saveBol(Constants.RATING_OPTIONS, true);
                }
            });

            builderSingle.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

           /* builderSingle.setNeutralButton("Don't show again", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SharedPref.saveBol(Constants.RATING_OPTIONS, true);
                    dialog.dismiss();
                }
            });*/

            builderSingle.show();
        }

    }

    @SuppressLint("StaticFieldLeak")
    private static MainActivity mainActivity;
    public static void showUpgradeDialog(final Context mContext) {
        mainActivity = (MainActivity) mContext;
        if (!SharedPref.getBol(Constants.UPGRADE_REMOVE_ADS_OPTIONS)) {
            MaterialAlertDialogBuilder builderSingle = new MaterialAlertDialogBuilder(mContext);
            builderSingle.setCancelable(true);
            builderSingle.setTitle("Upgrade to Premium");
            builderSingle.setMessage("Would you like to remove ads and unlock premium feature?");

            builderSingle.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mainActivity.launchRemoveAdsDialogFlow();
                    SharedPref.saveBol(Constants.UPGRADE_REMOVE_ADS_OPTIONS, true);
                }
            });

            builderSingle.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builderSingle.show();
        } else {
            ////Log.d("MP3Downloader", "Dialog doesnt show again");
            showReviewDialog(mainActivity);
        }

    }


    private static void goToUrl(String stringUrl, Context mContext) {
        Intent intentRating = new Intent(Intent.ACTION_VIEW);
        intentRating.setData(Uri.parse(stringUrl));
        mContext.startActivity(intentRating);
    }

    public static int getRandomDoubleBetweenRange(int min, int max) {
        int x = (int) (Math.random() * ((max - min) + 1)) + min;
        return x;
    }

    public static boolean getRandomBoolean() {
        return Math.random() < 0.5;
    }
}
