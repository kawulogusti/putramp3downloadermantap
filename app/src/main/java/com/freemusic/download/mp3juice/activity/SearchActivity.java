package com.freemusic.download.mp3juice.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.BaseColumns;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.cursoradapter.widget.SimpleCursorAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.freemusic.download.mp3juice.R;
import com.freemusic.download.mp3juice.adapter.ItemAddToPlaylistAdapter;
import com.freemusic.download.mp3juice.adapter.SearchMediaItemAdapter;
import com.freemusic.download.mp3juice.api.ConfApp;
import com.freemusic.download.mp3juice.api.IpResponse;
import com.freemusic.download.mp3juice.api.RequestAPI;
import com.freemusic.download.mp3juice.api.SearchSuggestionsApi;
import com.freemusic.download.mp3juice.api.SearchSuggestionsResponse;
import com.freemusic.download.mp3juice.ext.ExtractorException;
import com.freemusic.download.mp3juice.ext.YExtractor;
import com.freemusic.download.mp3juice.ext.model.YMedia;
import com.freemusic.download.mp3juice.ext.utils.LogUtils;
import com.freemusic.download.mp3juice.model.Download;
import com.freemusic.download.mp3juice.model.MediaItem;
import com.freemusic.download.mp3juice.model.PlaylistPost;
import com.freemusic.download.mp3juice.model.SearchResponse;
import com.freemusic.download.mp3juice.service.AudioPlayerService;
import com.freemusic.download.mp3juice.service.DownloadService;
import com.freemusic.download.mp3juice.utils.AppRaterHelper;
import com.freemusic.download.mp3juice.utils.Constants;
import com.freemusic.download.mp3juice.utils.DBHelper;
import com.freemusic.download.mp3juice.utils.HelperUtils;
import com.freemusic.download.mp3juice.utils.JdkmdenJav;
import com.freemusic.download.mp3juice.utils.SharedPref;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.chip.Chip;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.startapp.sdk.ads.banner.Banner;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.StartAppSDK;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchActivity extends AppCompatActivity implements ServiceConnection {

    public static final String SEARCH_QUERY = "query";

    static {
        System.loadLibrary("realm-lib-sok");
    }

    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    private String currentQuery = "";
    private List<MediaItem> mediaItems = new ArrayList<>();
    private List<MediaItem> mediaItemsServer2 = new ArrayList<>();
    private List<MediaItem> mediaItemsServer3 = new ArrayList<>();
    private List<MediaItem> mediaItemsServer4 = new ArrayList<>();
    private List<MediaItem> mediaItemsServer5 = new ArrayList<>();
    private ConfApp confApp;
    private IpResponse ipResponse;
    private String packageName = "emptypackageName";
    private String appVersion = "emptyappVersion";
    private Toolbar toolbar;
    private SearchView searchView;
    private RecyclerView recyclerView;
    private SearchMediaItemAdapter adapter;
    private SimpleCursorAdapter mAdapter;
    private TextView textViewNoData;
    private Chip chip_server1, chip_server2, chip_server3, chip_server4, chip_server5;
    //exoplayer
    private PlayerView playerView;
    private SimpleExoPlayer player;
    private TextView textViewSongTitle;
    private ImageView iv_thumbnail_playnow;
    private boolean mBound = false;
    private AudioPlayerService mService;
    private MediaItem currentMediaItem;
    //ads
    private AdView mAdView;
    private AdRequest adRequest;
    private InterstitialAd mInterstitialAd;
    //startapp ads
    private Banner banner;

    private boolean isPlaySourceAfterServiceEnd = false;
    private ArrayList<MediaItem> currentPlayMediaItems = new ArrayList<>();
    private int currentPosition = 0;
    private BottomSheetDialog mBottomSheetDialog;
    private RecyclerView recyclerViewPlayList;
    private ItemAddToPlaylistAdapter itemAddToPlaylistAdapter;
    private MaterialAlertDialogBuilder dialogAddPlaylist;
    private MediaItem localTracItemforPlaylist;
    private BroadcastReceiver downloaderReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final MediaItem mediaItem = intent.getParcelableExtra(DownloadService.MEDIA_ITEM);
            String message = intent.getStringExtra(DownloadService.BROADCAST_MESSAGE);

            adapter.notifyDataSetChanged();

            final MaterialAlertDialogBuilder alertDialog = new MaterialAlertDialogBuilder(SearchActivity.this);

            if (message.equalsIgnoreCase(DownloadService.BROADCAST_MESSAGE_SUCCESS)) {
                alertDialog.setIcon(R.drawable.ic_check_black_24dp);
                alertDialog.setTitle("Download Success");
//                alertDialog.setMessage(mediaItem.getTitle());
                alertDialog.setMessage(mediaItem.getTitle() + " downloaded successfully.");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (HelperUtils.getRandomBoolean()) {
                            showIntersialAdsWithoutRandom();
                        } else {
                            AppRaterHelper.showReviewDialog(SearchActivity.this);
                        }
                    }
                });
                alertDialog.setNegativeButton("Rate", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ratingApp();
                    }
                });
                Toast.makeText(SearchActivity.this, "Download is successful.\n" + mediaItem.getTitle(), Toast.LENGTH_LONG).show();
            } else if (message.equalsIgnoreCase(DownloadService.BROADCAST_MESSAGE_FAILED)) {
                alertDialog.setIcon(R.drawable.ic_error_outline_black_24dp);
                alertDialog.setTitle(DownloadService.BROADCAST_MESSAGE_FAILED);
                alertDialog.setMessage(mediaItem.getTitle());
                alertDialog.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        downloadFile(mediaItem);
                    }
                });
                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                Toast.makeText(SearchActivity.this, "Download is failed. Try again until success, please!\n" + mediaItem.getTitle(), Toast.LENGTH_LONG).show();
            }
            alertDialog.show();
        }
    };

    @Override
    public void onServiceConnected(ComponentName name, IBinder binder) {
        AudioPlayerService.MyBinder audioServiceBiner = (AudioPlayerService.MyBinder) binder;
        mService = audioServiceBiner.getService();
        mBound = true;
        player = mService.getPlayer();
        setupPlayer();
        //LogUtils.log("Service audio connected via Search Activity..");

        if (isPlaySourceAfterServiceEnd) {
            if (currentPlayMediaItems.size() > 0) {
                initializePlayer(currentPlayMediaItems, currentPosition);
            }
        }
    }

    //media player

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mBound = false;
        mService = null;
    }

    @Override
    protected void onStart() {
        super.onStart();
        runAudioPlayerService();
    }

    private void runAudioPlayerService() {
        try {
            Intent intent = new Intent(this, AudioPlayerService.class);
//        Util.startForegroundService(this, intent);
            bindService(intent, this, Context.BIND_AUTO_CREATE);
//        startService(intent);
        } catch (Exception e) {
            Toast.makeText(SearchActivity.this, "An error occured while running a service", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        toolbar = findViewById(R.id.toolbar);
        searchView = findViewById(R.id.searchView);
        setupSearchView();
        recyclerView = findViewById(R.id.recyclerView);
        textViewNoData = findViewById(R.id.textViewNoData);
        confApp = DBHelper.getConfApp();
        ipResponse = DBHelper.getConfAppIp();
        chip_server1 = findViewById(R.id.chip_server1);
        chip_server2 = findViewById(R.id.chip_server2);
        chip_server3 = findViewById(R.id.chip_server3);
        chip_server4 = findViewById(R.id.chip_server4);
        chip_server5 = findViewById(R.id.chip_server5);

        //receiver downloader
        try {
            IntentFilter intentFilter = new IntentFilter(DownloadService.BROADCAST_RECEIVE_DOWNLOAD_FILE);
            registerReceiver(downloaderReceiver, intentFilter);
        } catch (Exception ignored) {

        }

        //player
        playerView = findViewById(R.id.playerView);

        //add to playlist dialog
        setupAddToPlaylistDialog();

        //ads
        mAdView = findViewById(R.id.adView);

        //startapp ads
        banner = findViewById(R.id.startAppBanner);

        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            //Log.d("TwitterDl", "Ads is: "+confApp.isAdEnabled());
            initializeAdSDK();

            if (confApp.isAdSearchEnabled()) {
                initializeBannerAds();

            } else {
                mAdView.setVisibility(View.GONE);
                banner.hideBanner();
                banner.setVisibility(View.GONE);
            }

            if (confApp.isAdIntersialSearchEnabled()) {
                initializeIntersialAds();

            }

        } else {
            //Log.d("TwitterDl", "Ads is: "+confApp.isAdEnabled());
            mAdView.setVisibility(View.GONE);
            banner.hideBanner();
            banner.setVisibility(View.GONE);
        }

        Intent intent = getIntent();
        currentQuery = intent.getStringExtra(SEARCH_QUERY);

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            appVersion = pInfo.versionName;
            packageName = getPackageName();
        } catch (Exception ignored) {
            appVersion = "0.0";
        }

        //toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        if (currentQuery != null) {
            chip_server1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chip_server1.setChecked(true);
                    try {
                        if (mediaItems.size() <= 0) {
                            searchFromChips(currentQuery, "1");
                        } else {
                            bindDataToRecyclerView();
                        }
                    } catch (Exception ignored) {

                    }

                }
            });

            chip_server2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chip_server2.setChecked(true);
                    try {
                        if (mediaItemsServer2.size() <= 0) {
                            searchFromChips(currentQuery, "2");
                        } else {
                            bindDataToRecyclerViewServer2();
                        }
                    } catch (Exception ignored) {

                    }

                }
            });

            chip_server3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chip_server3.setChecked(true);
                    try {
                        if (mediaItemsServer3.size() <= 0) {
                            searchFromChips(currentQuery, "3");
                        } else {
                            bindDataToRecyclerViewServer3();
                        }
                    } catch (Exception ignored) {

                    }


                }
            });

            chip_server4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chip_server4.setChecked(true);
                    try {
                        if (mediaItemsServer4.size() <= 0) {
                            searchFromChips(currentQuery, "4");
                        } else {
                            bindDataToRecyclerViewServer4();
                        }
                    } catch (Exception ignored) {

                    }


                }
            });

            chip_server5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chip_server5.setChecked(true);
                    try {
                        if (mediaItemsServer5.size() <= 0) {
                            searchFromChips(currentQuery, "5");
                        } else {
                            bindDataToRecyclerViewServer5();
                        }
                    } catch (Exception ignored) {

                    }


                }
            });

            searchView.setQuery(currentQuery, false);
            chip_server1.setChecked(true);
            if (confApp != null && confApp.isMainSearch()) {
                searchFile(currentQuery);
            } else {
                mediaItems.clear();
                mediaItems = DBHelper.getSearchResults(currentQuery);
                bindDataToRecyclerView();
            }
        } else {
            try {
                Toast.makeText(SearchActivity.this, "No query", Toast.LENGTH_SHORT).show();
            } catch (Exception ignored) {

            }
        }
    }

    //ads
    private void initializeAdSDK() {
        if (confApp != null && confApp.isAdTypeAdmobSecondActivity()) {
            MobileAds.initialize(this, getString(R.string.google_admob_app_id));
        } else {
            if (confApp != null && confApp.getAdSaId() != null) {
                StartAppSDK.init(this, confApp.getAdSaId(), confApp.getAdSaReturnEnabled());
            } else {
                StartAppSDK.init(this, getResources().getString(R.string.startapp_app_id), confApp.getAdSaReturnEnabled());
            }
            if (!confApp.getAdSaSplashEnabled()) {
                StartAppAd.disableSplash();
            }

            /*if (!SharedPref.getBol(Constants.GDPR_AGREE)) {
                if (ipResponse != null & Constants.GDPR_COUNTRIES.toLowerCase().contains(ipResponse.getName().toLowerCase())) {
                    StartAppSDK.setUserConsent (this,
                            "pas",
                            System.currentTimeMillis(),
                            true);
                    SharedPref.saveBol(Constants.GDPR_AGREE, true);
                } else {
                    SharedPref.saveBol(Constants.GDPR_AGREE, true);
                }
            }*/
        }
    }

    private void initializeIntersialAds() {
        if (confApp != null && confApp.isAdTypeAdmobSecondActivity()) {
            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(getString(R.string.google_admob_intersial_id));
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    requestIntersialAds();
                    if (HelperUtils.getRandomBoolean()) {
                        AppRaterHelper.showReviewDialog(SearchActivity.this);
//                        AppRaterHelper.showUpgradeDialog(SearchActivity.this);
                    }
                }
            });

        } else {

        }
    }

    private void initializeBannerAds() {
        if (confApp != null && confApp.isAdTypeAdmobSecondActivity()) {
            mAdView.setVisibility(View.VISIBLE);
            banner.hideBanner();
            banner.setVisibility(View.GONE);
            adRequest = new AdRequest.Builder()
                    //.addTestDevice("33BE2250B43518CCDA7DE426D04EE231")
                    .build();
            mAdView.loadAd(adRequest);
        } else {
            banner.showBanner();
            banner.setVisibility(View.VISIBLE);
            mAdView.setVisibility(View.GONE);
        }
    }

    public void showIntersialAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser() && confApp.isAdIntersialSearchEnabled()) {
            if (confApp.isAdTypeAdmobSecondActivity()) {
                if (mInterstitialAd.isLoaded() && HelperUtils.getRandomBoolean()) {
                    mInterstitialAd.show();
                } else {
                    requestIntersialAds();
                    if (HelperUtils.getRandomBoolean()) {
                        AppRaterHelper.showReviewDialog(SearchActivity.this);
                    }
                    //Log.d("TAG", "The interstitial wasn't loaded yet.");
                }
            } else {
                StartAppAd.showAd(this);
            }

        }
    }

    public void showIntersialAdsWithoutRandom() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser() && confApp.isAdIntersialSearchEnabled()) {
            if (confApp.isAdTypeAdmobSecondActivity()) {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    requestIntersialAds();
                    //Log.d("TAG", "The interstitial wasn't loaded yet.");
                }
            } else {
                StartAppAd.showAd(this);
            }

        }
    }

    public void requestIntersialAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser() && confApp.isAdIntersialSearchEnabled()) {
            if (confApp.isAdTypeAdmobSecondActivity()) {
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            } else {

            }

        }
    }

    private void setupPlayer() {
        //player
        playerView.setUseController(true);
        playerView.showController();
        playerView.setControllerAutoShow(true);
        playerView.setControllerHideOnTouch(false);
        playerView.setPlayer(player);
        if (player == null) {
            playerView.setVisibility(View.GONE);
        } else {
            playerView.setVisibility(View.VISIBLE);
        }

        textViewSongTitle = findViewById(R.id.textViewSongTitle);
        textViewSongTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        textViewSongTitle.setSelected(true);
        textViewSongTitle.setSingleLine(true);

        textViewSongTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPlayer();
            }
        });

        setTextViewNowPlaying(mService.getTitleSongNowPlaying());


        iv_thumbnail_playnow = findViewById(R.id.iv_thumbnail_playnow);
        iv_thumbnail_playnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPlayer();
            }
        });
        refreshNowPlaying();

        setupPlayerEventListener();
        setTextViewNowPlaying(mService.getTitleSongNowPlaying());

        boolean shuffleMode = SharedPref.getBol(Constants.PLAYER_OPTIONS_SHUFFLE);
        player.setShuffleModeEnabled(shuffleMode);
        int repeatMode = SharedPref.getInt(Constants.PLAYER_OPTIONS_REPEAT);
        player.setRepeatMode(repeatMode);
    }

    private void refreshNowPlaying() {
        setTextViewNowPlaying(mService.getTitleSongNowPlaying());
        refreshThumbnailImage();
    }

    public SimpleExoPlayer getPlayer() {
        return player;
    }

    public void showPlayer() {
        showIntersialAds();
        startActivity(new Intent(SearchActivity.this, PlayerActivity.class));
        setPlayerTitleMarqueeText(player.getPlaybackState());
    }

    public void refreshThumbnailImage() {
        try {
            if (player != null && mService.currentSongList() != null && mService.currentSongList().size() > 0) {
//            Log.d("MP3Downloader", "Value thumbnail is: " + songLIst.get(player.getCurrentWindowIndex()).bitmapResource + " or " + songLIst.get(player.getCurrentWindowIndex()).getThumbnail());
                Glide.with(this)
                        .load(mService.currentSongList().get(player.getCurrentWindowIndex()).getImageUrl())
                        .placeholder(R.drawable.ic_musical_note)
                        .transform(new CircleCrop())
                        .transition(DrawableTransitionOptions.withCrossFade(100))
                        .into(iv_thumbnail_playnow);
            } else {
                Glide.with(this)
                        .load(R.drawable.iconmusic)
                        .placeholder(R.drawable.ic_musical_note)
                        .transform(new CircleCrop())
                        .transition(DrawableTransitionOptions.withCrossFade(100))
                        .into(iv_thumbnail_playnow);
            }
        } catch (Exception ignored) {

        }


    }

    public void setupPlayerEventListener() {
        player.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {
                ////Log.d("MP3Downloader", "Timeline Ganti");

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                ////Log.d("MP3Downloader", "Track Ganti");
                setPlayerTitleMarqueeText(player.getPlaybackState());
                DBHelper.setHistory(mService.currentSongList().get(player.getCurrentWindowIndex()));
            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

//                Log.d("MP3Downloader", "Playback onloadingchanged: " + isLoading);
            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (mService.currentSongList() != null) {
//                    Log.d("MP3Downloader", "Playback state: " + playbackState);
                    setPlayerTitleMarqueeText(playbackState);
                } else {
                    textViewSongTitle.setText(mService.getTitleSongNowPlaying());
                }
                ////Log.d("MP3Downloader", "XPlayerState Ganti " + playbackState);
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {
                ////Log.d("MP3Downloader", "Repeat mode Ganti " + repeatMode);
            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {
                ////Log.d("MP3Downloader", "Shuffle mode Ganti " + shuffleModeEnabled);
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
//                Log.d("MP3Downloader", "Error exo: " + error.getLocalizedMessage());

            }

            @Override
            public void onPositionDiscontinuity(int reason) {
                //Log.d("MP3Downloader", "Position Discontinuity = " + reason);

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
                ////Log.d("MP3Downloader", "Parameter Ganti = " + playbackParameters.speed);
            }

            @Override
            public void onSeekProcessed() {
                ////Log.d("MP3Downloader", "Seek processed Ganti");
                if (mService.currentSongList() != null) {
                    setPlayerTitleMarqueeText(player.getPlaybackState());
                }
            }
        });
    }

    public void initializePlayer(ArrayList<MediaItem> songItems, int index_position) {
        if (mBound) {

            playerView.setVisibility(View.VISIBLE);
            player = mService.getplayerInstance(songItems, index_position);
            playerView.setPlayer(player);

            setupPlayerEventListener();

            boolean shuffleMode = SharedPref.getBol(Constants.PLAYER_OPTIONS_SHUFFLE);
            player.setShuffleModeEnabled(shuffleMode);
            int repeatMode = SharedPref.getInt(Constants.PLAYER_OPTIONS_REPEAT);
            player.setRepeatMode(repeatMode);
        } else {
            runAudioPlayerService();
            isPlaySourceAfterServiceEnd = true;
            currentPlayMediaItems = songItems;
            currentPosition = index_position;
        }
    }

    public void setPlayerTitleMarqueeText(int status) {
        try {
            if (status == Player.STATE_BUFFERING) {
                textViewSongTitle.setText("Buffering.. " +
                        mService.getTitleSongNowPlaying());
                Glide.with(this)
                        .load(mService.currentSongList().get(player.getCurrentWindowIndex()).getImageUrl())
                        .placeholder(R.drawable.ic_musical_note)
                        .transform(new CircleCrop())
                        .transition(DrawableTransitionOptions.withCrossFade(100))
                        .into(iv_thumbnail_playnow);
            } else if (status == Player.STATE_READY) {
                setTextViewNowPlaying(mService.getTitleSongNowPlaying());
                Glide.with(this)
                        .load(mService.currentSongList().get(player.getCurrentWindowIndex()).getImageUrl())
                        .placeholder(R.drawable.ic_musical_note)
                        .transform(new CircleCrop())
                        .transition(DrawableTransitionOptions.withCrossFade(100))
                        .into(iv_thumbnail_playnow);
            } else if (status == Player.STATE_IDLE) {
                setTextViewNowPlaying("Player is idle. Try to play a song!");

            } else {
//                Log.d("MP3Downloader", "Error code: " + status);
                setTextViewNowPlaying("Player stopped.");
            }
//            updatePlayerFragmentViews();
            ////Log.d("MP3Downloader", "PlayerState Ganti " + status);
        } catch (Exception ignored) {
            ////Log.d("MP3Downloader", "Exception =  " + e.getLocalizedMessage());

        }
    }

    //add new playlist dialog

    public void downloadFile(final MediaItem mediaItem) {
        Toast.makeText(SearchActivity.this, "Starting download: " + mediaItem.getTitle() + ".\nSee download progress on the notification.", Toast.LENGTH_LONG).show();
        new YExtractor(confApp, new YExtractor.ExtractorListner() {
            @Override
            public void onExtractionGoesWrong(ExtractorException e) {
                Intent intent = new Intent(DownloadService.BROADCAST_START_DOWNLOAD_FILE);
                intent.putExtra(DownloadService.MEDIA_ITEM, mediaItem);
                sendBroadcast(intent);
            }

            @Override
            public void onExtractionDone(List<YMedia> adativeStream) {
                String url = "";
                for (YMedia media : adativeStream) {
                    url = media.getUrl();
                }

                mediaItem.setDownloadLinkFast(url);
                mediaItem.setTrackUrl(url);
                mediaItem.setFastDl(true);
                Intent intent = new Intent(DownloadService.BROADCAST_START_DOWNLOAD_FILE);
                intent.putExtra(DownloadService.MEDIA_ITEM, mediaItem);
                sendBroadcast(intent);

                //LogUtils.log("Url is: " + url);
            }
        }).useDefaultLogin().Extract(JdkmdenJav.getServTubURL(mediaItem.getMediaId()));

    }

    public void playSingleSong(final ArrayList<MediaItem> songItems, final int index_position) {
        setCurrentMediaItem(songItems.get(index_position));
        Download download = DBHelper.getDownload(currentMediaItem.getMediaId());
        if (download != null) {
            setTextViewNowPlaying("Buffering.. - " + currentMediaItem.getTitle());
            initializePlayer(songItems, index_position);
        } else {
            setTextViewNowPlaying("Buffering.. " + currentMediaItem.getTitle());
            new YExtractor(confApp, new YExtractor.ExtractorListner() {
                @Override
                public void onExtractionGoesWrong(ExtractorException e) {
//                        Log.d("MP3Downloader", "Cannot play from another way.");
                    setTextViewNowPlaying("Buffering.. - " + currentMediaItem.getTitle());
                    initializePlayer(songItems, index_position);
                }


                @Override
                public void onExtractionDone(List<YMedia> adativeStream) {
                    String url = "";
                    for (YMedia media : adativeStream) {
                        url = media.getUrl();
                    }

                    songItems.get(index_position).setTrackUrl(url);
                    songItems.get(index_position).setDownloadLinkFast(url);
                    initializePlayer(songItems, index_position);

                    //LogUtils.log("Url is: " + url);

               /* new YMultiExtractor(confApp, new YMultiExtractor.ExtractorListner() {
                    @Override
                    public void onExtractionGoesWrong(ExtractorException e) {

                    }

                    @Override
                    public void onExtractionDone(List<YMedia> urlsmp3Media) {
                        for (YMedia media:urlsmp3Media) {
                            //LogUtils.log("Success media url: "+media.getUrl());
                        }
                    }
                }).useDefaultLogin().Extract(listMediaId(songItems));*/

//                        Log.d("MP3Downloader", "Success play from another way. - " + url);
                }

            }).useDefaultLogin().Extract(JdkmdenJav.getServTubURL(songItems.get(index_position).getMediaId()));

        }

    }

    private void setTextViewNowPlaying(String string) {
        textViewSongTitle.setText(string);
    }

    public MediaItem getCurrentMediaItem() {
        return currentMediaItem;
    }

    public void setCurrentMediaItem(MediaItem currentMediaItem) {
        this.currentMediaItem = currentMediaItem;
    }

    public void getYtUrl(MediaItem mediaItem) {
        new YExtractor(confApp, new YExtractor.ExtractorListner() {
            @Override
            public void onExtractionGoesWrong(ExtractorException e) {
//                        Log.d("MP3Downloader", "Cannot play from another way.");
                setTextViewNowPlaying("Player stopped.");
            }


            @Override
            public void onExtractionDone(List<YMedia> adativeStream) {
                String url = "";
                for (YMedia media : adativeStream) {
                    url = media.getUrl();
                }

                //LogUtils.log("Url is: " + url);

//                        Log.d("MP3Downloader", "Success play from another way. - " + url);
            }


        }).useDefaultLogin().Extract(JdkmdenJav.getServTubURL(mediaItem.getMediaId()));
    }

    private String[] listMediaId(ArrayList<MediaItem> mediaItems) {
        List<String> mediaIds = new ArrayList<String>();
        for (MediaItem mediaItem : mediaItems) {
            mediaIds.add(mediaItem.getMediaId());
        }
        String[] mediaIdsArray = new String[mediaIds.size()];
        mediaIds.toArray(mediaIdsArray);
        return mediaIdsArray;
    }

    private void setupAddToPlaylistDialog() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        View sheetView = getLayoutInflater().inflate(R.layout.layout_playlist_options_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        Button btn_addToPlaylist = (Button) sheetView.findViewById(R.id.btn_new_playlist);
        recyclerViewPlayList = (RecyclerView) sheetView.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewPlayList.setLayoutManager(linearLayoutManager);


        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                // Do something
            }
        });

        btn_addToPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showAddNewPlaylistDialog(false);

            }
        });

    }


    //search section

    public void showAddNewPlaylistDialog(boolean isFromPplaylistFragment) {
        dialogAddPlaylist = new MaterialAlertDialogBuilder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_playlist_create_new, null);
        dialogAddPlaylist.setView(dialogView);
        dialogAddPlaylist.setCancelable(true);
        dialogAddPlaylist.setTitle(R.string.text_add_new_playlist);

        final EditText editText_playlist_title = (EditText) dialogView.findViewById(R.id.editText_playlist_title);

        dialogAddPlaylist.setPositiveButton("Create", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String playlistTitle = editText_playlist_title.getText().toString();
                DBHelper.createPlaylist(playlistTitle);
//                GeneralHelper.reduceCoinForPlaylist();
                mBottomSheetDialog.dismiss();
                showAddToPlaylistDialog(localTracItemforPlaylist);

            }
        });

        dialogAddPlaylist.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        dialogAddPlaylist.show();

    }

    public void showAddToPlaylistDialog(MediaItem mediaItem) {
        //LogUtils.log("MediaItem title is: " + mediaItem.getTitle());
        localTracItemforPlaylist = mediaItem;
        List<PlaylistPost> playlistList = DBHelper.getPlaylistList();

        itemAddToPlaylistAdapter = new ItemAddToPlaylistAdapter(this, playlistList, mediaItem);
        recyclerViewPlayList.setAdapter(itemAddToPlaylistAdapter);

        mBottomSheetDialog.show();
    }

    private void searchFromChips(String q, String source) {
        if (confApp != null && confApp.isMainSearch()) {
            searchFileChips(q, source);
        } else {
            mediaItems.clear();
            mediaItems = DBHelper.getSearchResults(q);
            bindDataToRecyclerView();
        }
    }

    private void setupSearchView() {
        final String[] from = new String[]{"itemName"};
        final int[] to = new int[]{android.R.id.text1};
        mAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_1,
                null,
                from,
                to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        searchView.setSuggestionsAdapter(mAdapter);
        searchView.setIconifiedByDefault(false);
        searchView.clearFocus();
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionClick(int position) {

                Cursor cursor = (Cursor) mAdapter.getItem(position);
                currentQuery = cursor.getString(cursor.getColumnIndex("itemName"));
                searchView.setQuery(currentQuery, false);
                //Log.d("MP3Downloader", "Query is: "+query);
                if (confApp != null && confApp.isMainSearch()) {
                    searchFile(currentQuery);
                } else {
                    mediaItems.clear();
                    mediaItems = DBHelper.getSearchResults(currentQuery);
                    bindDataToRecyclerView();
                }
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onSuggestionSelect(int position) {
                // Your code here
                Cursor cursor = (Cursor) mAdapter.getItem(position);
                currentQuery = cursor.getString(cursor.getColumnIndex("itemName"));
                //Log.d("MP3Downloader", "Query is: "+query);
                if (confApp != null && confApp.isMainSearch()) {
                    searchFile(currentQuery);
                } else {
                    mediaItems.clear();
                    mediaItems = DBHelper.getSearchResults(currentQuery);
                    bindDataToRecyclerView();
                }
                searchView.clearFocus();
                return false;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if (confApp != null && confApp.isMainSearch()) {
                    currentQuery = s;
                    searchFile(currentQuery);
                } else {
                    mediaItems.clear();
                    mediaItems = DBHelper.getSearchResults(currentQuery);
                    bindDataToRecyclerView();
                }
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                populateAdapter(s);
                return false;
            }
        });
    }

    private void populateAdapter(String query) {
        searchQueries(query);
    }

    private void searchQueries(final String query) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(SearchSuggestionsResponse.class, new SuggestionsDeserializer())
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.GQ_RUL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        final SearchSuggestionsApi service = retrofit.create(SearchSuggestionsApi.class);

        Call<SearchSuggestionsResponse> callConfApp = service.getSuggestions("firefox", "yt", query);
        callConfApp.enqueue(new Callback<SearchSuggestionsResponse>() {
            @Override
            public void onResponse(Call<SearchSuggestionsResponse> call, Response<SearchSuggestionsResponse> response) {
                try {
                    String[] suggestions = response.body() != null ? response.body().getSuggestions() : new String[0];
                    final MatrixCursor c = new MatrixCursor(new String[]{BaseColumns._ID, "itemName"});
                    for (int i = 0; i < suggestions.length; i++) {
                        if (suggestions[i].toLowerCase().startsWith(query.toLowerCase()))
                            c.addRow(new Object[]{i, suggestions[i]});
                    }
                    mAdapter.changeCursor(c);
                } catch (Exception ignored) {

                }

            }

            @Override
            public void onFailure(Call<SearchSuggestionsResponse> call, Throwable t) {
                try {
                    Toast.makeText(SearchActivity.this, "Unable to fetch data..", Toast.LENGTH_SHORT).show();
                } catch (Exception ignored) {

                }

            }
        });
    }

    private void searchFile(final String query) {
        //Log.d("MP3Downloader", "Call search file");
        textViewNoData.setText("Please wait..");
        chip_server1.setChecked(true);
        mediaItems.clear();
        mediaItemsServer2.clear();
        mediaItemsServer3.clear();
        mediaItemsServer4.clear();
        mediaItemsServer5.clear();
        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(Constants.WKMSCAPIGGBISMILLAHIRROHMANIRROHIIMALLOHUMMASHOLLIALASAYYIDINAMUHAMMAD)
//                .baseUrl(BuildConfig.WKMSCAPIGGBISMILLAHIRROHMANIRROHIIMALLOHUMMASHOLLIALASAYYIDINAMUHAMMAD)
                .baseUrl(JdkmdenJav.getWorker())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final RequestAPI service = retrofit.create(RequestAPI.class);

        Call<SearchResponse> callConfApp = service.getSearchFile(query, SharedPref.getString(Constants.UID), packageName, appVersion);
        callConfApp.enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                if (response.body() != null) {
                    int currentProgress = 0;
                    for (MediaItem resultsItem : response.body().getResults()) {
                        currentProgress = currentProgress + 1;
                        if (ipResponse != null) {
                            MediaItem trackItem = new MediaItem(resultsItem.getArtist(), resultsItem.getImageUrl(), resultsItem.getMediaId(), resultsItem.getTitle(), resultsItem.getTrackUrl(ipResponse));
                            trackItem.setFastDl(true);
                            if (!mediaItems.contains(trackItem)) {
                                mediaItems.add(trackItem);
                            }
                        } else {
                            MediaItem trackItem = new MediaItem(resultsItem.getArtist(), resultsItem.getImageUrl(), resultsItem.getMediaId(), resultsItem.getTitle(), resultsItem.getTrackUrl());
                            trackItem.setFastDl(true);
                            if (!mediaItems.contains(trackItem)) {
                                mediaItems.add(trackItem);
                            }
                        }

                    }

                    if (response.body().getResults().size() <= 0) {
                        int page = 1;
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(JdkmdenJav.getServTub())
                                .build();
                        final RequestAPI service = retrofit.create(RequestAPI.class);
                        Call<ResponseBody> call1 = service.getTub(query, page, "EgIQAQ%253D%253D");
                        call1.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.body() != null) {

                                    try {
//                                            Log.d("MP3Downloader", "Body value: " + response.body().string());

                                        final Elements select = Jsoup.parse(response.body().string()).select("div.yt-lockup-content");
                                        for (int i = 0; i < select.size(); i++) {
                                            Element element = (Element) select.get(i);
                                            Element elementVideoInfo = element.selectFirst("h3[class=\"yt-lockup-title \"]");
                                            Element elementVideoAndTitleUrl = elementVideoInfo.selectFirst("a[class=\"yt-uix-tile-link yt-ui-ellipsis yt-ui-ellipsis-2 yt-uix-sessionlink      spf-link \"]");
                                            Element elementVideoBadge = element.selectFirst("div[class=\"yt-lockup-badges\"]");
                                            String url = elementVideoAndTitleUrl.attr("href");
                                            String title = elementVideoAndTitleUrl.attr("title");
                                            if (elementVideoBadge != null) {
                                                Element elementisLive = elementVideoBadge.selectFirst("span[class=\"yt-badge  yt-badge-live\"]");
                                                if (elementisLive == null) {

//                                                    Log.d("MP3Downloader", "Element value with badge but not live: " + title + " - " + url);
//                                                Log.d("MP3Downloader", "Element value: " + element.toString());
                                                    String mediaId = url.replace("/watch?v=", "");
//                                        G_YouTubeVideo g_YouTubeVideo = new G_YouTubeVideo(element.select("div.playlist-btn a.playlist-play").attr("data-url"), element.select("div.playlist-name span.playlist-name-title a em").text(), "", element.select("div.playlist-name span.playlist-name-artist a").text());
                                                    MediaItem trackItem = new MediaItem(
                                                            "Music Downloader",
                                                            "https://images.unsplash.com/photo-1446057032654-9d8885db76c6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
                                                            mediaId,
                                                            title,
                                                            JdkmdenJav.getWorker() + "trackUrl?mediaId=" + mediaId + "&title=" + title + ipResponse.getURLEncoded());
                                                    trackItem.setFastDl(true);
                                                    if (!mediaItems.contains(trackItem)) {
                                                        mediaItems.add(trackItem);
                                                    }
                                                }
                                            } else {
//                                                Log.d("MP3Downloader", "Element value without badge: " + title + " - " + url);
//                                                Log.d("MP3Downloader", "Element value: " + element.toString());
                                                String mediaId = url.replace("/watch?v=", "");
//                                        G_YouTubeVideo g_YouTubeVideo = new G_YouTubeVideo(element.select("div.playlist-btn a.playlist-play").attr("data-url"), element.select("div.playlist-name span.playlist-name-title a em").text(), "", element.select("div.playlist-name span.playlist-name-artist a").text());
                                                MediaItem trackItem = new MediaItem(
                                                        "Music Downloader",
                                                        "https://images.unsplash.com/photo-1446057032654-9d8885db76c6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
                                                        mediaId,
                                                        title,
                                                        JdkmdenJav.getWorker() + "trackUrl?mediaId=" + mediaId + "&title=" + title + ipResponse.getURLEncoded());
                                                trackItem.setFastDl(true);
                                                if (!mediaItems.contains(trackItem)) {
                                                    mediaItems.add(trackItem);
                                                }
                                            }

                                        }
                                        bindDataToRecyclerView();
                                    } catch (IOException e) {

                                    }
                                }

                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {

                            }
                        });
                    } else {
                        bindDataToRecyclerView();
                    }
                } else {
                    try {
                        Toast.makeText(SearchActivity.this, "Server is busy, try again in a few minutes..", Toast.LENGTH_SHORT).show();
                    } catch (Exception ignored) {

                    }
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                try {
                    Toast.makeText(SearchActivity.this, "Server is busy, try again in a few minutes..", Toast.LENGTH_SHORT).show();
                } catch (Exception ignored) {

                }

                try {
                    bindDataToRecyclerView();
                } catch (Exception ignored) {

                }

            }
        });
    }

    private void searchFileChips(final String query, String source) {
        //Log.d("MP3Downloader", "Call search file");
        textViewNoData.setText("Please wait..");
        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(Constants.WKMSCAPIGGBISMILLAHIRROHMANIRROHIIMALLOHUMMASHOLLIALASAYYIDINAMUHAMMAD)
//                .baseUrl(BuildConfig.WKMSCAPIGGBISMILLAHIRROHMANIRROHIIMALLOHUMMASHOLLIALASAYYIDINAMUHAMMAD)
                .baseUrl(JdkmdenJav.getWorker())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final RequestAPI service = retrofit.create(RequestAPI.class);
        Call<SearchResponse> callConfApp;
        if (source.equalsIgnoreCase("1")) {
            if (mediaItems.size() <= 0) {
                callConfApp = service.getSearchFile(query, SharedPref.getString(Constants.UID), packageName, appVersion);
                callConfApp.enqueue(new Callback<SearchResponse>() {
                    @Override
                    public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                        if (response.body() != null) {
                            int currentProgress = 0;
                            for (MediaItem resultsItem : response.body().getResults()) {
                                currentProgress = currentProgress + 1;
                                if (ipResponse != null) {
                                    MediaItem trackItem = new MediaItem(resultsItem.getArtist(), resultsItem.getImageUrl(), resultsItem.getMediaId(), resultsItem.getTitle(), resultsItem.getTrackUrl(ipResponse));
                                    trackItem.setFastDl(true);
                                    if (!mediaItems.contains(trackItem)) {
                                        mediaItems.add(trackItem);
                                    }
                                } else {
                                    MediaItem trackItem = new MediaItem(resultsItem.getArtist(), resultsItem.getImageUrl(), resultsItem.getMediaId(), resultsItem.getTitle(), resultsItem.getTrackUrl());
                                    trackItem.setFastDl(true);
                                    if (!mediaItems.contains(trackItem)) {
                                        mediaItems.add(trackItem);
                                    }
                                }

                            }

                            if (response.body().getResults().size() <= 0) {

                                int page = 1;
                                Retrofit retrofit = new Retrofit.Builder()
                                        .baseUrl(JdkmdenJav.getServTub())
                                        .build();
                                final RequestAPI service = retrofit.create(RequestAPI.class);
                                Call<ResponseBody> call1 = service.getTub(query, page, "EgIQAQ%253D%253D");
                                call1.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        if (response.body() != null) {

                                            try {
//                                            Log.d("MP3Downloader", "Body value: " + response.body().string());

                                                final Elements select = Jsoup.parse(response.body().string()).select("div.yt-lockup-content");
                                                for (int i = 0; i < select.size(); i++) {
                                                    Element element = (Element) select.get(i);
                                                    Element elementVideoInfo = element.selectFirst("h3[class=\"yt-lockup-title \"]");
                                                    Element elementVideoAndTitleUrl = elementVideoInfo.selectFirst("a[class=\"yt-uix-tile-link yt-ui-ellipsis yt-ui-ellipsis-2 yt-uix-sessionlink      spf-link \"]");
                                                    Element elementVideoBadge = element.selectFirst("div[class=\"yt-lockup-badges\"]");
                                                    String url = elementVideoAndTitleUrl.attr("href");
                                                    String title = elementVideoAndTitleUrl.attr("title");
                                                    if (elementVideoBadge != null) {
                                                        Element elementisLive = elementVideoBadge.selectFirst("span[class=\"yt-badge  yt-badge-live\"]");
                                                        if (elementisLive == null) {

//                                                    Log.d("MP3Downloader", "Element value with badge but not live: " + title + " - " + url);
//                                                Log.d("MP3Downloader", "Element value: " + element.toString());
                                                            String mediaId = url.replace("/watch?v=", "");
                                                            MediaItem trackItem = new MediaItem(
                                                                    "Music Downloader",
                                                                    "https://images.unsplash.com/photo-1446057032654-9d8885db76c6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
                                                                    mediaId,
                                                                    title,
                                                                    JdkmdenJav.getWorker() + "trackUrl?mediaId=" + mediaId + "&title=" + title + ipResponse.getURLEncoded());
                                                            trackItem.setFastDl(true);
                                                            if (!mediaItems.contains(trackItem)) {
                                                                mediaItems.add(trackItem);
                                                            }
                                                        }
                                                    } else {
//                                                Log.d("MP3Downloader", "Element value without badge: " + title + " - " + url);
//                                                Log.d("MP3Downloader", "Element value: " + element.toString());
                                                        String mediaId = url.replace("/watch?v=", "");
//                                        G_YouTubeVideo g_YouTubeVideo = new G_YouTubeVideo(element.select("div.playlist-btn a.playlist-play").attr("data-url"), element.select("div.playlist-name span.playlist-name-title a em").text(), "", element.select("div.playlist-name span.playlist-name-artist a").text());
                                                        MediaItem trackItem = new MediaItem(
                                                                "Music Downloader",
                                                                "https://images.unsplash.com/photo-1446057032654-9d8885db76c6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
                                                                mediaId,
                                                                title,
                                                                JdkmdenJav.getWorker() + "trackUrl?mediaId=" + mediaId + "&title=" + title + ipResponse.getURLEncoded());
                                                        trackItem.setFastDl(true);
                                                        if (!mediaItems.contains(trackItem)) {
                                                            mediaItems.add(trackItem);
                                                        }
                                                    }

                                                }
                                                bindDataToRecyclerView();
                                            } catch (IOException e) {

                                            }
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                                    }
                                });
                            } else {
                                bindDataToRecyclerView();
                            }
                        } else {
                            try {
                                Toast.makeText(SearchActivity.this, "Server is busy, try again in a few minutes..", Toast.LENGTH_SHORT).show();
                            } catch (Exception ignored) {

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SearchResponse> call, Throwable t) {
                        try {
                            Toast.makeText(SearchActivity.this, "Server is busy, try again in a few minutes..", Toast.LENGTH_SHORT).show();
                        } catch (Exception ignored) {

                        }
                        try {
                            bindDataToRecyclerView();
                        } catch (Exception ignored) {

                        }

                    }
                });
            } else {
                bindDataToRecyclerView();
            }

        } else if (source.equalsIgnoreCase("2")) {
            if (mediaItemsServer2.size() <= 0) {
                callConfApp = service.getSearchFile2(query, SharedPref.getString(Constants.UID), packageName, appVersion);
                callConfApp.enqueue(new Callback<SearchResponse>() {
                    @Override
                    public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                        if (response.body() != null) {
                            int currentProgress = 0;
                            for (MediaItem resultsItem : response.body().getResults()) {
                                currentProgress = currentProgress + 1;
                                if (ipResponse != null) {
                                    MediaItem trackItem = new MediaItem(resultsItem.getArtist(), resultsItem.getImageUrl(), resultsItem.getMediaId(), resultsItem.getTitle(), resultsItem.getTrackUrl(ipResponse));
                                    if (!mediaItemsServer2.contains(trackItem)) {
                                        mediaItemsServer2.add(trackItem);
                                    }
                                } else {
                                    MediaItem trackItem = new MediaItem(resultsItem.getArtist(), resultsItem.getImageUrl(), resultsItem.getMediaId(), resultsItem.getTitle(), resultsItem.getTrackUrl(ipResponse));
                                    if (!mediaItemsServer2.contains(trackItem)) {
                                        mediaItemsServer2.add(trackItem);
                                    }
                                }

                            }
                            bindDataToRecyclerViewServer2();
                        } else {
                            try {
                                Toast.makeText(SearchActivity.this, "Server is busy, try again in a few minutes..", Toast.LENGTH_SHORT).show();
                            } catch (Exception ignored) {

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SearchResponse> call, Throwable t) {
                        try {
                            Toast.makeText(SearchActivity.this, "Server is busy, try again in a few minutes..", Toast.LENGTH_SHORT).show();
                        } catch (Exception ignored) {

                        }
                        try {
                            bindDataToRecyclerViewServer2();
                        } catch (Exception ignored) {

                        }

                    }
                });
            } else {
                bindDataToRecyclerViewServer2();
            }
        } else if (source.equalsIgnoreCase("3")) {
            if (mediaItemsServer3.size() <= 0) {
                callConfApp = service.getSearchFile4(query, SharedPref.getString(Constants.UID), packageName, appVersion);
                callConfApp.enqueue(new Callback<SearchResponse>() {
                    @Override
                    public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                        if (response.body() != null) {
                            int currentProgress = 0;
                            for (MediaItem resultsItem : response.body().getResults()) {
                                currentProgress = currentProgress + 1;
                                if (ipResponse != null) {
                                    MediaItem trackItem = new MediaItem(resultsItem.getArtist(), resultsItem.getImageUrl(), resultsItem.getMediaId(), resultsItem.getTitle(), resultsItem.getTrackUrl(ipResponse));
                                    if (!mediaItemsServer3.contains(trackItem)) {
                                        mediaItemsServer3.add(trackItem);
                                    }
                                } else {
                                    MediaItem trackItem = new MediaItem(resultsItem.getArtist(), resultsItem.getImageUrl(), resultsItem.getMediaId(), resultsItem.getTitle(), resultsItem.getTrackUrl(ipResponse));
                                    if (!mediaItemsServer3.contains(trackItem)) {
                                        mediaItemsServer3.add(trackItem);
                                    }
                                }

                            }
                            bindDataToRecyclerViewServer3();
                        } else {
                            try {
                                Toast.makeText(SearchActivity.this, "Server is busy, try again in a few minutes..", Toast.LENGTH_SHORT).show();
                            } catch (Exception ignored) {

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SearchResponse> call, Throwable t) {
                        try {
                            Toast.makeText(SearchActivity.this, "Server is busy, try again in a few minutes..", Toast.LENGTH_SHORT).show();
                        } catch (Exception ignored) {

                        }
                        try {
                            bindDataToRecyclerViewServer3();
                        } catch (Exception ignored) {

                        }

                    }
                });
            } else {
                bindDataToRecyclerViewServer3();
            }
        } else if (source.equalsIgnoreCase("4")) {
            if (mediaItemsServer4.size() <= 0) {
                callConfApp = service.getSearchFile5(query, SharedPref.getString(Constants.UID), packageName, appVersion);
                callConfApp.enqueue(new Callback<SearchResponse>() {
                    @Override
                    public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                        if (response.body() != null) {
                            int currentProgress = 0;
                            for (MediaItem resultsItem : response.body().getResults()) {
                                currentProgress = currentProgress + 1;
                                if (ipResponse != null) {
                                    MediaItem trackItem = new MediaItem(resultsItem.getArtist(), resultsItem.getImageUrl(), resultsItem.getMediaId(), resultsItem.getTitle(), resultsItem.getTrackUrl(ipResponse));
                                    if (!mediaItemsServer4.contains(trackItem)) {
                                        mediaItemsServer4.add(trackItem);
                                    }
                                } else {
                                    MediaItem trackItem = new MediaItem(resultsItem.getArtist(), resultsItem.getImageUrl(), resultsItem.getMediaId(), resultsItem.getTitle(), resultsItem.getTrackUrl(ipResponse));
                                    if (!mediaItemsServer4.contains(trackItem)) {
                                        mediaItemsServer4.add(trackItem);
                                    }
                                }

                            }

                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(JdkmdenJav.getServ())
                                    .build();
                            final RequestAPI service = retrofit.create(RequestAPI.class);
                            Call<ResponseBody> call1 = service.getRidl(query);
                            call1.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    if (response.body() != null) {

                                        try {
//                                            Log.d("MP3Downloader", "Body value: " + response.body().string());

                                            final Elements select = Jsoup.parse(response.body().string()).select("ul.playlist li");
                                            for (int i = 0; i < select.size(); i++) {
                                                Element element = (Element) select.get(i);
//                                                Log.d("MP3Downloader", "Element value: " + element.toString());
//                                        G_YouTubeVideo g_YouTubeVideo = new G_YouTubeVideo(element.select("div.playlist-btn a.playlist-play").attr("data-url"), element.select("div.playlist-name span.playlist-name-title a em").text(), "", element.select("div.playlist-name span.playlist-name-artist a").text());
                                                MediaItem trackItem = new MediaItem(element.select("div.playlist-name span.playlist-name-artist a").text(),
                                                        "https://images.unsplash.com/photo-1446057032654-9d8885db76c6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
                                                        "ridls-media-id" + i + element.select("div.playlist-name span.playlist-name-title a em").text(),
                                                        element.select("div.playlist-name span.playlist-name-title a em").text() + " - " + element.select("div.playlist-name span.playlist-name-artist a").text(),
                                                        element.select("div.playlist-btn a.playlist-play").attr("data-url"));
                                                if (!mediaItemsServer4.contains(trackItem)) {
                                                    mediaItemsServer4.add(trackItem);
                                                }
                                            }
                                            bindDataToRecyclerViewServer4();
                                        } catch (IOException e) {

                                        }
                                    }

                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {

                                }
                            });

                        } else {
                            try {
                                Toast.makeText(SearchActivity.this, "Server is busy, try again in a few minutes..", Toast.LENGTH_SHORT).show();
                            } catch (Exception ignored) {

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SearchResponse> call, Throwable t) {
                        try {
                            Toast.makeText(SearchActivity.this, "Server is busy, try again in a few minutes..", Toast.LENGTH_SHORT).show();
                        } catch (Exception ignored) {

                        }
                        try {
                            bindDataToRecyclerViewServer4();
                        } catch (Exception ignored) {

                        }

                    }
                });
            } else {
                bindDataToRecyclerViewServer4();
            }
        } else if (source.equalsIgnoreCase("5")) {
            if (mediaItemsServer5.size() <= 0) {
                callConfApp = service.getSearchFile3(query, SharedPref.getString(Constants.UID), packageName, appVersion);
                callConfApp.enqueue(new Callback<SearchResponse>() {
                    @Override
                    public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                        if (response.body() != null) {
                            int currentProgress = 0;
                            for (MediaItem resultsItem : response.body().getResults()) {
                                currentProgress = currentProgress + 1;
                                if (ipResponse != null) {
                                    MediaItem trackItem = new MediaItem(resultsItem.getArtist(), resultsItem.getImageUrl(), resultsItem.getMediaId(), resultsItem.getTitle(), resultsItem.getTrackUrl(ipResponse));
                                    if (!mediaItemsServer5.contains(trackItem)) {
                                        mediaItemsServer5.add(trackItem);
                                    }
                                } else {
                                    MediaItem trackItem = new MediaItem(resultsItem.getArtist(), resultsItem.getImageUrl(), resultsItem.getMediaId(), resultsItem.getTitle(), resultsItem.getTrackUrl(ipResponse));
                                    if (!mediaItemsServer5.contains(trackItem)) {
                                        mediaItemsServer5.add(trackItem);
                                    }
                                }

                            }
                            bindDataToRecyclerViewServer5();
                        } else {
                            try {
                                Toast.makeText(SearchActivity.this, "Server is busy, try again in a few minutes..", Toast.LENGTH_SHORT).show();
                            } catch (Exception ignored) {

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SearchResponse> call, Throwable t) {
                        try {
                            Toast.makeText(SearchActivity.this, "Server is busy, try again in a few minutes..", Toast.LENGTH_SHORT).show();
                        } catch (Exception ignored) {

                        }
                        try {
                            bindDataToRecyclerViewServer5();
                        } catch (Exception ignored) {

                        }

                    }
                });
            } else {
                bindDataToRecyclerViewServer5();
            }
        }


    }

    public void bindDataToRecyclerView() {
        try {
            if (mediaItems.size() > 0) {
                LinkedHashSet<MediaItem> hashSet = new LinkedHashSet<>(mediaItems);
                mediaItems = new ArrayList<>(hashSet);
                textViewNoData.setVisibility(View.VISIBLE);
                textViewNoData.setText("Server 1: Found " + mediaItems.size() + " items");
            } else {
                textViewNoData.setVisibility(View.VISIBLE);
                textViewNoData.setText("Currently, there is no data from server 1 😥");
            }
            recyclerView.setNestedScrollingEnabled(true);
            adapter = new SearchMediaItemAdapter(this, mediaItems);
            recyclerView.setAdapter(adapter);
            if (!confApp.isMainSearch()) {
                chip_server2.setVisibility(View.GONE);
                chip_server3.setVisibility(View.GONE);
                chip_server4.setVisibility(View.GONE);
                chip_server5.setVisibility(View.GONE);
            }
        } catch (Exception ignored) {

        }


    }

    public void bindDataToRecyclerViewServer2() {
        if (mediaItemsServer2.size() > 0) {
            LinkedHashSet<MediaItem> hashSet = new LinkedHashSet<>(mediaItemsServer2);
            mediaItemsServer2 = new ArrayList<>(hashSet);
            textViewNoData.setVisibility(View.VISIBLE);
            textViewNoData.setText("Server 2: Found " + mediaItemsServer2.size() + " items");
        } else {
            textViewNoData.setVisibility(View.VISIBLE);
            textViewNoData.setText("Currently, there is no data from server 2 😥");
        }
        recyclerView.setNestedScrollingEnabled(true);
        adapter = new SearchMediaItemAdapter(this, mediaItemsServer2);
        recyclerView.setAdapter(adapter);

    }

    public void bindDataToRecyclerViewServer3() {
        if (mediaItemsServer3.size() > 0) {
            LinkedHashSet<MediaItem> hashSet = new LinkedHashSet<>(mediaItemsServer3);
            mediaItemsServer3 = new ArrayList<>(hashSet);
            textViewNoData.setVisibility(View.VISIBLE);
            textViewNoData.setText("Server 3: Found " + mediaItemsServer3.size() + " items");
        } else {
            textViewNoData.setVisibility(View.VISIBLE);
            textViewNoData.setText("Currently, there is no data from server 3 😥");
        }
        recyclerView.setNestedScrollingEnabled(true);
        adapter = new SearchMediaItemAdapter(this, mediaItemsServer3);
        recyclerView.setAdapter(adapter);

    }

    public void bindDataToRecyclerViewServer4() {
        if (mediaItemsServer4.size() > 0) {
            LinkedHashSet<MediaItem> hashSet = new LinkedHashSet<>(mediaItemsServer4);
            mediaItemsServer4 = new ArrayList<>(hashSet);
            textViewNoData.setVisibility(View.VISIBLE);
            textViewNoData.setText("Server 4: Found " + mediaItemsServer4.size() + " items");
        } else {
            textViewNoData.setVisibility(View.VISIBLE);
            textViewNoData.setText("Currently, there is no data from server 4 😥");
        }
        recyclerView.setNestedScrollingEnabled(true);
        adapter = new SearchMediaItemAdapter(this, mediaItemsServer4);
        recyclerView.setAdapter(adapter);

    }

    public void bindDataToRecyclerViewServer5() {
        if (mediaItemsServer5.size() > 0) {
            LinkedHashSet<MediaItem> hashSet = new LinkedHashSet<>(mediaItemsServer5);
            mediaItemsServer5 = new ArrayList<>(hashSet);
            textViewNoData.setVisibility(View.VISIBLE);
            textViewNoData.setText("Server 5: Found " + mediaItemsServer5.size() + " items");
        } else {
            textViewNoData.setVisibility(View.VISIBLE);
            textViewNoData.setText("Currently, there is no data from server 5 😥");
        }
        recyclerView.setNestedScrollingEnabled(true);
        adapter = new SearchMediaItemAdapter(this, mediaItemsServer5);
        recyclerView.setAdapter(adapter);

    }

    //permission
    public boolean checkPermission() {
//        //Log.d("MP3Downloader", "Requesting permission");
        int result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;
        }
    }


    public void requestPermission() {

        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("Read Files");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write Files");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                ActivityCompat.requestPermissions(this, permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
        }
    }

    public boolean addPermission(List<String> permissionsList, String permission) {
        if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permission))
                return false;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {
            if (resultCode == RESULT_OK) {
//                runForFirstTime();
                Toast.makeText(this, "Thanks. Permission granted", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                requestPermissionAgain();
            } else {
                Toast.makeText(this, "Please allow the files permission. Main features will not working properly.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void requestPermissionAgain() {
        new MaterialAlertDialogBuilder(this)
                .setCancelable(false)
                .setTitle("Enable file permission")
                .setMessage("We need grant to write downloaded files to the storage. Enable file permission?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        requestPermission();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(SearchActivity.this, "Please allow the files permission. Main features will not working properly.", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    private void ratingApp() {
        String urlApp = "http://play.google.com/store/apps/details?id=" + getPackageName();
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlApp));
        startActivity(browserIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(downloaderReceiver);
        } catch (Exception ignored) {

        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(this);
        try {
            unregisterReceiver(downloaderReceiver);
        } catch (Exception ignored) {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        runAudioPlayerService();
        try {
            IntentFilter intentFilter = new IntentFilter(DownloadService.BROADCAST_RECEIVE_DOWNLOAD_FILE);
            registerReceiver(downloaderReceiver, intentFilter);
        } catch (Exception ignored) {

        }

        if (mService != null && player != null) {
            player = mService.getPlayer();
            if (player != null) {
                try {
                    setupPlayer();
                } catch (Exception e) {
                    Toast.makeText(SearchActivity.this, "An error occured.", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private class SuggestionsDeserializer implements JsonDeserializer<SearchSuggestionsResponse> {
        public SearchSuggestionsResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
            String rawSt = json.toString();
            String[] suggestions = rawSt.replaceAll("\\[(.*?)\\[", "")
                    .replaceAll("]", "")
                    .replaceAll("\"", "")
                    .split(",");
            return new SearchSuggestionsResponse(suggestions);
        }
    }



}
