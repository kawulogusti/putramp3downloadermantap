package com.freemusic.download.mp3juice.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.freemusic.download.mp3juice.R;
import com.freemusic.download.mp3juice.model.MediaItem;
import com.freemusic.download.mp3juice.model.PlaylistItem;
import com.freemusic.download.mp3juice.model.PlaylistPost;
import com.freemusic.download.mp3juice.utils.DBHelper;

import java.util.List;


/**
 * Created by ahm on 11/05/17.
 */

public class ItemAddToPlaylistAdapter extends RecyclerView.Adapter<ItemAddToPlaylistAdapter.MyViewHolder> {

    private Context mContext;
    private List<PlaylistPost> listKonten;
    private MediaItem mediaItem;

    public ItemAddToPlaylistAdapter(Context mContext, List<PlaylistPost> listKontenP, MediaItem mediaItem) {
        this.mContext = mContext;
        this.listKonten = listKontenP;
        this.mediaItem = mediaItem;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_playlist_dialog, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final PlaylistPost konten = listKonten.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        PlaylistItem itemsItem = DBHelper.getPlaylistItem(mediaItem.getMediaId());

        if (konten.getPlaylistItems().contains(itemsItem)) {
            holder.checkBox_playlist_item.setChecked(true);
        } else {
            holder.checkBox_playlist_item.setChecked(false);
        }


        holder.checkBox_playlist_item.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                PlaylistItem itemsItem = DBHelper.getPlaylistItem(mediaItem.getMediaId());
                String message = null;
                if (itemsItem == null) {
                    message = DBHelper.createItemPlaylist(konten, mediaItem.getPlaylistItem());
                } else {
                    message = DBHelper.createItemPlaylist(konten, itemsItem);
                }

                notifyDataSetChanged();

                Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
            }
        });

        holder.textViewTitle.setText(konten.getTitle());

    }

    @Override
    public int getItemCount() {
        return listKonten.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public CheckBox checkBox_playlist_item;
        public TextView textViewTitle;

        public MyViewHolder(View view) {
            super(view);
            checkBox_playlist_item = view.findViewById(R.id.checkBox_playlist_item);
            textViewTitle = view.findViewById(R.id.tv_playlist_item);

        }
    }


}