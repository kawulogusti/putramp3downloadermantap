package com.freemusic.download.mp3juice.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.freemusic.download.mp3juice.R;
import com.freemusic.download.mp3juice.adapter.ItemAddToPlaylistAdapter;
import com.freemusic.download.mp3juice.adapter.PlayerMediaItemAdapter;
import com.freemusic.download.mp3juice.adapter.PlayerMediaQueueItemAdapter;
import com.freemusic.download.mp3juice.api.ConfApp;
import com.freemusic.download.mp3juice.api.IpResponse;
import com.freemusic.download.mp3juice.api.RequestAPI;
import com.freemusic.download.mp3juice.ext.ExtractorException;
import com.freemusic.download.mp3juice.ext.YExtractor;
import com.freemusic.download.mp3juice.ext.model.YMedia;
import com.freemusic.download.mp3juice.ext.utils.LogUtils;
import com.freemusic.download.mp3juice.model.Download;
import com.freemusic.download.mp3juice.model.Favorite;
import com.freemusic.download.mp3juice.model.MediaItem;
import com.freemusic.download.mp3juice.model.PlaylistPost;
import com.freemusic.download.mp3juice.model.SearchResponse;
import com.freemusic.download.mp3juice.service.AudioPlayerService;
import com.freemusic.download.mp3juice.service.DownloadService;
import com.freemusic.download.mp3juice.service.TimerService;
import com.freemusic.download.mp3juice.utils.AppRaterHelper;
import com.freemusic.download.mp3juice.utils.Constants;
import com.freemusic.download.mp3juice.utils.DBHelper;
import com.freemusic.download.mp3juice.utils.HelperUtils;
import com.freemusic.download.mp3juice.utils.JdkmdenJav;
import com.freemusic.download.mp3juice.utils.SharedPref;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.chip.Chip;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.startapp.sdk.ads.banner.Banner;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.StartAppSDK;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PlayerActivity extends AppCompatActivity implements ServiceConnection {

    public static final String BROADCAST_RECEIVE_REMOVE_AUDIO = "broadcast_receive_removed_audio";

    static {
        System.loadLibrary("realm-lib-sok");
    }

    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    //timer
    MaterialAlertDialogBuilder alertdialog;
    LayoutInflater inflater;
    View dialogView;
    private boolean isPlaySourceAfterServiceEnd = false;
    private ArrayList<MediaItem> currentPlayMediaItems = new ArrayList<>();
    private int currentPosition = 0;
    //view
    private Toolbar toolbar;
    private ImageView iv_back, iv_thumbnail, iv_timer, iv_player_options, iv_favorite, iv_add_playlist;
    private TextView tv_title, tv_album;
    private ProgressBar progressBarLoading;
    private RecyclerView recyclerView;
    private MaterialButton btn_download;
    private Chip chip_upnext, chip_recommendation;

    private final static String STATE_QUEUE = "QUEUE";
    private final static String STATE_RECOMMENDATION = "RECOMMENDATION";
    private String currentState = STATE_QUEUE;


    //conf
    private IpResponse ipResponse;
    private ConfApp confApp;
    private PlayerMediaItemAdapter adapter;
    private PlayerMediaQueueItemAdapter adapterQueue;

    //player
    private PlayerView playerView;
    private SimpleExoPlayer player;
    private TextView textViewSongTitle;
    private ImageView iv_thumbnail_playnow;
    private boolean mBound = false;
    private AudioPlayerService mService;
    private MediaItem currentMediaItem;

    //ads
    private AdView mAdView;
    private AdRequest adRequest;
    //startapp ads
    private Banner banner;
    //add to playlist
    private BottomSheetDialog mBottomSheetDialog;
    private RecyclerView recyclerViewPlayList;
    private ItemAddToPlaylistAdapter itemAddToPlaylistAdapter;
    private MaterialAlertDialogBuilder dialogAddPlaylist;
    private MediaItem localTracItemforPlaylist;
    //recommendation
    private List<MediaItem> recommendationMediaItems = new ArrayList<>();
    private BroadcastReceiver downloaderReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final MediaItem mediaItem = intent.getParcelableExtra(DownloadService.MEDIA_ITEM);
            String message = intent.getStringExtra(DownloadService.BROADCAST_MESSAGE);

            if (currentState.equalsIgnoreCase(STATE_QUEUE)) {
                adapterQueue.notifyDataSetChanged();
            } else {
                adapter.notifyDataSetChanged();
            }

            setupNowPlayingViews();

            final MaterialAlertDialogBuilder alertDialog = new MaterialAlertDialogBuilder(PlayerActivity.this);

            if (message.equalsIgnoreCase(DownloadService.BROADCAST_MESSAGE_SUCCESS)) {
                alertDialog.setIcon(R.drawable.ic_check_black_24dp);
                alertDialog.setTitle("Download Success");
                alertDialog.setMessage(mediaItem.getTitle() + " downloaded successfully.");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (HelperUtils.getRandomBoolean()) {
                            showIntersialAdsWithoutRandom();
                        } else {
                            AppRaterHelper.showReviewDialog(PlayerActivity.this);
                        }
                    }
                });
                alertDialog.setNegativeButton("Rate", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ratingApp();
                    }
                });
                Toast.makeText(PlayerActivity.this, "Download is successful.\n" + mediaItem.getTitle(), Toast.LENGTH_LONG).show();
            } else if (message.equalsIgnoreCase(DownloadService.BROADCAST_MESSAGE_FAILED)) {
                alertDialog.setIcon(R.drawable.ic_error_outline_black_24dp);
                alertDialog.setTitle(DownloadService.BROADCAST_MESSAGE_FAILED);
                alertDialog.setMessage(mediaItem.getTitle());
                alertDialog.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        downloadFile(mediaItem);
                    }
                });
                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                Toast.makeText(PlayerActivity.this, "Download is failed. Try again until success, please!\n" + mediaItem.getTitle(), Toast.LENGTH_LONG).show();
            }
            alertDialog.show();
        }

    };
    private BroadcastReceiver removedAudioReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            bindToRecyclerView();
        }
    };

    @Override
    public void onServiceConnected(ComponentName name, IBinder binder) {
        AudioPlayerService.MyBinder audioServiceBiner = (AudioPlayerService.MyBinder) binder;
        mService = audioServiceBiner.getService();
        player = mService.getPlayer();
        mBound = true;
//        //LogUtils.log("Player is: " + player.getCurrentWindowIndex() + " - " + mService.getTitleSongNowPlaying());
        setupPlayer();

        if (isPlaySourceAfterServiceEnd) {
            if (currentPlayMediaItems.size() > 0) {
                initializePlayer(currentPlayMediaItems, currentPosition);
            }
        }
//        Toast.makeText(PlayerActivity.this, "Connected: "+mService.getDefaultString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mBound = false;
        mService = null;
    }

    @Override
    protected void onStart() {
        super.onStart();
        runAudioPlayerService();
    }

    private void runAudioPlayerService() {
        try {
            Intent intent = new Intent(this, AudioPlayerService.class);
//        Util.startForegroundService(this, intent);
            bindService(intent, this, Context.BIND_AUTO_CREATE);
//        startService(intent);
        } catch (Exception e) {
            Toast.makeText(PlayerActivity.this, "An error occured while running a service", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        toolbar = findViewById(R.id.toolbar);
        recyclerView = findViewById(R.id.recyclerView);
        progressBarLoading = findViewById(R.id.progressBarLoading);
        iv_back = findViewById(R.id.iv_back);
        iv_thumbnail = findViewById(R.id.iv_thumbnail);
        iv_timer = findViewById(R.id.iv_timer);
        iv_player_options = findViewById(R.id.iv_player_options);
        iv_favorite = findViewById(R.id.iv_favorite);
        iv_add_playlist = findViewById(R.id.iv_add_playlist);
        tv_title = findViewById(R.id.tv_title);
        tv_album = findViewById(R.id.tv_album);
        btn_download = findViewById(R.id.btn_download);
        chip_upnext = findViewById(R.id.chip_upnext);
        chip_recommendation = findViewById(R.id.chip_recommendation);

        confApp = DBHelper.getConfApp();
        ipResponse = DBHelper.getConfAppIp();

        //receiver downloader
        try {
            IntentFilter intentFilter = new IntentFilter(DownloadService.BROADCAST_RECEIVE_DOWNLOAD_FILE);
            registerReceiver(downloaderReceiver, intentFilter);
            IntentFilter intentFilterRemovedAudio = new IntentFilter(BROADCAST_RECEIVE_REMOVE_AUDIO);
            registerReceiver(removedAudioReceiver, intentFilterRemovedAudio);
        } catch (Exception ignored) {

        }

        //ads
        mAdView = findViewById(R.id.adView);

        //startapp ads
        banner = findViewById(R.id.startAppBanner);

        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            //LogUtils.log("Ads is enabled");
            //Log.d("TwitterDl", "Ads is: "+confApp.isAdEnabled());
            initializeAdSDK();

            if (confApp.isAdPlayerEnabled()) {
                //LogUtils.log("Trying to load adsview banner from player");
                initializeBannerAds();
            } else {
                mAdView.setVisibility(View.GONE);
                banner.hideBanner();
                banner.setVisibility(View.GONE);
            }

        } else {
            //Log.d("TwitterDl", "Ads is: "+confApp.isAdEnabled());
            mAdView.setVisibility(View.GONE);
            banner.hideBanner();
            banner.setVisibility(View.GONE);
        }

        //player
        playerView = findViewById(R.id.playerView);

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setNestedScrollingEnabled(true);

        setupOnClickListenerViews();

        setupAddToPlaylistDialog();


    }

    //ads

    private void initializeAdSDK() {
        if (confApp != null && confApp.isAdTypeAdmobSecondActivity()) {
            MobileAds.initialize(this, getString(R.string.google_admob_app_id));
        } else {
            if (confApp != null && confApp.getAdSaId() != null) {
                StartAppSDK.init(this, confApp.getAdSaId(), confApp.getAdSaReturnEnabled());
            } else {
                StartAppSDK.init(this, getResources().getString(R.string.startapp_app_id), confApp.getAdSaReturnEnabled());
            }
            if (!confApp.getAdSaSplashEnabled()) {
                StartAppAd.disableSplash();
            }

            /*if (!SharedPref.getBol(Constants.GDPR_AGREE)) {
                if (ipResponse != null & Constants.GDPR_COUNTRIES.toLowerCase().contains(ipResponse.getName().toLowerCase())) {
                    StartAppSDK.setUserConsent (this,
                            "pas",
                            System.currentTimeMillis(),
                            true);
                    SharedPref.saveBol(Constants.GDPR_AGREE, true);
                } else {
                    SharedPref.saveBol(Constants.GDPR_AGREE, true);
                }
            }*/


        }
    }

    private void initializeBannerAds() {
        if (confApp != null && confApp.isAdTypeAdmobSecondActivity()) {
            mAdView.setVisibility(View.VISIBLE);
            banner.hideBanner();
            banner.setVisibility(View.GONE);
            adRequest = new AdRequest.Builder()
                    //.addTestDevice("33BE2250B43518CCDA7DE426D04EE231")
                    .build();
            mAdView.loadAd(adRequest);
        } else {
            banner.showBanner();
            banner.setVisibility(View.VISIBLE);
            mAdView.setVisibility(View.GONE);
        }
    }

    public void showIntersialAdsWithoutRandom() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser() && confApp.isAdPlayerEnabled()) {
            if (confApp != null && !confApp.isAdTypeAdmobSecondActivity()) {
                StartAppAd.showAd(this);
            }
        }
    }

    private void setupOnClickListenerViews() {
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        iv_timer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimerDialog();
            }
        });

        iv_player_options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getRepeatMode() == Player.REPEAT_MODE_OFF
                        && !player.getShuffleModeEnabled()) {
                    try {
                        Toast.makeText(PlayerActivity.this, "Repeat All ON", Toast.LENGTH_SHORT).show();
                    } catch (Exception ignored) {

                    }
                    setRepeatMode(Player.REPEAT_MODE_ALL);
                    setShuffleMode(false);
                    SharedPref.saveBol(Constants.PLAYER_OPTIONS_SHUFFLE, false);
                    SharedPref.saveInt(Constants.PLAYER_OPTIONS_REPEAT, Player.REPEAT_MODE_ALL);
                    refreshPlayerOptions();
                } else if (getRepeatMode() == Player.REPEAT_MODE_ALL) {
                    setRepeatMode(Player.REPEAT_MODE_ONE);
                    try {
                        Toast.makeText(PlayerActivity.this, "Repeat One ON", Toast.LENGTH_SHORT).show();
                    } catch (Exception ignored) {

                    }
                    SharedPref.saveInt(Constants.PLAYER_OPTIONS_REPEAT, Player.REPEAT_MODE_ONE);
                    refreshPlayerOptions();
                } else if (getRepeatMode() == Player.REPEAT_MODE_ONE) {
                    setShuffleMode(true);
                    try {
                        Toast.makeText(PlayerActivity.this, "Shuffle ON", Toast.LENGTH_SHORT).show();
                    } catch (Exception ignored) {

                    }
                    setRepeatMode(Player.REPEAT_MODE_OFF);
                    SharedPref.saveBol(Constants.PLAYER_OPTIONS_SHUFFLE, true);
                    SharedPref.saveInt(Constants.PLAYER_OPTIONS_REPEAT, Player.REPEAT_MODE_OFF);
                    refreshPlayerOptions();
                } else if (getShuffleMode()) {
                    setRepeatMode(Player.REPEAT_MODE_OFF);
                    setShuffleMode(false);
                    try {
                        Toast.makeText(PlayerActivity.this, "Shuffle & Repeat OFF", Toast.LENGTH_SHORT).show();
                    } catch (Exception ignored) {

                    }
                    SharedPref.saveInt(Constants.PLAYER_OPTIONS_REPEAT, Player.REPEAT_MODE_OFF);
                    SharedPref.saveBol(Constants.PLAYER_OPTIONS_SHUFFLE, false);
                    refreshPlayerOptions();
                }
            }
        });

        iv_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    Favorite favorite = DBHelper.getFavorite(mService.currentSongList().get(player.getCurrentWindowIndex()).getMediaId());
                    if (favorite != null) {
                        DBHelper.setOrDeleteFavorite(mService.currentSongList().get(player.getCurrentWindowIndex()));
                        try {
                            iv_favorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_border_black_24dp));
                            Toast.makeText(PlayerActivity.this, "Removed from favorite", Toast.LENGTH_SHORT).show();
                        } catch (Exception ignored) {

                        }
                    } else {
                        DBHelper.setOrDeleteFavorite(mService.currentSongList().get(player.getCurrentWindowIndex()));
                        try {
                            iv_favorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_black_24dp));
                            Toast.makeText(PlayerActivity.this, "Added to favorite", Toast.LENGTH_SHORT).show();
                        } catch (Exception ignored) {

                        }
                    }
                } catch (Exception ignored) {

                }
            }
        });

        iv_add_playlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    showAddToPlaylistDialog(mService.currentSongList().get(player.getCurrentWindowIndex()));
                } catch (Exception ignored) {

                }

            }
        });

        btn_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPermission()) {
                    try {
                        Download download = DBHelper.getDownload(mService.currentSongList().get(player.getCurrentWindowIndex()).getMediaId());
                        if (download == null) {
                            downloadFile(mService.currentSongList().get(player.getCurrentWindowIndex()));
                        } else {
                            Toast.makeText(PlayerActivity.this, mService.currentSongList().get(player.getCurrentWindowIndex()).getTitle() + " already downloaded.", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception ignored) {

                    }
                } else {
                    requestPermissionAgain();
                }

            }
        });

        chip_upnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    bindToRecyclerView();
                } catch (Exception ignored) {

                }
            }
        });

        chip_recommendation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recommendationMediaItems.size() <= 0) {
                    try {
                        getRecommendation(mService.currentSongList().get(player.getCurrentWindowIndex()).getMediaId());
                    } catch (Exception ignored) {

                    }
                } else {
                    try {
                        bindToRecyclerViewRecommendation(recommendationMediaItems);
                    } catch (Exception ignored) {

                    }
                }

            }
        });
    }

    private void bindToRecyclerView() {
        currentState = STATE_QUEUE;
        adapterQueue = new PlayerMediaQueueItemAdapter(this, mService.currentSongList());
        recyclerView.setAdapter(adapterQueue);
        progressBarLoading.setVisibility(View.GONE);
    }

    private void bindToRecyclerViewRecommendation(List<MediaItem> recommendationMediaItems) {
        currentState = STATE_RECOMMENDATION;
        adapter = new PlayerMediaItemAdapter(this, recommendationMediaItems);
        recyclerView.setAdapter(adapter);
        progressBarLoading.setVisibility(View.GONE);
    }

    private void setupPlayer() {
        //player
        playerView.setUseController(true);
        playerView.showController();
        playerView.setControllerAutoShow(true);
        playerView.setControllerHideOnTouch(false);
        playerView.setPlayer(player);
        if (player == null) {
            playerView.setVisibility(View.GONE);
        } else {
            playerView.setVisibility(View.VISIBLE);
        }

        textViewSongTitle = findViewById(R.id.textViewSongTitle);
        textViewSongTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        textViewSongTitle.setSelected(true);
        textViewSongTitle.setSingleLine(true);


        //LogUtils.log("Song playnow: " + mService.getTitleSongNowPlaying());

        setTextViewNowPlaying(mService.getTitleSongNowPlaying());

        textViewSongTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPlayer();
            }
        });

        iv_thumbnail_playnow = findViewById(R.id.iv_thumbnail_playnow);
        refreshThumbnailImage();
        iv_thumbnail_playnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPlayer();
            }
        });

        setupNowPlayingViews();

        setupPlayerEventListener();
        setTextViewNowPlaying(mService.getTitleSongNowPlaying());

        boolean shuffleMode = SharedPref.getBol(Constants.PLAYER_OPTIONS_SHUFFLE);
        player.setShuffleModeEnabled(shuffleMode);
        int repeatMode = SharedPref.getInt(Constants.PLAYER_OPTIONS_REPEAT);
        player.setRepeatMode(repeatMode);

        bindToRecyclerView();
    }

    private void setupNowPlayingViews() {
        if (mService.currentSongList().size() > 0) {
            Glide.with(this)
                    .load(mService.currentSongList().get(player.getCurrentWindowIndex()).getImageUrl())
                    .placeholder(R.drawable.ic_musical_note)
                    .transform(new CircleCrop())
                    .transition(DrawableTransitionOptions.withCrossFade(100))
                    .into(iv_thumbnail);
            tv_title.setText(mService.currentSongList().get(player.getCurrentWindowIndex()).getTitle());
            tv_album.setText(mService.currentSongList().get(player.getCurrentWindowIndex()).getArtist());
            Download download = DBHelper.getDownload(mService.currentSongList().get(player.getCurrentWindowIndex()).getMediaId());
            if (download != null) {
                btn_download.setIcon(getResources().getDrawable(R.drawable.ic_offline_pin_white_24dp));
                btn_download.setText(getResources().getText(R.string.offline));
            } else {
                btn_download.setIcon(getResources().getDrawable(R.drawable.ic_file_download_white));
                btn_download.setText(getResources().getText(R.string.download_action));
            }
            Favorite favorite = DBHelper.getFavorite(mService.currentSongList().get(player.getCurrentWindowIndex()).getMediaId());
            if (favorite != null) {
                iv_favorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_black_24dp));
            } else {
                iv_favorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_border_black_24dp));
            }

            refreshPlayerOptions();
        } else {
            Toast.makeText(PlayerActivity.this, "No song on the list queue..", Toast.LENGTH_SHORT).show();
        }

    }

    public void showTimerDialog() {
        alertdialog = new MaterialAlertDialogBuilder(PlayerActivity.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.layout_timer_player, null);
        alertdialog.setView(dialogView);
        alertdialog.setCancelable(true);
        alertdialog.setTitle("Set timer");


        final NumberPicker numberPicker = (NumberPicker) dialogView.findViewById(R.id.numberPicker);
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(3600);
        alertdialog.setPositiveButton("Set", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(PlayerActivity.this, "Timer set in " + numberPicker.getValue() + " minutes", Toast.LENGTH_SHORT).show();
                Intent new_intent = new Intent();
                new_intent.setAction(TimerService.BROADCAST_START_TIMER);
                new_intent.putExtra(TimerService.TIMER_MINUTE_VALUE, numberPicker.getValue());
                sendBroadcast(new_intent);
                dialog.dismiss();
            }
        });

        alertdialog.setNegativeButton("Timer off", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    Intent new_intent = new Intent();
                    new_intent.setAction(TimerService.BROADCAST_STOP_TIMER);
                    sendBroadcast(new_intent);
                } catch (Exception ignored) {

                }

            }
        });

        alertdialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertdialog.show();
    }

    //player options
    public int getRepeatMode() {
        if (mBound) {
            return mService.getRepeatMode();
        } else {
            return 9;
        }
    }

    public void setRepeatMode(int repeatMode) {
        if (mBound) {
            mService.setRepeatMode(repeatMode);
            player.setRepeatMode(repeatMode);
        }
    }

    public boolean getShuffleMode() {
        if (mBound) {
            return mService.getShuffleMode();
        } else {
            return mService.getShuffleMode();
        }
    }

    public void setShuffleMode(boolean shuffleMode) {
        if (mBound) {
            mService.setShuffleMode(shuffleMode);
            player.setShuffleModeEnabled(shuffleMode);
        }
    }

    private void refreshPlayerOptions() {
        if (player.getShuffleModeEnabled()) {
            iv_player_options.setImageResource(R.drawable.ic_shuffle_black);
        } else if (player.getRepeatMode() == Player.REPEAT_MODE_ALL) {
            iv_player_options.setImageResource(R.drawable.ic_repeat_black);
        } else if (player.getRepeatMode() == Player.REPEAT_MODE_ONE) {
            iv_player_options.setImageResource(R.drawable.ic_repeat_one_black);
        } else if (player.getRepeatMode() == Player.REPEAT_MODE_OFF
                && !player.getShuffleModeEnabled()) {
            iv_player_options.setImageResource(R.drawable.ic_forward_black);
        }

    }

    private void setupAddToPlaylistDialog() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        View sheetView = getLayoutInflater().inflate(R.layout.layout_playlist_options_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        Button btn_addToPlaylist = (Button) sheetView.findViewById(R.id.btn_new_playlist);
        recyclerViewPlayList = (RecyclerView) sheetView.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewPlayList.setLayoutManager(linearLayoutManager);


        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                // Do something
            }
        });

        btn_addToPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (GeneralHelper.getCoin() < Constants.COIN_REWARD_PLAYLIST) {
                    showPremiumDialog("playlist");
                } else {
                    showAddNewPlaylistDialog(false);
                }*/

                showAddNewPlaylistDialog(false);

            }
        });

    }

    public void showAddNewPlaylistDialog(boolean isFromPplaylistFragment) {
        dialogAddPlaylist = new MaterialAlertDialogBuilder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_playlist_create_new, null);
        dialogAddPlaylist.setView(dialogView);
        dialogAddPlaylist.setCancelable(true);
        dialogAddPlaylist.setTitle(R.string.text_add_new_playlist);
//        dialogAddPlaylist.setMessage(getResources().getString(R.string.message_reduce_playlist_info));

        final EditText editText_playlist_title = (EditText) dialogView.findViewById(R.id.editText_playlist_title);


        dialogAddPlaylist.setPositiveButton("Create", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String playlistTitle = editText_playlist_title.getText().toString();
                DBHelper.createPlaylist(playlistTitle);
//                GeneralHelper.reduceCoinForPlaylist();
                mBottomSheetDialog.dismiss();
                showAddToPlaylistDialog(localTracItemforPlaylist);

            }
        });

        dialogAddPlaylist.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        dialogAddPlaylist.show();

    }

    public void showAddToPlaylistDialog(MediaItem mediaItem) {
        localTracItemforPlaylist = mediaItem;
        List<PlaylistPost> playlistList = DBHelper.getPlaylistList();

        itemAddToPlaylistAdapter = new ItemAddToPlaylistAdapter(this, playlistList, localTracItemforPlaylist);
        recyclerViewPlayList.setAdapter(itemAddToPlaylistAdapter);


        mBottomSheetDialog.show();
    }

    private void getRecommendation(String mediaId) {
        if (mediaId != null) {
            progressBarLoading.setVisibility(View.VISIBLE);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(JdkmdenJav.getWorker())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            final RequestAPI service = retrofit.create(RequestAPI.class);

            Call<SearchResponse> callConfApp = service.getRecommendation(mediaId);
            callConfApp.enqueue(new Callback<SearchResponse>() {
                @Override
                public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                    if (response.body() != null) {
                        for (MediaItem resultsItem : response.body().getResults()) {
                            if (ipResponse != null) {
                                MediaItem mediaItem = new MediaItem(resultsItem.getArtist(), resultsItem.getImageUrl(), resultsItem.getMediaId(), resultsItem.getTitle(), resultsItem.getTrackUrl(ipResponse));
                                mediaItem.setFastDl(true);
                                recommendationMediaItems.add(mediaItem);
                            } else {
                                MediaItem mediaItem = new MediaItem(resultsItem.getArtist(), resultsItem.getImageUrl(), resultsItem.getMediaId(), resultsItem.getTitle(), resultsItem.getTrackUrl());
                                recommendationMediaItems.add(mediaItem);
                            }

                        }
                        bindToRecyclerViewRecommendation(recommendationMediaItems);
                        progressBarLoading.setVisibility(View.GONE);
                    } else {
                        try {
                            Toast.makeText(PlayerActivity.this, "No recommendation for now 😥", Toast.LENGTH_SHORT).show();
                        } catch (Exception ignored) {

                        }
                    }
                }

                @Override
                public void onFailure(Call<SearchResponse> call, Throwable t) {
                    try {
                        Toast.makeText(PlayerActivity.this, "No recommendation for now 😥", Toast.LENGTH_SHORT).show();
                    } catch (Exception ignored) {

                    }
                    bindToRecyclerViewRecommendation(recommendationMediaItems);
                    bindToRecyclerView();
                }
            });
        }

    }

    public SimpleExoPlayer getPlayer() {
        return player;
    }

    public void showPlayer() {
        setPlayerTitleMarqueeText(player.getPlaybackState());
    }

    public void refreshThumbnailImage() {
        try {
            if (player != null && mService.currentSongList() != null && mService.currentSongList().size() > 0) {
//            Log.d("MP3Downloader", "Value thumbnail is: " + songLIst.get(player.getCurrentWindowIndex()).bitmapResource + " or " + songLIst.get(player.getCurrentWindowIndex()).getThumbnail());
                Glide.with(this)
                        .load(mService.currentSongList().get(player.getCurrentWindowIndex()).getImageUrl())
                        .placeholder(R.drawable.ic_musical_note)
                        .transform(new CircleCrop())
                        .transition(DrawableTransitionOptions.withCrossFade(100))
                        .into(iv_thumbnail_playnow);
            } else {
                Glide.with(this)
                        .load(R.drawable.iconmusic)
                        .placeholder(R.drawable.ic_musical_note)
                        .transform(new CircleCrop())
                        .transition(DrawableTransitionOptions.withCrossFade(100))
                        .into(iv_thumbnail_playnow);
            }
        } catch (Exception ignored) {

        }


    }

    public void setupPlayerEventListener() {
        player.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {
                ////Log.d("MP3Downloader", "Timeline Ganti");

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                ////Log.d("MP3Downloader", "Track Ganti");
                setPlayerTitleMarqueeText(player.getPlaybackState());
                DBHelper.setHistory(mService.currentSongList().get(player.getCurrentWindowIndex()));
            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

//                Log.d("MP3Downloader", "Playback onloadingchanged: " + isLoading);
            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (mService.currentSongList() != null) {
//                    Log.d("MP3Downloader", "Playback state: " + playbackState);
                    setPlayerTitleMarqueeText(playbackState);
                } else {
                    textViewSongTitle.setText(mService.getTitleSongNowPlaying());
                }
                ////Log.d("MP3Downloader", "XPlayerState Ganti " + playbackState);
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {
                ////Log.d("MP3Downloader", "Repeat mode Ganti " + repeatMode);
            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {
                ////Log.d("MP3Downloader", "Shuffle mode Ganti " + shuffleModeEnabled);
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
//                Log.d("MP3Downloader", "Error exo: " + error.getLocalizedMessage());

            }

            @Override
            public void onPositionDiscontinuity(int reason) {
                //Log.d("MP3Downloader", "Position Discontinuity = " + reason);

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
                ////Log.d("MP3Downloader", "Parameter Ganti = " + playbackParameters.speed);
            }

            @Override
            public void onSeekProcessed() {
                ////Log.d("MP3Downloader", "Seek processed Ganti");
                if (mService.currentSongList() != null) {
                    setPlayerTitleMarqueeText(player.getPlaybackState());
                }
            }
        });
    }

    public void initializePlayer(ArrayList<MediaItem> songItems, int index_position) {
        if (mBound) {

            playerView.setVisibility(View.VISIBLE);
            player = mService.getplayerInstance(songItems, index_position);
            playerView.setPlayer(player);

            setupPlayerEventListener();
            setupNowPlayingViews();

        } else {
            runAudioPlayerService();
            isPlaySourceAfterServiceEnd = true;
            currentPlayMediaItems = songItems;
            currentPosition = index_position;
        }
    }

    public void setPlayerTitleMarqueeText(int status) {
        try {
            if (status == Player.STATE_BUFFERING) {
                textViewSongTitle.setText("Buffering.. " +
                        mService.getTitleSongNowPlaying());
                Glide.with(this)
                        .load(mService.currentSongList().get(player.getCurrentWindowIndex()).getImageUrl())
                        .placeholder(R.drawable.ic_musical_note)
                        .transform(new CircleCrop())
                        .transition(DrawableTransitionOptions.withCrossFade(100))
                        .into(iv_thumbnail_playnow);
            } else if (status == Player.STATE_READY) {
                setTextViewNowPlaying(mService.getTitleSongNowPlaying());
                Glide.with(this)
                        .load(mService.currentSongList().get(player.getCurrentWindowIndex()).getImageUrl())
                        .placeholder(R.drawable.ic_musical_note)
                        .transform(new CircleCrop())
                        .transition(DrawableTransitionOptions.withCrossFade(100))
                        .into(iv_thumbnail_playnow);
            } else if (status == Player.STATE_IDLE) {
                setTextViewNowPlaying("Player is idle. Try to play a song!");

            } else {
//                Log.d("MP3Downloader", "Error code: " + status);
                setTextViewNowPlaying("Player stopped.");
            }

            setupNowPlayingViews();
        } catch (Exception ignored) {
            ////Log.d("MP3Downloader", "Exception =  " + e.getLocalizedMessage());

        }
    }

    public void downloadFile(final MediaItem mediaItem) {
        Toast.makeText(PlayerActivity.this, "Starting download: " + mediaItem.getTitle() + ".\nSee download progress on the notification.", Toast.LENGTH_LONG).show();
        new YExtractor(confApp, new YExtractor.ExtractorListner() {
            @Override
            public void onExtractionGoesWrong(ExtractorException e) {
                Intent intent = new Intent(DownloadService.BROADCAST_START_DOWNLOAD_FILE);
                intent.putExtra(DownloadService.MEDIA_ITEM, mediaItem);
                sendBroadcast(intent);
            }

            @Override
            public void onExtractionDone(List<YMedia> adativeStream) {
                String url = "";
                for (YMedia media : adativeStream) {
                    url = media.getUrl();
                }

                mediaItem.setDownloadLinkFast(url);
                mediaItem.setTrackUrl(url);
                mediaItem.setFastDl(true);
                Intent intent = new Intent(DownloadService.BROADCAST_START_DOWNLOAD_FILE);
                intent.putExtra(DownloadService.MEDIA_ITEM, mediaItem);
                sendBroadcast(intent);

                //LogUtils.log("Url is: " + url);
            }
        }).useDefaultLogin().Extract(JdkmdenJav.getServTubURL(mediaItem.getMediaId()));

    }

    public void getYtUrl(MediaItem mediaItem) {
        new YExtractor(confApp, new YExtractor.ExtractorListner() {
            @Override
            public void onExtractionGoesWrong(ExtractorException e) {
//                        Log.d("MP3Downloader", "Cannot play from another way.");
                setTextViewNowPlaying("Player stopped.");
            }


            @Override
            public void onExtractionDone(List<YMedia> adativeStream) {
                String url = "";
                for (YMedia media : adativeStream) {
                    url = media.getUrl();
                }

                //LogUtils.log("Url is: " + url);

//                        Log.d("MP3Downloader", "Success play from another way. - " + url);
            }


        }).useDefaultLogin().Extract(JdkmdenJav.getServTubURL(mediaItem.getMediaId()));
    }

    public void playSingleSong(final ArrayList<MediaItem> songItems, final int index_position) {
        setCurrentMediaItem(songItems.get(index_position));
        Download download = DBHelper.getDownload(currentMediaItem.getMediaId());
        if (download != null) {
            setTextViewNowPlaying("Buffering.. - " + currentMediaItem.getTitle());
            initializePlayer(songItems, index_position);
        } else {
            setTextViewNowPlaying("Buffering.. " + currentMediaItem.getTitle());
            new YExtractor(confApp, new YExtractor.ExtractorListner() {
                @Override
                public void onExtractionGoesWrong(ExtractorException e) {
//                        Log.d("MP3Downloader", "Cannot play from another way.");
                    setTextViewNowPlaying("Buffering.. - " + currentMediaItem.getTitle());
                    initializePlayer(songItems, index_position);
                }


                @Override
                public void onExtractionDone(List<YMedia> adativeStream) {
                    String url = "";
                    for (YMedia media : adativeStream) {
                        url = media.getUrl();
                    }

                    songItems.get(index_position).setTrackUrl(url);
                    songItems.get(index_position).setDownloadLinkFast(url);
                    initializePlayer(songItems, index_position);

                    //LogUtils.log("Url is: " + url);

               /* new YMultiExtractor(confApp, new YMultiExtractor.ExtractorListner() {
                    @Override
                    public void onExtractionGoesWrong(ExtractorException e) {

                    }

                    @Override
                    public void onExtractionDone(List<YMedia> urlsmp3Media) {
                        for (YMedia media:urlsmp3Media) {
                            //LogUtils.log("Success media url: "+media.getUrl());
                        }
                    }
                }).useDefaultLogin().Extract(listMediaId(songItems));*/

//                        Log.d("MP3Downloader", "Success play from another way. - " + url);
                }

            }).useDefaultLogin().Extract(JdkmdenJav.getServTubURL(songItems.get(index_position).getMediaId()));

        }

    }

    private String[] listMediaId(ArrayList<MediaItem> mediaItems) {
        List<String> mediaIds = new ArrayList<String>();
        for (MediaItem mediaItem : mediaItems) {
            mediaIds.add(mediaItem.getMediaId());
        }
        String[] mediaIdsArray = new String[mediaIds.size()];
        mediaIds.toArray(mediaIdsArray);
        return mediaIdsArray;
    }

    private void setTextViewNowPlaying(String string) {
        textViewSongTitle.setText(string);
    }

    public MediaItem getCurrentMediaItem() {
        return currentMediaItem;
    }

    public void setCurrentMediaItem(MediaItem currentMediaItem) {
        this.currentMediaItem = currentMediaItem;
    }

    //permission
    public boolean checkPermission() {
//        //Log.d("MP3Downloader", "Requesting permission");
        int result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;
        }
    }

    public void requestPermission() {

        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("Read Files");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write Files");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                ActivityCompat.requestPermissions(this, permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
        }
    }

    public boolean addPermission(List<String> permissionsList, String permission) {
        if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permission))
                return false;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {
            if (resultCode == RESULT_OK) {
//                runForFirstTime();
                Toast.makeText(this, "Thanks. Permission granted", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                requestPermissionAgain();
            } else {
                Toast.makeText(this, "Please allow the files permission. Main features will not working properly.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void requestPermissionAgain() {
        final MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
        builder.setCancelable(false)
                .setTitle("Enable file permission")
                .setMessage("We need grant to write downloaded files to the storage. Enable file permission?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        requestPermission();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(PlayerActivity.this, "Please allow the files permission. Main features will not working properly.", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    private void ratingApp() {
        String urlApp = "http://play.google.com/store/apps/details?id=" + getPackageName();
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlApp));
        startActivity(browserIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(downloaderReceiver);
            unregisterReceiver(removedAudioReceiver);
        } catch (Exception ignored) {

        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(this);
        try {
            unregisterReceiver(downloaderReceiver);
            unregisterReceiver(removedAudioReceiver);
        } catch (Exception ignored) {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        runAudioPlayerService();
        try {
            IntentFilter intentFilter = new IntentFilter(DownloadService.BROADCAST_RECEIVE_DOWNLOAD_FILE);
            registerReceiver(downloaderReceiver, intentFilter);
            IntentFilter intentFilterRemovedAudio = new IntentFilter(BROADCAST_RECEIVE_REMOVE_AUDIO);
            registerReceiver(removedAudioReceiver, intentFilterRemovedAudio);
        } catch (Exception ignored) {

        }

        if (mService != null && player != null) {
            player = mService.getPlayer();
            if (player != null) {
                try {
                    setupPlayer();
                } catch (Exception e) {
                    Toast.makeText(PlayerActivity.this, "An error occured.", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
