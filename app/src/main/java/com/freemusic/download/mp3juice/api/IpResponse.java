package com.freemusic.download.mp3juice.api;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class IpResponse extends RealmObject {

    @SerializedName("country")
    private String country;

    @SerializedName("loc")
    private String loc;

    @SerializedName("name")
    private String name;

    @SerializedName("hostname")
    private String hostname;

    @SerializedName("city")
    private String city;

    @SerializedName("org")
    private String org;

    @SerializedName("timezone")
    private String timezone;

    @SerializedName("ip")
    private String ip;

    @SerializedName("postal")
    private String postal;

    @SerializedName("readme")
    private String readme;

    @SerializedName("region")
    private String region;

    @PrimaryKey
    private String uniqueId;

    private String packageName;

    private String appVersion;

    private String jkey;

    public String getJkey() {
        return jkey;
    }

    public void setJkey(String jkey) {
        this.jkey = jkey;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getReadme() {
        return readme;
    }

    public void setReadme(String readme) {
        this.readme = readme;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getURLEncoded() {
        return "&country=" + country + "&loc=" + loc + "&name=" + name + "&hostname=" + hostname + "&city=" + city + "&org=" + org + "&timezone=" + timezone + "&ip=" + ip + "&postal=" + postal + "&region=" + region + "&uniqueId=" + uniqueId + "&packageName=" + packageName + "&appVersion=" + appVersion + "&key=" + jkey;
    }

    @Override
    public String toString() {
        return
                "IpResponse{" +
                        "country = '" + country + '\'' +
                        ",loc = '" + loc + '\'' +
                        ",hostname = '" + hostname + '\'' +
                        ",city = '" + city + '\'' +
                        ",org = '" + org + '\'' +
                        ",timezone = '" + timezone + '\'' +
                        ",ip = '" + ip + '\'' +
                        ",postal = '" + postal + '\'' +
                        ",readme = '" + readme + '\'' +
                        ",region = '" + region + '\'' +
                        ",country name = '" + name + '\'' +
                        ",uniqueId = '" + uniqueId + '\'' +
                        ",appVersion = '" + appVersion + '\'' +
                        ",packageName = '" + packageName + '\'' +
                        ",jkey = '" + jkey + '\'' +
                        "}";
    }

	/*@SerializedName("name")
	private String name;

	@PrimaryKey
	@SerializedName("code")
	private String code;

	@SerializedName("ipaddress")
	private String ipaddress;


	public IpResponse() {

	}

	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}


	@Override
 	public String toString(){
		return 
			"IpResponse{" +
					",name = '" + name + '\'' +
					",code = '" + code + '\'' +
					",ipaddress = '" + ipaddress + '\'' +
					"}";
		}*/
}