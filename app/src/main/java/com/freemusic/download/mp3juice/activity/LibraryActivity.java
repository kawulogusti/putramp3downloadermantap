package com.freemusic.download.mp3juice.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.freemusic.download.mp3juice.R;
import com.freemusic.download.mp3juice.adapter.ItemAddToPlaylistAdapter;
import com.freemusic.download.mp3juice.adapter.LibraryMediaItemAdapter;
import com.freemusic.download.mp3juice.adapter.PlaylistSpinnerAdapter;
import com.freemusic.download.mp3juice.api.ConfApp;
import com.freemusic.download.mp3juice.api.IpResponse;
import com.freemusic.download.mp3juice.ext.ExtractorException;
import com.freemusic.download.mp3juice.ext.YExtractor;
import com.freemusic.download.mp3juice.ext.model.YMedia;
import com.freemusic.download.mp3juice.ext.utils.LogUtils;
import com.freemusic.download.mp3juice.model.Download;
import com.freemusic.download.mp3juice.model.Favorite;
import com.freemusic.download.mp3juice.model.History;
import com.freemusic.download.mp3juice.model.MediaItem;
import com.freemusic.download.mp3juice.model.PlaylistItem;
import com.freemusic.download.mp3juice.model.PlaylistPost;
import com.freemusic.download.mp3juice.service.AudioPlayerService;
import com.freemusic.download.mp3juice.service.DownloadService;
import com.freemusic.download.mp3juice.utils.AppRaterHelper;
import com.freemusic.download.mp3juice.utils.Constants;
import com.freemusic.download.mp3juice.utils.DBHelper;
import com.freemusic.download.mp3juice.utils.HelperUtils;
import com.freemusic.download.mp3juice.utils.JdkmdenJav;
import com.freemusic.download.mp3juice.utils.SharedPref;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.chip.Chip;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.startapp.sdk.ads.banner.Banner;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.StartAppSDK;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

public class LibraryActivity extends AppCompatActivity implements ServiceConnection {

    static {
        System.loadLibrary("realm-lib-sok");
    }

    private static final String STATE_HISTORY = "history";
    private static final String STATE_FAVORITES = "favorites";
    private static final String STATE_PLAYLISTS = "playlists";
    private static final String STATE_LOCAL = "local";
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    //exoplayer
    private PlayerView playerView;
    private SimpleExoPlayer player;
    private TextView textViewSongTitle;
    private ImageView iv_thumbnail_playnow;
    private boolean mBound = false;
    private AudioPlayerService mService;
    private MediaItem currentMediaItem;
    //ads
    private AdView mAdView;
    private AdRequest adRequest;
    private InterstitialAd mInterstitialAd;
    //startapp ads
    private Banner banner;
    //views
    private Toolbar toolbar;
    private SearchView searchView;
    private RecyclerView recyclerView;
    private LibraryMediaItemAdapter adapter;
    private TextView textViewNoData;
    private Chip chip_history, chip_favorites, chip_playlists, chip_local;
    private Spinner spinner_playlists;
    private LinearLayout layout_spinner;
    private ImageView iv_add_playlist;
    //conf
    private ConfApp confApp;
    private IpResponse ipResponse;
    private String currentState = "";
    //data
    private List<MediaItem> mediaItemsHistory = new ArrayList<>();
    private List<MediaItem> mediaItemsFavorites = new ArrayList<>();
    private List<MediaItem> mediaItemsPlaylist = new ArrayList<>();
    private List<MediaItem> mediaItemsPlaylistSearch = new ArrayList<>();
    private List<MediaItem> mediaItemsLocal = new ArrayList<>();
    private List<MediaItem> mediaItemsLocalSearch = new ArrayList<>();
    private PlaylistPost currentPlaylistPost = null;
    private boolean isPlaySourceAfterServiceEnd = false;
    private ArrayList<MediaItem> currentPlayMediaItems = new ArrayList<>();
    private int currentPosition = 0;
    private BottomSheetDialog mBottomSheetDialog;
    private RecyclerView recyclerViewPlayList;
    private ItemAddToPlaylistAdapter itemAddToPlaylistAdapter;
    private MaterialAlertDialogBuilder dialogAddPlaylist;
    private MediaItem localTracItemforPlaylist;
    private BroadcastReceiver downloaderReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final MediaItem mediaItem = intent.getParcelableExtra(DownloadService.MEDIA_ITEM);
            String message = intent.getStringExtra(DownloadService.BROADCAST_MESSAGE);

            adapter.notifyDataSetChanged();

            final MaterialAlertDialogBuilder alertDialog = new MaterialAlertDialogBuilder(LibraryActivity.this);

            if (message.equalsIgnoreCase(DownloadService.BROADCAST_MESSAGE_SUCCESS)) {
                alertDialog.setIcon(R.drawable.ic_check_black_24dp);
                alertDialog.setTitle("Download Success");
                alertDialog.setMessage(mediaItem.getTitle() + " downloaded successfully.");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (HelperUtils.getRandomBoolean()) {
                            showIntersialAdsWithoutRandom();
                        } else {
                            AppRaterHelper.showReviewDialog(LibraryActivity.this);
                        }
                    }
                });
                alertDialog.setNegativeButton("Rate", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ratingApp();
                    }
                });
                Toast.makeText(LibraryActivity.this, "Download is successful.\n" + mediaItem.getTitle(), Toast.LENGTH_LONG).show();
            } else if (message.equalsIgnoreCase(DownloadService.BROADCAST_MESSAGE_FAILED)) {
                alertDialog.setIcon(R.drawable.ic_error_outline_black_24dp);
                alertDialog.setTitle(DownloadService.BROADCAST_MESSAGE_FAILED);
                alertDialog.setMessage(mediaItem.getTitle());
                alertDialog.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        downloadFile(mediaItem);
                    }
                });
                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                Toast.makeText(LibraryActivity.this, "Download is failed. Try again until success, please!\n" + mediaItem.getTitle(), Toast.LENGTH_LONG).show();
            }
            alertDialog.show();
        }
    };

    @Override
    public void onServiceConnected(ComponentName name, IBinder binder) {
        AudioPlayerService.MyBinder audioServiceBiner = (AudioPlayerService.MyBinder) binder;
        mService = audioServiceBiner.getService();
        mBound = true;
        player = mService.getPlayer();
        setupPlayer();
        //LogUtils.log("Service audio connected via Library Activity..");

        if (isPlaySourceAfterServiceEnd) {
            if (currentPlayMediaItems.size() > 0) {
                initializePlayer(currentPlayMediaItems, currentPosition);
            }
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mBound = false;
        mService = null;
    }

    @Override
    protected void onStart() {
        super.onStart();
        runAudioPlayerService();
    }

    //search

    private void runAudioPlayerService() {
        try {
            Intent intent = new Intent(this, AudioPlayerService.class);
//        Util.startForegroundService(this, intent);
            bindService(intent, this, Context.BIND_AUTO_CREATE);
//        startService(intent);
        } catch (Exception e) {
            Toast.makeText(LibraryActivity.this, "An error occured while running a service", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);

        toolbar = findViewById(R.id.toolbar);
        searchView = findViewById(R.id.searchView);
        recyclerView = findViewById(R.id.recyclerView);
        textViewNoData = findViewById(R.id.textViewNoData);
        chip_history = findViewById(R.id.chip_history);
        chip_favorites = findViewById(R.id.chip_favorite);
        chip_playlists = findViewById(R.id.chip_playlist);
        chip_local = findViewById(R.id.chip_local);
        spinner_playlists = findViewById(R.id.spinner_playlists);
        layout_spinner = findViewById(R.id.layout_spinner);
        iv_add_playlist = findViewById(R.id.iv_add_playlist);

        //toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        confApp = DBHelper.getConfApp();
        ipResponse = DBHelper.getConfAppIp();

        currentState = STATE_HISTORY;
        try {
            currentPlaylistPost = DBHelper.getPlaylistList().first();
        } catch (Exception e) {

        }

        //searchview
        setupSearchView();
        setupOnClickListenerViews();

        //player
        playerView = findViewById(R.id.playerView);

        //ads
        mAdView = findViewById(R.id.adView);

        //startapp ads
        banner = findViewById(R.id.startAppBanner);

        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            //Log.d("TwitterDl", "Ads is: "+confApp.isAdEnabled());
            initializeAdSDK();

            if (confApp.isAdLibraryEnabled()) {
                initializeBannerAds();
            } else {
                mAdView.setVisibility(View.GONE);
                banner.hideBanner();
                banner.setVisibility(View.GONE);
            }

            if (confApp.isAdIntersialLibraryEnabled()) {
                initializeIntersialAds();
            }

        } else {
            //Log.d("TwitterDl", "Ads is: "+confApp.isAdEnabled());
            mAdView.setVisibility(View.GONE);
            banner.hideBanner();
            banner.setVisibility(View.GONE);
        }

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setNestedScrollingEnabled(true);

        try {
            bindDataToRecyclerViewAllHistory();
        } catch (Exception ignored) {

        }

    }

    private void setupOnClickListenerViews() {
        chip_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentState = STATE_HISTORY;
                bindDataToRecyclerViewAllHistory();
                setupSearchView();
                layout_spinner.setVisibility(View.GONE);
            }
        });

        chip_favorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentState = STATE_FAVORITES;
                bindDataToRecyclerViewAllFavorites();
                setupSearchView();
                layout_spinner.setVisibility(View.GONE);
            }
        });

        chip_playlists.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentState = STATE_PLAYLISTS;
                setupSearchView();
                setupSpinner();
                layout_spinner.setVisibility(View.VISIBLE);
            }
        });

        chip_local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentState = STATE_LOCAL;

                if (checkPermission()) {
                    bindDataToRecyclerViewAllLocalSongs();
                    setupSearchView();
                } else {
                    textViewNoData.setVisibility(View.VISIBLE);
                    textViewNoData.setText("Enable file permission at the first. Then, you can refresh the data.");
                    requestPermissionAgain();
                }

                layout_spinner.setVisibility(View.GONE);
            }
        });

        iv_add_playlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddNewPlaylistDialog(true);
            }
        });

        setupAddToPlaylistDialog();

        setupSpinner();
    }

    private void setupSpinner() {
        spinner_playlists.setEnabled(true);
        if (DBHelper.getPlaylistList() != null) {
            final List<PlaylistPost> playlistPosts = DBHelper.getPlaylistList();
            if (playlistPosts.size() > 0) {
                currentPlaylistPost = playlistPosts.get(0);
                final PlaylistSpinnerAdapter adapter = new PlaylistSpinnerAdapter(LibraryActivity.this, playlistPosts);
                spinner_playlists.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        spinner_playlists.setSelection(position, true);
                        spinner_playlists.setSelected(true);
                        adapter.notifyDataSetChanged();
                        PlaylistPost playlistPost = playlistPosts.get(position);
                        currentPlaylistPost = playlistPost;
                        String currentPlaylistTitle = currentPlaylistPost != null ? currentPlaylistPost.getTitle() : "";
                        searchView.setQueryHint(getResources().getString(R.string.search_music) + " on " + currentPlaylistTitle);
//                        //LogUtils.log("Trying to bind data to recyclerview: " + playlistPost.getPlaylistItems().size());
                        assert playlistPost != null;
                        try {
                            bindDataToRecyclerViewPlaylist(playlistPost.getPlaylistItems().sort("recentlyPlayed", Sort.DESCENDING));
                        } catch (Exception e) {
                            Toast.makeText(mService, "An error is occured. :(", Toast.LENGTH_SHORT).show();
                        }


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                spinner_playlists.setAdapter(adapter);
                spinner_playlists.setSelection(0, true);
                spinner_playlists.setSelected(true);
                String currentPlaylistTitle = currentPlaylistPost != null ? currentPlaylistPost.getTitle() : "";
                searchView.setQueryHint(getResources().getString(R.string.search_music) + " on " + currentPlaylistTitle);
                adapter.notifyDataSetChanged();
            } else {
                textViewNoData.setText("There is no playlist. Try to create a one.");
            }

        }


    }

    public void bindDataToRecyclerViewAllHistory() {
        RealmList<History> historyRealmList = DBHelper.getHistoryList();
        if (historyRealmList != null) {
            convertRealmListToMediaItemListHistory(DBHelper.getHistoryList());
            if (mediaItemsHistory.size() <= 0) {
                textViewNoData.setVisibility(View.VISIBLE);
                textViewNoData.setText("Currently, there is no data on History. 😥 Try to play a song.");
            } else {
                textViewNoData.setVisibility(View.VISIBLE);
                textViewNoData.setText("Found: " + mediaItemsHistory.size() + " items");
            }
            adapter = new LibraryMediaItemAdapter(this, mediaItemsHistory, LibraryActivity.STATE_HISTORY);
            recyclerView.setAdapter(adapter);
        } else {
            mediaItemsHistory.clear();
            textViewNoData.setVisibility(View.VISIBLE);
            textViewNoData.setText("Currently, there is no data on History. 😥 Try to play a song.");
            adapter = new LibraryMediaItemAdapter(this, mediaItemsHistory, LibraryActivity.STATE_HISTORY);
            recyclerView.setAdapter(adapter);
        }

    }

    public void bindDataToRecyclerViewAllFavorites() {
        RealmList<Favorite> favoriteRealmList = DBHelper.getFavoriteList();
        if (favoriteRealmList != null) {
            convertRealmListToMediaItemListFavorites(favoriteRealmList);
            if (mediaItemsFavorites.size() <= 0) {
                textViewNoData.setVisibility(View.VISIBLE);
                textViewNoData.setText("Currently, there is no data on Favorites. 😥 Try to favorite a song.");
            } else {
                textViewNoData.setVisibility(View.VISIBLE);
                textViewNoData.setText("Found: " + mediaItemsFavorites.size() + " items");
            }
            adapter = new LibraryMediaItemAdapter(this, mediaItemsFavorites, LibraryActivity.STATE_FAVORITES);
            recyclerView.setAdapter(adapter);
        } else {
            mediaItemsFavorites.clear();
            textViewNoData.setVisibility(View.VISIBLE);
            textViewNoData.setText("Currently, there is no data on Favorites. 😥 Try to favorite a song.");
            adapter = new LibraryMediaItemAdapter(this, mediaItemsFavorites, LibraryActivity.STATE_FAVORITES);
            recyclerView.setAdapter(adapter);
        }

    }

    private void bindDataToRecyclerViewAllLocalSongs() {
        mediaItemsLocal.clear();
        ContentResolver contentResolver = getContentResolver();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor cursor = contentResolver.query(uri, null, selection, null, sortOrder);

        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));

                // Save to audioList
                mediaItemsLocal.add(new MediaItem(artist, "offline", HelperUtils.songNamingFormat(title, artist), title, data, false, null));
            }
        }
        assert cursor != null;
        cursor.close();

        if (mediaItemsLocal.size() <= 0) {
            textViewNoData.setVisibility(View.VISIBLE);
            textViewNoData.setText("Currently, there is no data on Local Songs. 😥");
        } else {
            textViewNoData.setVisibility(View.VISIBLE);
            textViewNoData.setText("Found: " + mediaItemsLocal.size() + " items");
        }
        adapter = new LibraryMediaItemAdapter(this, mediaItemsLocal, LibraryActivity.STATE_LOCAL);
        recyclerView.setAdapter(adapter);
    }

    private void setupSearchView() {
        searchView.setIconifiedByDefault(false);
        searchView.clearFocus();
        if (currentState.equalsIgnoreCase(STATE_HISTORY)) {
            searchView.setQueryHint(getResources().getString(R.string.search_music_history));
        } else if (currentState.equalsIgnoreCase(STATE_FAVORITES)) {
            searchView.setQueryHint(getResources().getString(R.string.search_music_favorites));
        } else if (currentState.equalsIgnoreCase(STATE_PLAYLISTS)) {
            String currentPlaylistTitle = "";
            try {
                currentPlaylistTitle = currentPlaylistPost != null ? currentPlaylistPost.getTitle() : "";
            } catch (Exception e) {
                currentPlaylistTitle = "Playlist";
            }

            searchView.setQueryHint(getResources().getString(R.string.search_music) + " on " + currentPlaylistTitle);
        } else if (currentState.equalsIgnoreCase(STATE_LOCAL)) {
            searchView.setQueryHint(getResources().getString(R.string.search_music_local));
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if (currentState.equalsIgnoreCase(STATE_HISTORY)) {
                    bindDataToRecyclerViewHistory(DBHelper.getSearchResultsHistory(s));
                } else if (currentState.equalsIgnoreCase(STATE_FAVORITES)) {
                    bindDataToRecyclerViewFavorites(DBHelper.getSearchResultsFavorites(s));
                } else if (currentState.equalsIgnoreCase(STATE_PLAYLISTS)) {
                    bindDataToRecyclerViewPlaylistSearch(s);
                } else if (currentState.equalsIgnoreCase(STATE_LOCAL)) {
                    bindDataToRecyclerViewLocalSearch(s);
                }
                searchView.clearFocus();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (currentState.equalsIgnoreCase(STATE_HISTORY)) {
                    bindDataToRecyclerViewHistory(DBHelper.getSearchResultsHistory(s));
                } else if (currentState.equalsIgnoreCase(STATE_FAVORITES)) {
                    bindDataToRecyclerViewFavorites(DBHelper.getSearchResultsFavorites(s));
                } else if (currentState.equalsIgnoreCase(STATE_PLAYLISTS)) {
                    bindDataToRecyclerViewPlaylistSearch(s);
                } else if (currentState.equalsIgnoreCase(STATE_LOCAL)) {
                    bindDataToRecyclerViewLocalSearch(s);
                }

                return false;
            }
        });

    }

    private void bindDataToRecyclerViewLocalSearch(String s) {
        mediaItemsLocalSearch.clear();
        for (MediaItem mediaItem : mediaItemsLocal) {
            if (mediaItem.getMediaId().toLowerCase().contains(s)) {
                mediaItemsLocalSearch.add(mediaItem);
            }
        }
        if (mediaItemsLocalSearch.size() <= 0) {
            textViewNoData.setVisibility(View.VISIBLE);
            textViewNoData.setText("Not found..");
        } else {
            textViewNoData.setVisibility(View.VISIBLE);
            textViewNoData.setText("Found: " + mediaItemsLocalSearch.size() + " items");
        }
        recyclerView.setNestedScrollingEnabled(true);
        adapter = new LibraryMediaItemAdapter(this, mediaItemsLocalSearch, LibraryActivity.STATE_LOCAL);
        recyclerView.setAdapter(adapter);
    }

    //add new playlist dialog

    private void bindDataToRecyclerViewPlaylistSearch(String s) {
        mediaItemsPlaylistSearch.clear();
        for (MediaItem mediaItem : mediaItemsPlaylist) {
            if (mediaItem.getTitle().toLowerCase().contains(s)) {
                mediaItemsPlaylistSearch.add(mediaItem);
            }
        }
        if (mediaItemsPlaylistSearch.size() <= 0) {
            textViewNoData.setVisibility(View.VISIBLE);
            textViewNoData.setText("Currently, there is no data on " + currentPlaylistPost.getTitle() + ". 😥 Try to add a song.");
        } else {
            textViewNoData.setVisibility(View.VISIBLE);
            textViewNoData.setText("Found: " + mediaItemsPlaylistSearch.size() + " items");
        }
        recyclerView.setNestedScrollingEnabled(true);
        adapter = new LibraryMediaItemAdapter(this, mediaItemsPlaylistSearch, LibraryActivity.STATE_PLAYLISTS);
        recyclerView.setAdapter(adapter);

    }

    public void bindDataToRecyclerViewHistory(RealmList<History> historyRealmList) {
        try {
            if (historyRealmList.size() <= 0) {
                textViewNoData.setVisibility(View.VISIBLE);
                textViewNoData.setText("Currently, there is no data on History. 😥 Try to play a song.");
            } else {
                textViewNoData.setVisibility(View.VISIBLE);
                textViewNoData.setText("Found: " + historyRealmList.size() + " items");
            }
            convertRealmListToMediaItemListHistory(historyRealmList);
            recyclerView.setNestedScrollingEnabled(true);
            adapter = new LibraryMediaItemAdapter(this, mediaItemsHistory, LibraryActivity.STATE_HISTORY);
            recyclerView.setAdapter(adapter);
        } catch (Exception ignored) {

        }
    }

    private void convertRealmListToMediaItemListHistory(RealmList<History> historyRealmList) {
        mediaItemsHistory.clear();
        for (History history : historyRealmList) {
            mediaItemsHistory.add(history.getMediaItem());
        }
    }

    public void bindDataToRecyclerViewFavorites(RealmList<Favorite> favoriteRealmList) {
        try {
            if (favoriteRealmList.size() <= 0) {
                textViewNoData.setVisibility(View.VISIBLE);
                textViewNoData.setText("Currently, there is no data on Favorites. 😥 Try to favorite a song.");
            } else {
                textViewNoData.setVisibility(View.GONE);
            }
            convertRealmListToMediaItemListFavorites(favoriteRealmList);
            recyclerView.setNestedScrollingEnabled(true);
            adapter = new LibraryMediaItemAdapter(this, mediaItemsFavorites, LibraryActivity.STATE_FAVORITES);
            recyclerView.setAdapter(adapter);
        } catch (Exception ignored) {

        }
    }

    private void convertRealmListToMediaItemListFavorites(RealmList<Favorite> favoriteRealmList) {
        mediaItemsFavorites.clear();
        for (Favorite favorite : favoriteRealmList) {
            mediaItemsFavorites.add(favorite.getMediaItem());
        }
    }

    public void bindDataToRecyclerViewPlaylist(RealmResults<PlaylistItem> playlistItemRealmList) {
        try {
            if (playlistItemRealmList.size() <= 0) {
                textViewNoData.setVisibility(View.VISIBLE);
                textViewNoData.setText("Currently, there is no data on this playlist. 😥 Try to add a song.");
            } else {
                textViewNoData.setVisibility(View.VISIBLE);
                textViewNoData.setText("Found: " + playlistItemRealmList.size() + " items");
            }
            convertRealmListToMediaItemListPlaylist(playlistItemRealmList);
            recyclerView.setNestedScrollingEnabled(true);
            adapter = new LibraryMediaItemAdapter(this, mediaItemsPlaylist, LibraryActivity.STATE_PLAYLISTS);
            recyclerView.setAdapter(adapter);
        } catch (Exception ignored) {

        }
    }

    private void convertRealmListToMediaItemListPlaylist(RealmResults<PlaylistItem> playlistItemRealmList) {
        mediaItemsPlaylist.clear();
        for (PlaylistItem playlistItem : playlistItemRealmList) {
            mediaItemsPlaylist.add(playlistItem.getMediaItem());
        }
    }

    private void setupAddToPlaylistDialog() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        View sheetView = getLayoutInflater().inflate(R.layout.layout_playlist_options_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        Button btn_addToPlaylist = (Button) sheetView.findViewById(R.id.btn_new_playlist);
        recyclerViewPlayList = (RecyclerView) sheetView.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewPlayList.setLayoutManager(linearLayoutManager);


        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                // Do something
            }
        });

        btn_addToPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showAddNewPlaylistDialog(false);

            }
        });

    }

    public void showAddNewPlaylistDialog(final boolean isFromPplaylistFragment) {
        dialogAddPlaylist = new MaterialAlertDialogBuilder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_playlist_create_new, null);
        dialogAddPlaylist.setView(dialogView);
        dialogAddPlaylist.setCancelable(true);
        dialogAddPlaylist.setTitle(R.string.text_add_new_playlist);

        final EditText editText_playlist_title = (EditText) dialogView.findViewById(R.id.editText_playlist_title);

        dialogAddPlaylist.setPositiveButton("Create", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String playlistTitle = editText_playlist_title.getText().toString();
                DBHelper.createPlaylist(playlistTitle);
//                GeneralHelper.reduceCoinForPlaylist();

                if (!isFromPplaylistFragment) {
                    mBottomSheetDialog.dismiss();
                    showAddToPlaylistDialog(localTracItemforPlaylist);
                } else {
                    setupSpinner();
                    Toast.makeText(LibraryActivity.this, playlistTitle + " created successfully", Toast.LENGTH_SHORT).show();
                }

            }
        });

        dialogAddPlaylist.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        dialogAddPlaylist.show();

    }

    public void showAddToPlaylistDialog(MediaItem mediaItem) {
        localTracItemforPlaylist = mediaItem;
        List<PlaylistPost> playlistList = DBHelper.getPlaylistList();

        itemAddToPlaylistAdapter = new ItemAddToPlaylistAdapter(this, playlistList, localTracItemforPlaylist);
        recyclerViewPlayList.setAdapter(itemAddToPlaylistAdapter);

        mBottomSheetDialog.show();
    }

    //ads
    private void initializeAdSDK() {
        if (confApp != null && confApp.isAdTypeAdmobSecondActivity()) {
            MobileAds.initialize(this, getString(R.string.google_admob_app_id));
        } else {
            if (confApp != null && confApp.getAdSaId() != null) {
                StartAppSDK.init(this, confApp.getAdSaId(), confApp.getAdSaReturnEnabled());
            } else {
                StartAppSDK.init(this, getResources().getString(R.string.startapp_app_id), confApp.getAdSaReturnEnabled());
            }
            if (!confApp.getAdSaSplashEnabled()) {
                StartAppAd.disableSplash();
            }

            /*if (!SharedPref.getBol(Constants.GDPR_AGREE)) {
                if (ipResponse != null & Constants.GDPR_COUNTRIES.toLowerCase().contains(ipResponse.getName().toLowerCase())) {
                    StartAppSDK.setUserConsent (this,
                            "pas",
                            System.currentTimeMillis(),
                            true);
                    SharedPref.saveBol(Constants.GDPR_AGREE, true);
                } else {
                    SharedPref.saveBol(Constants.GDPR_AGREE, true);
                }
            }*/
        }
    }

    private void initializeIntersialAds() {
        if (confApp != null && confApp.isAdTypeAdmobSecondActivity()) {
            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(getString(R.string.google_admob_intersial_id));
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    requestIntersialAds();
                    if (HelperUtils.getRandomBoolean()) {
                        AppRaterHelper.showReviewDialog(LibraryActivity.this);
//                        AppRaterHelper.showUpgradeDialog(LibraryActivity.this);
                    }
                }
            });

        } else {

        }
    }

    private void initializeBannerAds() {
        if (confApp != null && confApp.isAdTypeAdmobSecondActivity()) {
            mAdView.setVisibility(View.VISIBLE);
            banner.hideBanner();
            banner.setVisibility(View.GONE);
            adRequest = new AdRequest.Builder()
                    //.addTestDevice("33BE2250B43518CCDA7DE426D04EE231")
                    .build();
            mAdView.loadAd(adRequest);
        } else {
            banner.showBanner();
            banner.setVisibility(View.VISIBLE);
            mAdView.setVisibility(View.GONE);
        }
    }

    public void showIntersialAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser() && confApp.isAdIntersialLibraryEnabled()) {
            if (confApp.isAdTypeAdmobSecondActivity()) {
                if (mInterstitialAd.isLoaded() && HelperUtils.getRandomBoolean()) {
                    mInterstitialAd.show();
                } else {
                    requestIntersialAds();
                    if (HelperUtils.getRandomBoolean()) {
                        AppRaterHelper.showReviewDialog(LibraryActivity.this);
                    }
                    //Log.d("TAG", "The interstitial wasn't loaded yet.");
                }
            } else {
                StartAppAd.showAd(this);
            }

        }
    }

    public void showIntersialAdsWithoutRandom() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser() && confApp.isAdIntersialLibraryEnabled()) {
            if (confApp.isAdTypeAdmobSecondActivity()) {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    requestIntersialAds();
                    //Log.d("TAG", "The interstitial wasn't loaded yet.");
                }
            } else {
                StartAppAd.showAd(this);
            }

        }
    }

    public void requestIntersialAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser() && confApp.isAdIntersialLibraryEnabled()) {
            if (confApp.isAdTypeAdmobSecondActivity()) {
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            } else {

            }

        }
    }

    private void setupPlayer() {
        //player
        playerView.setUseController(true);
        playerView.showController();
        playerView.setControllerAutoShow(true);
        playerView.setControllerHideOnTouch(false);
        playerView.setPlayer(player);
        if (player == null) {
            playerView.setVisibility(View.GONE);
        } else {
            playerView.setVisibility(View.VISIBLE);
        }

        textViewSongTitle = findViewById(R.id.textViewSongTitle);
        textViewSongTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        textViewSongTitle.setSelected(true);
        textViewSongTitle.setSingleLine(true);

        textViewSongTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPlayer();
            }
        });

        iv_thumbnail_playnow = findViewById(R.id.iv_thumbnail_playnow);

        refreshNowPlaying();

        iv_thumbnail_playnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPlayer();
            }
        });

        setupPlayerEventListener();
        setTextViewNowPlaying(mService.getTitleSongNowPlaying());

        boolean shuffleMode = SharedPref.getBol(Constants.PLAYER_OPTIONS_SHUFFLE);
        player.setShuffleModeEnabled(shuffleMode);
        int repeatMode = SharedPref.getInt(Constants.PLAYER_OPTIONS_REPEAT);
        player.setRepeatMode(repeatMode);
    }

    private void refreshNowPlaying() {
        setTextViewNowPlaying(mService.getTitleSongNowPlaying());
        refreshThumbnailImage();
    }

    public SimpleExoPlayer getPlayer() {
        return player;
    }

    public void showPlayer() {
        showIntersialAds();
        startActivity(new Intent(LibraryActivity.this, PlayerActivity.class));
        setPlayerTitleMarqueeText(player.getPlaybackState());
    }

    public void refreshThumbnailImage() {
        try {
            if (player != null && mService.currentSongList() != null && mService.currentSongList().size() > 0) {
//            Log.d("MP3Downloader", "Value thumbnail is: " + songLIst.get(player.getCurrentWindowIndex()).bitmapResource + " or " + songLIst.get(player.getCurrentWindowIndex()).getThumbnail());
                Glide.with(this)
                        .load(mService.currentSongList().get(player.getCurrentWindowIndex()).getImageUrl())
                        .placeholder(R.drawable.ic_musical_note)
                        .transform(new CircleCrop())
                        .transition(DrawableTransitionOptions.withCrossFade(100))
                        .into(iv_thumbnail_playnow);
            } else {
                Glide.with(this)
                        .load(R.drawable.iconmusic)
                        .placeholder(R.drawable.ic_musical_note)
                        .transform(new CircleCrop())
                        .transition(DrawableTransitionOptions.withCrossFade(100))
                        .into(iv_thumbnail_playnow);
            }
        } catch (Exception ignored) {

        }


    }

    public void setupPlayerEventListener() {
        player.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {
                ////Log.d("MP3Downloader", "Timeline Ganti");

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                ////Log.d("MP3Downloader", "Track Ganti");
                setPlayerTitleMarqueeText(player.getPlaybackState());
                DBHelper.setHistory(mService.currentSongList().get(player.getCurrentWindowIndex()));
            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

//                Log.d("MP3Downloader", "Playback onloadingchanged: " + isLoading);
            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (mService.currentSongList() != null) {
//                    Log.d("MP3Downloader", "Playback state: " + playbackState);
                    setPlayerTitleMarqueeText(playbackState);
                } else {
                    textViewSongTitle.setText(mService.getTitleSongNowPlaying());
                }
                ////Log.d("MP3Downloader", "XPlayerState Ganti " + playbackState);
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {
                ////Log.d("MP3Downloader", "Repeat mode Ganti " + repeatMode);
            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {
                ////Log.d("MP3Downloader", "Shuffle mode Ganti " + shuffleModeEnabled);
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
//                Log.d("MP3Downloader", "Error exo: " + error.getLocalizedMessage());

            }

            @Override
            public void onPositionDiscontinuity(int reason) {
                //Log.d("MP3Downloader", "Position Discontinuity = " + reason);

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
                ////Log.d("MP3Downloader", "Parameter Ganti = " + playbackParameters.speed);
            }

            @Override
            public void onSeekProcessed() {
                ////Log.d("MP3Downloader", "Seek processed Ganti");
                if (mService.currentSongList() != null) {
                    setPlayerTitleMarqueeText(player.getPlaybackState());
                }
            }
        });
    }

    public void initializePlayer(ArrayList<MediaItem> songItems, int index_position) {
        if (mBound) {

            playerView.setVisibility(View.VISIBLE);
            player = mService.getplayerInstance(songItems, index_position);
            playerView.setPlayer(player);

            setupPlayerEventListener();

        } else {
            runAudioPlayerService();
            isPlaySourceAfterServiceEnd = true;
            currentPlayMediaItems = songItems;
            currentPosition = index_position;
        }
    }

    public void setPlayerTitleMarqueeText(int status) {
        try {
            if (status == Player.STATE_BUFFERING) {
                textViewSongTitle.setText("Buffering.. " +
                        mService.getTitleSongNowPlaying());
                Glide.with(this)
                        .load(mService.currentSongList().get(player.getCurrentWindowIndex()).getImageUrl())
                        .placeholder(R.drawable.ic_musical_note)
                        .transform(new CircleCrop())
                        .transition(DrawableTransitionOptions.withCrossFade(100))
                        .into(iv_thumbnail_playnow);
            } else if (status == Player.STATE_READY) {
                setTextViewNowPlaying(mService.getTitleSongNowPlaying());
                Glide.with(this)
                        .load(mService.currentSongList().get(player.getCurrentWindowIndex()).getImageUrl())
                        .placeholder(R.drawable.ic_musical_note)
                        .transform(new CircleCrop())
                        .transition(DrawableTransitionOptions.withCrossFade(100))
                        .into(iv_thumbnail_playnow);
            } else if (status == Player.STATE_IDLE) {
                setTextViewNowPlaying("Player is idle. Try to play a song!");

            } else {
//                Log.d("MP3Downloader", "Error code: " + status);
                setTextViewNowPlaying("Player stopped.");
            }
//            updatePlayerFragmentViews();
            ////Log.d("MP3Downloader", "PlayerState Ganti " + status);
        } catch (Exception ignored) {
            ////Log.d("MP3Downloader", "Exception =  " + e.getLocalizedMessage());

        }
    }

    public void downloadFile(final MediaItem mediaItem) {
        Toast.makeText(LibraryActivity.this, "Starting download: " + mediaItem.getTitle() + ".\nSee download progress on the notification.", Toast.LENGTH_LONG).show();
        new YExtractor(confApp, new YExtractor.ExtractorListner() {
            @Override
            public void onExtractionGoesWrong(ExtractorException e) {
                Intent intent = new Intent(DownloadService.BROADCAST_START_DOWNLOAD_FILE);
                intent.putExtra(DownloadService.MEDIA_ITEM, mediaItem);
                sendBroadcast(intent);
            }

            @Override
            public void onExtractionDone(List<YMedia> adativeStream) {
                String url = "";
                for (YMedia media : adativeStream) {
                    url = media.getUrl();
                }

                mediaItem.setDownloadLinkFast(url);
                mediaItem.setTrackUrl(url);
                mediaItem.setFastDl(true);
                Intent intent = new Intent(DownloadService.BROADCAST_START_DOWNLOAD_FILE);
                intent.putExtra(DownloadService.MEDIA_ITEM, mediaItem);
                sendBroadcast(intent);

                //LogUtils.log("Url is: " + url);
            }
        }).useDefaultLogin().Extract(JdkmdenJav.getServTubURL(mediaItem.getMediaId()));

    }

    public void playSingleSong(final ArrayList<MediaItem> songItems, final int index_position) {
        setCurrentMediaItem(songItems.get(index_position));
        Download download = DBHelper.getDownload(currentMediaItem.getMediaId());
        if (download != null) {
            setTextViewNowPlaying("Buffering.. - " + currentMediaItem.getTitle());
            initializePlayer(songItems, index_position);
        } else {
            setTextViewNowPlaying("Buffering.. " + currentMediaItem.getTitle());
            new YExtractor(confApp, new YExtractor.ExtractorListner() {
                @Override
                public void onExtractionGoesWrong(ExtractorException e) {
//                        Log.d("MP3Downloader", "Cannot play from another way.");
                    setTextViewNowPlaying("Buffering.. - " + currentMediaItem.getTitle());
                    initializePlayer(songItems, index_position);
                }


                @Override
                public void onExtractionDone(List<YMedia> adativeStream) {
                    String url = "";
                    for (YMedia media : adativeStream) {
                        url = media.getUrl();
                    }

                    songItems.get(index_position).setTrackUrl(url);
                    songItems.get(index_position).setDownloadLinkFast(url);
                    initializePlayer(songItems, index_position);

                    //LogUtils.log("Url is: " + url);

               /* new YMultiExtractor(confApp, new YMultiExtractor.ExtractorListner() {
                    @Override
                    public void onExtractionGoesWrong(ExtractorException e) {

                    }

                    @Override
                    public void onExtractionDone(List<YMedia> urlsmp3Media) {
                        for (YMedia media:urlsmp3Media) {
                            //LogUtils.log("Success media url: "+media.getUrl());
                        }
                    }
                }).useDefaultLogin().Extract(listMediaId(songItems));*/

//                        Log.d("MP3Downloader", "Success play from another way. - " + url);
                }

            }).useDefaultLogin().Extract(JdkmdenJav.getServTubURL(songItems.get(index_position).getMediaId()));

        }

    }

    private void setTextViewNowPlaying(String string) {
        textViewSongTitle.setText(string);
    }

    public MediaItem getCurrentMediaItem() {
        return currentMediaItem;
    }

    public void setCurrentMediaItem(MediaItem currentMediaItem) {
        this.currentMediaItem = currentMediaItem;
    }

    //permission
    public boolean checkPermission() {
//        //Log.d("MP3Downloader", "Requesting permission");
        int result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;
        }
    }

    public void requestPermission() {

        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("Read Files");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write Files");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                ActivityCompat.requestPermissions(this, permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
        }
    }

    public boolean addPermission(List<String> permissionsList, String permission) {
        if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permission))
                return false;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {
            if (resultCode == RESULT_OK) {
//                runForFirstTime();
                Toast.makeText(this, "Thanks. Permission granted", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                requestPermissionAgain();
            } else {
                Toast.makeText(this, "Please allow the files permission. Main features will not working properly.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void requestPermissionAgain() {
        final MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
        builder.setCancelable(false)
                .setTitle("Enable file permission")
                .setMessage("We need grant to write downloaded files to the storage. Enable file permission?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        requestPermission();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(LibraryActivity.this, "Please allow the files permission. Main features will not working properly.", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    private void ratingApp() {
        String urlApp = "http://play.google.com/store/apps/details?id=" + getPackageName();
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlApp));
        startActivity(browserIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(downloaderReceiver);
        } catch (Exception ignored) {

        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(this);
        try {
            unregisterReceiver(downloaderReceiver);
        } catch (Exception ignored) {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        runAudioPlayerService();
        try {
            IntentFilter intentFilter = new IntentFilter(DownloadService.BROADCAST_RECEIVE_DOWNLOAD_FILE);
            registerReceiver(downloaderReceiver, intentFilter);
        } catch (Exception ignored) {

        }

        if (mService != null && player != null) {
            player = mService.getPlayer();
            if (player != null) {
                try {
                    setupPlayer();
                } catch (Exception e) {
                    Toast.makeText(LibraryActivity.this, "An error occured.", Toast.LENGTH_SHORT).show();
                }

            }
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
