package com.freemusic.download.mp3juice.ext.model;

import java.util.List;

public class PlayerResponse {
    private PlayabilityStatus playabilityStatus;
    private StreamingData streamingData;
    private YMeta videoDetails;
    private Captions captions;

    public Captions getCaptions() {
        return captions;
    }

    public void setCaptions(Captions captions) {
        this.captions = captions;
    }

    public PlayabilityStatus getPlayabilityStatus() {
        return playabilityStatus;
    }

    public void setPlayabilityStatus(PlayabilityStatus playabilityStatus) {
        this.playabilityStatus = playabilityStatus;
    }

    public StreamingData getStreamingData() {
        return streamingData;
    }

    public void setStreamingData(StreamingData streamingData) {
        this.streamingData = streamingData;
    }

    public YMeta getVideoDetails() {
        return videoDetails;
    }

    public void setVideoDetails(YMeta videoDetails) {
        this.videoDetails = videoDetails;
    }

    public class Captions {
        private PlayerCaptionsTracklistRenderer playerCaptionsTracklistRenderer;

        public PlayerCaptionsTracklistRenderer getPlayerCaptionsTracklistRenderer() {
            return playerCaptionsTracklistRenderer;
        }

        public void setPlayerCaptionsTracklistRenderer(PlayerCaptionsTracklistRenderer playerCaptionsTracklistRenderer) {
            this.playerCaptionsTracklistRenderer = playerCaptionsTracklistRenderer;
        }

        public class PlayerCaptionsTracklistRenderer {
            private List<YTSubtitles> captionTracks;

            public List<YTSubtitles> getCaptionTracks() {
                return captionTracks;
            }

            public void setCaptionTracks(List<YTSubtitles> captionTracks) {
                this.captionTracks = captionTracks;
            }
        }
    }


    public class PlayabilityStatus {
        private String status;
        private boolean playableInEmbed;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public boolean isPlayableInEmbed() {
            return playableInEmbed;
        }

        public void setPlayableInEmbed(boolean playableInEmbed) {
            this.playableInEmbed = playableInEmbed;
        }
    }
}


