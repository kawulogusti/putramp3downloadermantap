package com.freemusic.download.mp3juice.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;
import com.freemusic.download.mp3juice.R;
import com.freemusic.download.mp3juice.activity.LibraryActivity;
import com.freemusic.download.mp3juice.api.IpResponse;
import com.freemusic.download.mp3juice.ext.utils.LogUtils;
import com.freemusic.download.mp3juice.model.Download;
import com.freemusic.download.mp3juice.model.Favorite;
import com.freemusic.download.mp3juice.model.MediaItem;
import com.freemusic.download.mp3juice.service.AudioPlayerService;
import com.freemusic.download.mp3juice.utils.DBHelper;
import com.freemusic.download.mp3juice.utils.GlideApp;
import com.freemusic.download.mp3juice.utils.HelperUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ahm on 11/05/17.
 */

public class LibraryMediaItemAdapter extends RecyclerView.Adapter<LibraryMediaItemAdapter.MyViewHolder> {

    private Context mContext;
    private List<MediaItem> listKonten;
    private IpResponse ipResponse;
    private LibraryActivity activity;
    private String state;

    public static final String STATE_HISTORY = "history";
    public static final String STATE_FAVORITE = "favorite";
    public static final String STATE_PLAYLIST = "playlist";
    public static final String STATE_LOCAL_SONG = "playlist";


    public LibraryMediaItemAdapter(Context mContext, List<MediaItem> listKontenP, String state) {
        this.mContext = mContext;
        this.listKonten = listKontenP;
        this.ipResponse = DBHelper.getConfAppIp();
        this.activity = (LibraryActivity) mContext;
        this.state = state;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_mediaitem, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final MediaItem mediaItem = listKonten.get(position);

        GlideApp
                .with(mContext)
//                .load(Uri.parse(mediaItem.getImageUrl()))
                .load(mediaItem.getImageUrl())
                .error(R.drawable.musicicon_failed)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.imageViewThumbnail);

        holder.tv_title.setText(HelperUtils.songNamingFormat(mediaItem.getTitle(), mediaItem.getArtist()));
        holder.tv_artist.setText(mediaItem.getArtist());

        final Download download = DBHelper.getDownload(mediaItem.getMediaId());

        if (download != null || mediaItem.getImageUrl().contains("offline")) {
            holder.iv_download.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_offline_pin_black_24dp));
        } else {
            holder.iv_download.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_file_download_black_24dp));
        }


        holder.iv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMenu(mediaItem, v);
            }
        });


        holder.iv_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (activity.checkPermission()) {
                    if (download == null && !mediaItem.getImageUrl().contains("offline")) {
                        activity.downloadFile(mediaItem);
                        HelperUtils.saveLogsDownloads(ipResponse, mediaItem);
                    } else {
                        Toast.makeText(mContext, mediaItem.getTitle() + " already downloaded.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    activity.requestPermissionAgain();
                }

            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //LogUtils.log("Value is fast dl: " + mediaItem.isFastDl());
                if (mediaItem.isFastDl()) {
                    activity.playSingleSong(new ArrayList<>(listKonten), position);
                } else {
                    activity.initializePlayer(new ArrayList<>(listKonten), position);
                }
                HelperUtils.saveLogsPlays(ipResponse, mediaItem);
                Toast.makeText(mContext, "Playing.. " + mediaItem.getTitle(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void showPopupMenu(final MediaItem konten, View view) {
        PopupMenu popup = new PopupMenu(mContext, view);
        //inflating menu from xml resource
        if (this.state.equalsIgnoreCase(STATE_HISTORY)) {
            popup.inflate(R.menu.song_options_menu_history);
        } else {
            popup.inflate(R.menu.song_options_menu);
        }

        Menu menuOpts = popup.getMenu();

        final Favorite favorite = DBHelper.getFavorite(konten.getMediaId());
        if (favorite != null) {
            menuOpts.getItem(0).setTitle(R.string.remove_favorite);
        } else {
            menuOpts.getItem(0).setTitle(R.string.action_favorite);
        }
        //adding click listener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_playlist:
                        //handle menu2 click
                        activity.showAddToPlaylistDialog(konten);
//                        Toast.makeText(mContext, "Coming soon..", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.action_favorite:
                        //handle menu2 click
                        DBHelper.setOrDeleteFavorite(konten);
                        if (favorite != null) {
                            Toast.makeText(mContext, "Removed from favorite", Toast.LENGTH_SHORT).show();
                            if (state.equalsIgnoreCase(STATE_FAVORITE)) {
                                activity.bindDataToRecyclerViewAllFavorites();
                            }
                        } else {
                            Toast.makeText(mContext, "Added to favorite", Toast.LENGTH_SHORT).show();
                        }
                        notifyDataSetChanged();
                        return true;
                    case R.id.action_add_to_queue:
                        //handle menu2 click
                        Intent intent = new Intent(AudioPlayerService.BROADCAST_ADD_NEW_AUDIO);
                        intent.putExtra(AudioPlayerService.BROADCAST_ADD_NEW_AUDIO, konten);
                        mContext.sendBroadcast(intent);
                        return true;
                    case R.id.action_delete:
                        //handle menu2 click
                        DBHelper.deleteHistory(konten);
                        Toast.makeText(mContext, "History deleted", Toast.LENGTH_SHORT).show();
                        if (state.equalsIgnoreCase(STATE_HISTORY)) {
                            activity.bindDataToRecyclerViewAllHistory();
                        }
                        return true;

                    default:
                        return false;
                }
            }
        });
        //displaying the popup
        popup.show();
    }

    @Override
    public int getItemCount() {
        return listKonten.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageViewThumbnail, iv_download, iv_more;
        private TextView tv_duration, tv_title, tv_artist, tv_album;

        public MyViewHolder(View view) {
            super(view);
            imageViewThumbnail = view.findViewById(R.id.imageViewThumbnail);
            iv_download = view.findViewById(R.id.iv_download);
            iv_more = view.findViewById(R.id.iv_more);
            tv_duration = view.findViewById(R.id.tv_duration);
            tv_title = view.findViewById(R.id.tv_title);
            tv_artist = view.findViewById(R.id.tv_artist);
            tv_album = view.findViewById(R.id.tv_album);
        }
    }


}