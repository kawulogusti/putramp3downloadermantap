package com.freemusic.download.mp3juice.model;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PlaylistPost extends RealmObject {

    public RealmList<PlaylistItem> playlistItems;
    @PrimaryKey
    private String id;
    private String title;
    private Date date;

    public PlaylistPost() {
    }

    public PlaylistPost(String id, String title) {
        this.id = id;
        this.title = title;
        this.date = new Date();
    }

    public void addPlaylistItemObject(PlaylistItem playlistItem) {
        this.playlistItems.add(playlistItem);
    }

    public void removePlaylistItemObject(PlaylistItem playlistItem) {
        this.playlistItems.remove(playlistItem);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public RealmList<PlaylistItem> getPlaylistItems() {
        return playlistItems;
    }

    public void setPlaylistItems(RealmList<PlaylistItem> playlistItems) {
        this.playlistItems = playlistItems;
    }

}
