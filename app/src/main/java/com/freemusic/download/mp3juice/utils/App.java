package com.freemusic.download.mp3juice.utils;

import android.app.Application;
import android.content.Context;

import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;
import com.freemusic.download.mp3juice.R;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by ahm on 02/09/17.
 */

public class App extends Application {
    private static Context context;

    public static synchronized Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        context = getApplicationContext();

        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
                .setDatabaseEnabled(true)
                .build();
        PRDownloader.initialize(getApplicationContext(), config);

        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name(context.getResources().getString(R.string.app_name) + ".realm")
//                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
        Realm.getDefaultInstance();
        super.onCreate();


    }
}
