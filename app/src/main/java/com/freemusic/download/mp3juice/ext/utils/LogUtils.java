package com.freemusic.download.mp3juice.ext.utils;

import android.util.Log;

import com.freemusic.download.mp3juice.BuildConfig;


public class LogUtils {
    public static void log(String x) {
        if (BuildConfig.DEBUG) {
            Log.i("MP3Downloader", x);
        }
    }

    public static void log(int x) {
        if (BuildConfig.DEBUG) {
            Log.i("MP3Downloader", String.valueOf(x));
        }
    }

}
