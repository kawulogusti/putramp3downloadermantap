package com.freemusic.download.mp3juice.service;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.freemusic.download.mp3juice.R;
import com.freemusic.download.mp3juice.ext.utils.LogUtils;
import com.freemusic.download.mp3juice.model.Favorite;
import com.freemusic.download.mp3juice.model.MediaItem;
import com.freemusic.download.mp3juice.utils.DBHelper;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.ui.PlayerNotificationManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomActionNotification implements PlayerNotificationManager.CustomActionReceiver {

    public static final String CONSTANTS_ACTION = "constant_action";
    public static final String STATIC_ACTION_CLOSE = "Close";
    public static final int STATIC_ACTION_CLOSE_REQUEST_CODE = 9898;
    public static final String STATIC_ACTION_FAVORITE = "Favorite";
    public static final int STATIC_ACTION_FAVORITE_REQUEST_CODE = 9899;

    private Context context;
    private NotificationCompat.Action favorite;
    private NotificationCompat.Action close;
    private ArrayList<MediaItem> mediaItems = new ArrayList<>();

    public CustomActionNotification(Context context, ArrayList<MediaItem> mediaItems) {
        this.context = context;
        this.mediaItems = mediaItems;
    }

    @Override
    public Map<String, NotificationCompat.Action> createCustomActions(Context context) {
        this.context = context;
        Intent intentFavorite = new Intent(STATIC_ACTION_FAVORITE).setPackage(context.getPackageName());
        PendingIntent pendingIntentFavorite = PendingIntent.getBroadcast(
                context, STATIC_ACTION_FAVORITE_REQUEST_CODE, intentFavorite, PendingIntent.FLAG_CANCEL_CURRENT);
        favorite = new NotificationCompat.Action(
                R.drawable.ic_favorite_black_24dp,
                STATIC_ACTION_FAVORITE,
                pendingIntentFavorite
        );
        Intent intentClose = new Intent(STATIC_ACTION_CLOSE).setPackage(context.getPackageName());
        PendingIntent pendingIntentClose = PendingIntent.getBroadcast(
                context, STATIC_ACTION_CLOSE_REQUEST_CODE, intentClose, PendingIntent.FLAG_CANCEL_CURRENT);
        close = new NotificationCompat.Action(
                R.drawable.ic_close_black_24dp,
                STATIC_ACTION_CLOSE,
                pendingIntentClose
        );
        Map<String, NotificationCompat.Action> actionMap = new HashMap<>();
        actionMap.put(STATIC_ACTION_FAVORITE, favorite);
        actionMap.put(STATIC_ACTION_CLOSE, close);
        return actionMap;
    }

    @Override
    public List<String> getCustomActions(Player player) {
        List<String> customActions = new ArrayList<>();
        customActions.add(STATIC_ACTION_FAVORITE);
        customActions.add(STATIC_ACTION_CLOSE);
        return customActions;
    }

    @Override
    public void onCustomAction(Player player, String action, Intent intent) {
        if (action.equalsIgnoreCase(STATIC_ACTION_CLOSE)) {
            Intent intentStopMusicPlayerService = new Intent(AudioPlayerService.BROADCAST_STOP_MUSIC_PLAYER_SERVICE);
            this.context.sendBroadcast(intentStopMusicPlayerService);
            //LogUtils.log("action stop clicked");
        } else if (action.equalsIgnoreCase(STATIC_ACTION_FAVORITE)) {
            //LogUtils.log("action favorite clicked");
            MediaItem mediaItem = mediaItems.get(player.getCurrentWindowIndex());
            Favorite favoriteObject = DBHelper.getFavorite(mediaItem.getMediaId());
            DBHelper.setOrDeleteFavorite(mediaItems.get(player.getCurrentWindowIndex()));
            if (favoriteObject != null) {
                Toast.makeText(context, mediaItem.getTitle() + " removed from Favorites", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, mediaItem.getTitle() + " added to Favorites", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
