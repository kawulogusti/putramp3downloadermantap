package com.freemusic.download.mp3juice.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.freemusic.download.mp3juice.R;
import com.freemusic.download.mp3juice.activity.LibraryActivity;
import com.freemusic.download.mp3juice.model.PlaylistPost;
import com.freemusic.download.mp3juice.utils.Constants;
import com.freemusic.download.mp3juice.utils.DBHelper;
import com.freemusic.download.mp3juice.utils.SharedPref;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.List;

public class PlaylistSpinnerAdapter extends BaseAdapter {
    Context context;
    List<PlaylistPost> playlistPosts;
    LayoutInflater inflater;
    LibraryActivity activity;
    private MaterialAlertDialogBuilder dialogAddPlaylist;

    public PlaylistSpinnerAdapter(Context context, List<PlaylistPost> playlistPosts) {
        this.context = context;
        this.playlistPosts = playlistPosts;
        this.inflater = (LayoutInflater.from(context));
        this.activity = (LibraryActivity) context;
    }

    @Override
    public int getCount() {
        return playlistPosts.size();
    }

    @Override
    public Object getItem(int i) {
        return playlistPosts.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.item_playlistpost_spinner, null);
        ImageView more = view.findViewById(R.id.iv_more);
        TextView title = view.findViewById(R.id.tv_playlistpost_title);
        title.setText(playlistPosts.get(i).getTitle());
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddNewPlaylistDialog(playlistPosts.get(i));
            }
        });
        return view;
    }

    public void showAddNewPlaylistDialog(final PlaylistPost playlistPost) {
        dialogAddPlaylist = new MaterialAlertDialogBuilder(context);
        View dialogView = inflater.inflate(R.layout.layout_playlist_create_new, null);
        dialogAddPlaylist.setView(dialogView);
        dialogAddPlaylist.setCancelable(true);
        dialogAddPlaylist.setTitle(R.string.text_edit_playlist);

        final EditText editText_playlist_title = (EditText) dialogView.findViewById(R.id.editText_playlist_title);
        editText_playlist_title.setText(playlistPost.getTitle());

        dialogAddPlaylist.setPositiveButton("Edit", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String playlistTitle = editText_playlist_title.getText().toString();
                DBHelper.editPlaylist(playlistPost.getId(), playlistTitle);
                notifyDataSetChanged();
                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
            }
        });

        dialogAddPlaylist.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogAddPlaylist.setNeutralButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                PlaylistPost playlistPost1 = DBHelper.getPlaylistPost(playlistPost.getId());
                if (playlistPost1 != null && playlistPost1.getId().equalsIgnoreCase(SharedPref.getString(Constants.FIRST_CREATE_PLAYLIST))) {
                    Toast.makeText(context, "Unable to delete playlist. This is a mandatory playlist..", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        DBHelper.deletePlaylist(playlistPost.getId());
                        notifyDataSetChanged();
                        Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Toast.makeText(context, "Error while deleting playlist.", Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });

        dialogAddPlaylist.show();

    }
}
