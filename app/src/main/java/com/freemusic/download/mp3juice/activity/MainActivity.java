package com.freemusic.download.mp3juice.activity;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.BaseColumns;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.cursoradapter.widget.SimpleCursorAdapter;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.freemusic.download.mp3juice.R;
import com.freemusic.download.mp3juice.api.ConfApp;
import com.freemusic.download.mp3juice.api.IpResponse;
import com.freemusic.download.mp3juice.api.RequestAPI;
import com.freemusic.download.mp3juice.api.ResponseLogs;
import com.freemusic.download.mp3juice.api.SearchSuggestionsApi;
import com.freemusic.download.mp3juice.api.SearchSuggestionsResponse;
import com.freemusic.download.mp3juice.service.AudioPlayerService;
import com.freemusic.download.mp3juice.service.DownloadService;
import com.freemusic.download.mp3juice.service.TimerService;
import com.freemusic.download.mp3juice.utils.AppRaterHelper;
import com.freemusic.download.mp3juice.utils.Constants;
import com.freemusic.download.mp3juice.utils.DBHelper;
import com.freemusic.download.mp3juice.utils.HelperUtils;
import com.freemusic.download.mp3juice.utils.JdkmdenJav;
import com.freemusic.download.mp3juice.utils.SharedPref;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.startapp.sdk.ads.banner.Banner;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.StartAppSDK;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements ServiceConnection, PurchasesUpdatedListener {

    static {
        System.loadLibrary("realm-lib-sok");
    }

    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    private Toolbar toolbar;
    private TextView tv_downloads, tv_library, tv_share, tv_rating, tv_musicplayer, tv_removeads;
    private SearchView searchView;
    private SimpleCursorAdapter mAdapter;
    private ConfApp confApp;
    private IpResponse ipResponse;
    //ads
    private AdView mAdView;
    private AdRequest adRequest;
    private InterstitialAd mInterstitialAd;
    //startapp ads
    private Banner banner;

    //billings
    private BillingClient billingClient;
    //realm
    private Realm realm;
    private DownloadService downloadService;
    private AudioPlayerService audioPlayerService;
    private TimerService timerService;
    private ServiceConnection mConnectionDownloader = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            DownloadService.MyBinder binder = (DownloadService.MyBinder) iBinder;
            downloadService = binder.getService();
            //LogUtils.log("Downloader service is connected..");
            //Log.d("MAINDEDUG", "Nilainya connected = " + mBound);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            downloadService = null;
            //Log.d("MAINDEDUG", "Nilainya disconected = " + mBound);
        }
    };
    private ServiceConnection mConnectionTimerMusic = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            TimerService.MyBinder binder = (TimerService.MyBinder) iBinder;
            timerService = binder.getService();
            //LogUtils.log("Timer service is connected..");
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            timerService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        tv_downloads = findViewById(R.id.tv_downloads);
        tv_library = findViewById(R.id.tv_library);
        tv_share = findViewById(R.id.tv_share);
        tv_rating = findViewById(R.id.tv_rating);
        tv_musicplayer = findViewById(R.id.tv_musicplayer);
        tv_removeads = findViewById(R.id.tv_removeads);
        searchView = findViewById(R.id.searchView);

        //realm
        realm = Realm.getDefaultInstance();
        confApp = DBHelper.getConfApp();
        ipResponse = DBHelper.getConfAppIp();

        if (ipResponse != null && ipResponse.getOrg() != null && (ipResponse.getOrg().contains("Google")
                || ipResponse.getOrg().contains("Amazon")
                || ipResponse.getOrg().contains("Hosting Services"))
        ) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    confApp.setMainSearch(false);
                    realm.insertOrUpdate(confApp);
                }
            });
        }

        if (confApp != null && confApp.getBlackCountries() != null && ipResponse != null && confApp.getBlackCountries().toLowerCase().contains(ipResponse.getName().toLowerCase())) {
            //LogUtils.log("Unfortunately, you are not allowed to use our app.");
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    confApp.setMainSearch(false);
                    realm.insertOrUpdate(confApp);
                }
            });
        }

//        //LogUtils.log("Conf app: " + confApp.toString());
//        //LogUtils.log("IP Conf app: " + ipResponse.toString());

        //check force dialog
        setupForceDialog();

        //billing
        setupBillings();

        //check is premium user
        if (confApp != null && confApp.isPremiumUser()) {
            Drawable img = getResources().getDrawable(R.drawable.ic_thumb_up_white_24dp);
//            img.setBounds(0, 0, 60, 60);
            tv_removeads.setCompoundDrawablesWithIntrinsicBounds(null, img, null, null);
            tv_removeads.setText("Premium User");
        }

        //search
        setupSearchView();

        tv_downloads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showIntersialAds();
                startActivity(new Intent(MainActivity.this, DownloadsActivity.class));
            }
        });

        tv_library.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showIntersialAds();
                startActivity(new Intent(MainActivity.this, LibraryActivity.class));
            }
        });

        tv_rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ratingApp();
            }
        });

        tv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareApp();
            }
        });

        tv_musicplayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showIntersialAds();
                startActivity(new Intent(MainActivity.this, PlayerActivity.class));
            }
        });

        tv_removeads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confApp != null && !confApp.isPremiumUser()) {
                    launchRemoveAdsDialogFlow();
                } else if (confApp != null) {
                    Toast.makeText(MainActivity.this, "You are a Premium User..", Toast.LENGTH_SHORT).show();
                }
            }
        });


        //ads
        mAdView = findViewById(R.id.adView);

        //startapp ads
        banner = findViewById(R.id.startAppBanner);

        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            //Log.d("TwitterDl", "Ads is: "+confApp.isAdEnabled());
            initializeAdSDK();

            if (confApp.isAdMainEnabled()) {
                initializeBannerAds();

            } else {
                //LogUtils.log("hide both banner ads admob and startapp");
                mAdView.setVisibility(View.GONE);
                banner.hideBanner();
                banner.setVisibility(View.GONE);
            }

            if (confApp.isAdIntersialMainEnabled()) {
                initializeIntersialAds();

            }

        } else {
            //Log.d("TwitterDl", "Ads is: "+confApp.isAdEnabled());
            mAdView.setVisibility(View.GONE);
            banner.hideBanner();
            banner.setVisibility(View.GONE);
        }

    }

    private void initializeAdSDK() {
        if (confApp != null && confApp.isAdTypeAdmob()) {
            MobileAds.initialize(this, getString(R.string.google_admob_app_id));
        } else {
            if (confApp != null && confApp.getAdSaId() != null) {
                //LogUtils.log("Initialize startapp conf with sa id");
                StartAppSDK.init(this, confApp.getAdSaId(), confApp.getAdSaReturnEnabled());
            } else {
                //LogUtils.log("Initialize startapp conf with sa id from string");
                StartAppSDK.init(this, getResources().getString(R.string.startapp_app_id), confApp.getAdSaReturnEnabled());
            }
            if (!confApp.getAdSaSplashEnabled()) {
                //LogUtils.log("Disable splash ads startapp conf");
                StartAppAd.disableSplash();
            }

            if (!SharedPref.getBol(Constants.GDPR_AGREE)) {
                if (ipResponse != null & Constants.GDPR_COUNTRIES.toLowerCase().contains(ipResponse.getName().toLowerCase())) {
                    StartAppSDK.setUserConsent(this,
                            "pas",
                            System.currentTimeMillis(),
                            true);
                    SharedPref.saveBol(Constants.GDPR_AGREE, true);
                } else {
                    SharedPref.saveBol(Constants.GDPR_AGREE, true);
                }
            }
        }
    }

    private void initializeIntersialAds() {
        if (confApp != null && confApp.isAdTypeAdmob()) {
            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(getString(R.string.google_admob_intersial_id));
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    requestIntersialAds();
                    if (HelperUtils.getRandomBoolean()) {
//                            AppRaterHelper.showReviewDialog(MainActivity.this);
                        AppRaterHelper.showUpgradeDialog(MainActivity.this);
                    }
                }
            });

        } else {

        }
    }

    private void initializeBannerAds() {
        if (confApp != null && confApp.isAdTypeAdmob()) {
            //LogUtils.log("hide startapp banner and enable admob banner");
            mAdView.setVisibility(View.VISIBLE);
            banner.hideBanner();
            banner.setVisibility(View.GONE);
            adRequest = new AdRequest.Builder()
                    //.addTestDevice("33BE2250B43518CCDA7DE426D04EE231")
                    .build();
            mAdView.loadAd(adRequest);
        } else {
            //LogUtils.log("hide admob banner and enable startapp banner");
            banner.showBanner();
            banner.setVisibility(View.VISIBLE);
            mAdView.setVisibility(View.GONE);
        }
    }

    private void requestBannerAds() {
        adRequest = new AdRequest.Builder()
                //.addTestDevice("33BE2250B43518CCDA7DE426D04EE231")
                .build();
        mAdView.loadAd(adRequest);
    }

    public void showIntersialAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser() && confApp.isAdIntersialMainEnabled()) {
            if (confApp.isAdTypeAdmob()) {
                if (mInterstitialAd.isLoaded() && HelperUtils.getRandomBoolean()) {
                    //LogUtils.log("Show admob intersial ads");
                    mInterstitialAd.show();
                } else {
                    //LogUtils.log("Failed to load admob intersial ads and request new intersial ads");
                    requestIntersialAds();
                    //Log.d("TAG", "The interstitial wasn't loaded yet.");
                }
            } else {
                //LogUtils.log("Show startapp intersial ads");
                StartAppAd.showAd(this);
            }

        }
    }

    public void showIntersialAdsWithoutRandom() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser() && confApp.isAdIntersialMainEnabled()) {
            if (confApp.isAdTypeAdmob()) {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    requestIntersialAds();
                    //Log.d("TAG", "The interstitial wasn't loaded yet.");
                }
            } else {
                StartAppAd.showAd(this);
            }

        }
    }

    public void requestIntersialAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser() && confApp.isAdIntersialMainEnabled()) {
            if (confApp.isAdTypeAdmob()) {
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            } else {

            }

        }
    }

    private void setupForceDialog() {
        if (confApp != null && confApp.isForce_dialog()) {
            AppRaterHelper.showForceDialog(MainActivity.this, confApp);
        }
    }

    //billings
    private void setupBillings() {
        billingClient = BillingClient.newBuilder(MainActivity.this).enablePendingPurchases().setListener(this).build();
        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    // The BillingClient is ready. You can query purchases here.
//                    Log.d("MP3Downloader", "The BillingClient is ready");
                    if (!SharedPref.getBol(Constants.PREMIUM_USER_REMOVE_ADS)) {
                        SharedPref.saveBol(Constants.PREMIUM_USER_REMOVE_ADS, true);
                        isUserAlreadyPurchased();
                    }

                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
                //Log.d("MP3Downloader", "The BillingClient is not ready");
            }
        });
    }

    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> list) {

        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && list != null) {
            for (Purchase purchase : list) {
                handlePurchase(purchase);
            }
        } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
            // Handle an error caused by a user cancelling the purchase flow.
        } else {
            // Handle any other error codes.
        }
    }

    private void handlePurchase(Purchase purchase) {
        if (purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED) {
            if (!purchase.isAcknowledged()) {
                acknowledgePurchases(purchase, 0);
            }
            // Acknowledge purchase and grant the item to the user

        } else if (purchase.getPurchaseState() == Purchase.PurchaseState.PENDING) {
            // Here you can confirm to the user that they've started the pending
            // purchase, and to complete it, they should follow instructions that
            // are given to them. You can also choose to remind the user in the
            // future to complete the purchase if you detect that it is still
            // pending.
            Toast.makeText(MainActivity.this, "The purchase is pending, please complete it to use the premium feature.", Toast.LENGTH_SHORT).show();
        }

    }

    private void acknowledgePurchases(final Purchase purchase, final int alreadyPurchased) {
        AcknowledgePurchaseParams acknowledgePurchaseParams =
                AcknowledgePurchaseParams.newBuilder()
                        .setPurchaseToken(purchase.getPurchaseToken())
                        .build();
        billingClient.acknowledgePurchase(acknowledgePurchaseParams, new AcknowledgePurchaseResponseListener() {
            @Override
            public void onAcknowledgePurchaseResponse(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    if (purchase.getSku().contains(Constants.REMOVE_ADS_TEXT_ID)) {
                        try {
                            saveLogsPremium(ipResponse, purchase);
                        } catch (Exception ignored) {
                        }
                        performRemoveAdsRealm(alreadyPurchased);
                    }
                }
            }
        });

    }

    private void saveLogsPremium(IpResponse confAppIp, Purchase purchase) {
//        Log.d("MP3Downloader", "Tring to send message.. ");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(JdkmdenJav.getWorker())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final RequestAPI service = retrofit.create(RequestAPI.class);

        Call<ResponseLogs> call = service.getLogsPremium(confAppIp.getUniqueId(), confAppIp.getName(), confAppIp.getPackageName(), confAppIp.getAppVersion(),
                purchase.getOriginalJson(), purchase.getSku(), purchase.isAcknowledged(), purchase.getOrderId(), purchase.getPurchaseTime(),
                purchase.getPurchaseToken(), purchase.getPurchaseState(), purchase.getSignature());
        call.enqueue(new Callback<ResponseLogs>() {
            @Override
            public void onResponse(Call<ResponseLogs> call, Response<ResponseLogs> response) {

            }

            @Override
            public void onFailure(Call<ResponseLogs> call, Throwable t) {


            }
        });
    }

    private void performRemoveAdsRealm(int alreadyPurchased) {
        try {
            SharedPref.saveBol(Constants.IS_PREMIUM_USER_REMOVE_ADS, true);
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    confApp.setPremiumUser(true);
                    realm.insertOrUpdate(confApp);
                }
            });
        } finally {
            AppRaterHelper.showRestartDialog(MainActivity.this, alreadyPurchased);
        }

    }

    private void isUserAlreadyPurchased() {
//        Log.d("MP3Downloader", "Trying to query item already purchased");
        /*billingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.INAPP, new PurchaseHistoryResponseListener() {
            @Override
            public void onPurchaseHistoryResponse(BillingResult billingResult, List<PurchaseHistoryRecord> list) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK
                        && list != null) {
                    for (PurchaseHistoryRecord purchase : list) {
                        // Process the result.
                        Log.d("MP3Downloader", "Purchase is: "+purchase.getSku());
                        if (purchase.getSku().contains(Constants.REMOVE_ADS_TEXT_ID)) {
                            performRemoveAdsRealm(1);
                        }
                    }
                }
            }
        });*/
        Purchase.PurchasesResult purchasesResult = billingClient.queryPurchases(BillingClient.SkuType.INAPP);
        for (Purchase purchase : purchasesResult.getPurchasesList()) {
            if (!purchase.isAcknowledged()) {
                acknowledgePurchases(purchase, 1);
            } else {
                if (purchase.getSku().contains(Constants.REMOVE_ADS_TEXT_ID)) {
                    performRemoveAdsRealm(1);
                }
            }

        }
    }

    public void launchRemoveAdsDialogFlow() {
        //Log.d("MP3Downloader", "Trying to remove ads..");
        List<String> skuList = new ArrayList<>();
        skuList.add(Constants.REMOVE_ADS_TEXT_ID);
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
        billingClient.querySkuDetailsAsync(params.build(),
                new SkuDetailsResponseListener() {
                    @Override
                    public void onSkuDetailsResponse(BillingResult billingResult,
                                                     List<SkuDetails> skuDetailsList) {
                        // Process the result.
                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && skuDetailsList != null) {
                            for (SkuDetails skuDetails : skuDetailsList) {
                                String sku = skuDetails.getSku();
                                String price = skuDetails.getPrice();
                                if (Constants.REMOVE_ADS_TEXT_ID.equals(sku)) {
//                                    priceRemoveAds = price;
                                    // Retrieve a value for "skuDetails" by calling querySkuDetailsAsync().
                                    BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                                            .setSkuDetails(skuDetails)
                                            .build();
                                    billingClient.launchBillingFlow(MainActivity.this, flowParams);
                                }
                            }
                        }
                    }
                });
    }

    private void setupSearchView() {
        final String[] from = new String[]{"itemName"};
        final int[] to = new int[]{android.R.id.text1};
        mAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_1,
                null,
                from,
                to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        searchView.setSuggestionsAdapter(mAdapter);
        searchView.setIconifiedByDefault(false);
        searchView.clearFocus();
        // Getting selected (clicked) item suggestion
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionClick(int position) {

                Cursor cursor = (Cursor) mAdapter.getItem(position);
                String query = cursor.getString(cursor.getColumnIndex("itemName"));
                searchView.setQuery(query, false);
                //Log.d("MP3Downloader", "Query is: "+query);
                showIntersialAds();
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                intent.putExtra(SearchActivity.SEARCH_QUERY, query);
                startActivity(intent);

                return false;
            }

            @Override
            public boolean onSuggestionSelect(int position) {
                // Your code here
                Cursor cursor = (Cursor) mAdapter.getItem(position);
                String query = cursor.getString(cursor.getColumnIndex("itemName"));
                searchView.setQuery(query, false);
                showIntersialAds();
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                intent.putExtra(SearchActivity.SEARCH_QUERY, query);
                startActivity(intent);
                //Log.d("MP3Downloader", "Query is: "+query);

                return false;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                showIntersialAds();
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                intent.putExtra(SearchActivity.SEARCH_QUERY, s);
                startActivity(intent);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                searchQueries(s);
                return false;
            }
        });
    }

    private void searchQueries(final String query) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(SearchSuggestionsResponse.class, new SuggestionsDeserializer())
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.GQ_RUL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        final SearchSuggestionsApi service = retrofit.create(SearchSuggestionsApi.class);

        Call<SearchSuggestionsResponse> callConfApp = service.getSuggestions("firefox", "yt", query);
        callConfApp.enqueue(new Callback<SearchSuggestionsResponse>() {
            @Override
            public void onResponse(Call<SearchSuggestionsResponse> call, Response<SearchSuggestionsResponse> response) {
                String[] suggestions = response.body() != null ? response.body().getSuggestions() : new String[0];
                final MatrixCursor c = new MatrixCursor(new String[]{BaseColumns._ID, "itemName"});
                for (int i = 0; i < suggestions.length; i++) {
                    if (suggestions[i].toLowerCase().startsWith(query.toLowerCase()))
                        c.addRow(new Object[]{i, suggestions[i]});
                }
                mAdapter.changeCursor(c);
            }

            @Override
            public void onFailure(Call<SearchSuggestionsResponse> call, Throwable t) {
                try {
                    Toast.makeText(MainActivity.this, "Unable to fetch data..", Toast.LENGTH_SHORT).show();
                } catch (Exception ignored) {

                }

            }
        });
    }

    private void shareApp() {
        String urlaplikasi = "http://play.google.com/store/apps/details?id=" + getPackageName();
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        share.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.share_text));
        String strippedText = "Like this App? Share to your friends NOW!";
        share.putExtra(Intent.EXTRA_TEXT, "*Download this " + getResources().getString(R.string.app_name) + " App*" + "\n\n" + strippedText + "\n\nDownload at : " + urlaplikasi);
        startActivity(Intent.createChooser(share, getResources().getString(R.string.share_text)));
    }

    private void ratingApp() {
        String urlApp = "http://play.google.com/store/apps/details?id=" + getPackageName();
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlApp));
        startActivity(browserIntent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //start downloaderservice
        Intent intentDownload = new Intent(this, DownloadService.class);
        bindService(intentDownload, mConnectionDownloader, Context.BIND_AUTO_CREATE);
        //start timer service
        Intent intentTimer = new Intent(this, TimerService.class);
        bindService(intentTimer, mConnectionTimerMusic, Context.BIND_AUTO_CREATE);
        //start audioplayerservice
        Intent intent = new Intent(this, AudioPlayerService.class);
        Util.startForegroundService(this, intent);
        bindService(intent, this, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent intentDownload = new Intent(this, DownloadService.class);
        stopService(intentDownload);
        //stop timer service
        Intent intentTimer = new Intent(this, TimerService.class);
        stopService(intentTimer);
        //stop audioplayerservice
        Intent intent = new Intent(this, AudioPlayerService.class);
        stopService(intent);
        unbindService(this);
        unbindService(mConnectionDownloader);
        unbindService(mConnectionTimerMusic);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder binder) {
        AudioPlayerService.MyBinder audioServiceBiner = (AudioPlayerService.MyBinder) binder;
        audioPlayerService = audioServiceBiner.getService();
        //LogUtils.log("Music player service is connected..");
//        Toast.makeText(MainActivity.this, "Connected", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        audioPlayerService = null;
    }

    //permission
    public boolean checkPermission() {
//        //Log.d("MP3Downloader", "Requesting permission");
        int result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;
        }
    }

    public void requestPermission() {

        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("Read Files");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write Files");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                ActivityCompat.requestPermissions(this, permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
        }
    }

    public boolean addPermission(List<String> permissionsList, String permission) {
        if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permission))
                return false;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {
            if (resultCode == RESULT_OK) {
//                runForFirstTime();
                Toast.makeText(this, "Thanks. Permission granted", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                requestPermissionAgain();
            } else {
                Toast.makeText(this, "Please allow the files permission. Main features will not working properly.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void requestPermissionAgain() {
        new MaterialAlertDialogBuilder(this)
                .setCancelable(false)
                .setTitle("Enable file permission")
                .setMessage("We need grant to write downloaded files to the storage. Enable file permission?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        requestPermission();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, "Please allow the files permission. Main features will not working properly.", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupForceDialog();
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser() && confApp.isAdMainEnabled() && confApp.isAdTypeAdmob()) {
            if (HelperUtils.getRandomBoolean()) {
                requestBannerAds();
            } else {
                AppRaterHelper.showReviewDialog(MainActivity.this);
            }
        } else {
            if (HelperUtils.getRandomBoolean()) {
                AppRaterHelper.showReviewDialog(MainActivity.this);
            }
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    private class SuggestionsDeserializer implements JsonDeserializer<SearchSuggestionsResponse> {
        public SearchSuggestionsResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
            String rawSt = json.toString();
            String[] suggestions = rawSt.replaceAll("\\[(.*?)\\[", "")
                    .replaceAll("]", "")
                    .replaceAll("\"", "")
                    .split(",");
            return new SearchSuggestionsResponse(suggestions);
        }
    }
}
