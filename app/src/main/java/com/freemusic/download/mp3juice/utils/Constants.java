package com.freemusic.download.mp3juice.utils;

public class Constants {

    public static final String RATING_OPTIONS = "rating_options";
    public static final String DOWNLOADS = "downloads";
    public static final String FAVORITES = "favorites";
    public static final String HISTORY = "history";
    public static final String BASE_URL_DB = "https://www.dropbox.com/";
    public static final String GQ_RUL = "https://suggestqueries.google.com/";
    public static final String SONGS_ARRAY = "songs_array";
    public static final String SONGS_ARRAY_POSITION = "songs_array_position";
    public static final String CHANNEL_ID = "music_downloader";
    public static final String UID = "uid";
    public static final String MY_PB_IP = "public_ip";
    public static final String IS_JSON_INSERTED_TO_DB = "is_json_already_inserted";
    public static final String FIRST_RUN_APP_CONF_IP = "firstrun_ipresponseconf";
    public static final String FIRST_RUN_APP_CONF = "firstrun_appconf";
    public static final String PLAYER_OPTIONS_REPEAT = "player_options_repeat";
    public static final String PLAYER_OPTIONS_SHUFFLE = "player_options_shuffle";
    public static final String FIRST_CREATE_PLAYLIST = "create_first_playlist";
    public static final String PREMIUM_USER_REMOVE_ADS = "premium_user_remove_ads";
//        public static final String REMOVE_ADS_TEXT_ID = "android.test.purchased";
    public static final String REMOVE_ADS_TEXT_ID = "remove_ads";
    public static final String IS_PREMIUM_USER_REMOVE_ADS = "is_premium_user_remove_ads";
    public static final String UPGRADE_REMOVE_ADS_OPTIONS = "upgrade_premium_user";
    public static final String FIRST_RUN = "first_run_app";
    public static final String GDPR_COUNTRIES = "Austria, Belgium, Bulgaria, Croatia, Cyprus, Czech Republic, Denmark, Estonia, Finland, France, Germany, Greece, Hungary, Ireland, Italy, Latvia, Lithuania, Luxembourg, Malta, the Netherlands, Poland, Portugal, Romania, Slovakia, Slovenia, Spain, and Sweden";
    public static final String GDPR_AGREE = "gdpr_agrement";
}
