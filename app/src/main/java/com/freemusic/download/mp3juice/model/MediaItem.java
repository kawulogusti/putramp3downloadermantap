package com.freemusic.download.mp3juice.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;

import com.freemusic.download.mp3juice.R;
import com.freemusic.download.mp3juice.api.IpResponse;
import com.freemusic.download.mp3juice.utils.HelperUtils;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class MediaItem extends RealmObject implements Parcelable {

    public static final Parcelable.Creator<MediaItem> CREATOR = new Parcelable.Creator<MediaItem>() {
        @Override
        public MediaItem createFromParcel(Parcel source) {
            return new MediaItem(source);
        }

        @Override
        public MediaItem[] newArray(int size) {
            return new MediaItem[size];
        }
    };
    @SerializedName("artist")
    private String artist;
    @SerializedName("imageUrl")
    private String imageUrl;
    @PrimaryKey
    @SerializedName("mediaId")
    private String mediaId;
    @SerializedName("title")
    private String title;
    @SerializedName("trackUrl")
    private String trackUrl;
    private boolean isFastDl;
    private String downloadLinkFast;

    public MediaItem() {
    }

    public MediaItem(String artist, String imageUrl, String mediaId, String title, String trackUrl) {
        this.artist = artist;
        this.imageUrl = imageUrl;
        this.mediaId = mediaId;
        this.title = title;
        this.trackUrl = trackUrl;
    }

    public MediaItem(String artist, String imageUrl, String mediaId, String title, String trackUrl, boolean isFastDl, String downloadLinkFast) {
        this.artist = artist;
        this.imageUrl = imageUrl;
        this.mediaId = mediaId;
        this.title = title;
        this.trackUrl = trackUrl;
        this.isFastDl = isFastDl;
        this.downloadLinkFast = downloadLinkFast;
    }

    protected MediaItem(Parcel in) {
        this.artist = in.readString();
        this.imageUrl = in.readString();
        this.mediaId = in.readString();
        this.title = in.readString();
        this.trackUrl = in.readString();
    }

    public static MediaDescriptionCompat getMediaDescription(Context context, MediaItem mediaItem) {
        Bundle extras = new Bundle();
        Bitmap bitmap = getBitmap(context);
        extras.putParcelable(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, bitmap);
        extras.putParcelable(MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON, bitmap);
        return new MediaDescriptionCompat.Builder()
                .setMediaId(mediaItem.mediaId)
                .setIconBitmap(bitmap)
                .setTitle(mediaItem.title)
                .setDescription(HelperUtils.songNamingFormat(mediaItem.title, mediaItem.artist))
                .setExtras(extras)
                .build();
    }

    public static Bitmap getBitmap(Context context) {
        return ((BitmapDrawable) context.getResources().getDrawable(R.drawable.iconmusic)).getBitmap();
    }

    public Favorite getFavorite() {
        return new Favorite(this.artist, this.imageUrl, this.mediaId, this.title, this.trackUrl, this.isFastDl, this.downloadLinkFast);
    }

    public History getHistory() {
        return new History(this.artist, this.imageUrl, this.mediaId, this.title, this.trackUrl, this.isFastDl, this.downloadLinkFast);
    }

    public Download getDownload(Context context) {
        return new Download(this.artist, this.imageUrl, this.mediaId, this.title, this.trackUrl, HelperUtils.getFilePath(context, HelperUtils.songNamingFormat(this.title, this.artist)));
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTrackUrl() {
        return trackUrl;
    }

    public void setTrackUrl(String trackUrl) {
        this.trackUrl = trackUrl;
    }

    public String getTrackUrl(IpResponse ipResponse) {
        return trackUrl + "&title=" + title + ipResponse.getURLEncoded();
    }

    public String getTrackUrlForLogsPlays(IpResponse ipResponse) {
        return "mediaId="+mediaId+"&action=PLAY&title=" + title + ipResponse.getURLEncoded();
    }

    public String getTrackUrlForLogsDownloads(IpResponse ipResponse) {
        return "mediaId="+mediaId+"&action=DOWNLOAD&title=" + title + ipResponse.getURLEncoded();
    }

    public boolean isFastDl() {
        return isFastDl;
    }

    public void setFastDl(boolean fastDl) {
        isFastDl = fastDl;
    }

    public String getDownloadLinkFast() {
        return downloadLinkFast;
    }

    public void setDownloadLinkFast(String downloadLinkFast) {
        this.downloadLinkFast = downloadLinkFast;
    }

    @Override
    public String toString() {
        return
                "ResultsItem{" +
                        "artist = '" + artist + '\'' +
                        ",imageUrl = '" + imageUrl + '\'' +
                        ",mediaId = '" + mediaId + '\'' +
                        ",title = '" + title + '\'' +
                        ",trackUrl = '" + trackUrl + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.artist);
        dest.writeString(this.imageUrl);
        dest.writeString(this.mediaId);
        dest.writeString(this.title);
        dest.writeString(this.trackUrl);
    }

    public int getNotificationId() {
        return 9876;
    }

    public PlaylistItem getPlaylistItem() {
        return new PlaylistItem(this.artist, this.imageUrl, this.mediaId, this.title, this.trackUrl, this.isFastDl, this.downloadLinkFast);
    }
}