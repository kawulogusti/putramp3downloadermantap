/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.freemusic.download.mp3juice.service;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;

import androidx.annotation.DrawableRes;

import com.freemusic.download.mp3juice.R;
import com.freemusic.download.mp3juice.model.MediaItem;
import com.freemusic.download.mp3juice.utils.HelperUtils;


public final class Samples {

    public static final Sample[] SAMPLES = new Sample[]{
            new Sample("Square a Saw",
                    "https://images.unsplash.com/photo-1511671782779-c97d3d27a1d4?ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
                    "square-a-saw-wake-up",
                    "Wake Up",
                    "https://mp3d.jamendo.com/download/track/1672835/mp32",
                    getRandomBitmap())
    };

    public static MediaDescriptionCompat getMediaDescription(Context context, Sample sample) {
        Bundle extras = new Bundle();
        Bitmap bitmap = getBitmap(context, sample.bitmapResource);
        extras.putParcelable(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, bitmap);
        extras.putParcelable(MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON, bitmap);
        return new MediaDescriptionCompat.Builder()
                .setMediaId(sample.mediaId)
                .setIconBitmap(bitmap)
                .setTitle(sample.title)
                .setDescription(HelperUtils.songNamingFormat(sample.title, sample.artist))
                .setExtras(extras)
                .build();
    }

    public static MediaDescriptionCompat getMediaDescriptionFromMediaItem(Context context, MediaItem mediaItem) {
        Bundle extras = new Bundle();
        Bitmap bitmap = getBitmap(context, getRandomBitmap());
        extras.putParcelable(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, bitmap);
        extras.putParcelable(MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON, bitmap);
        return new MediaDescriptionCompat.Builder()
                .setMediaId(mediaItem.getMediaId())
                .setIconBitmap(bitmap)
                .setTitle(mediaItem.getTitle())
                .setDescription(HelperUtils.songNamingFormat(mediaItem.getTitle(), mediaItem.getArtist()))
                .setExtras(extras)
                .build();
    }

    public static Bitmap getBitmap(Context context, @DrawableRes int bitmapResource) {
        return ((BitmapDrawable) context.getResources().getDrawable(bitmapResource)).getBitmap();
    }

    public static int getRandomBitmap() {
        return R.drawable.iconmusic;
    }

    public static Bitmap getDefaultBitmap(Context context) {
        Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.iconmusic);
        return bm;
    }

    public static final class Sample {
        public String artist;
        public String imageUrl;
        public String mediaId;
        public String title;
        public String trackUrl;
        public int bitmapResource;

        public Sample(String artist, String imageUrl, String mediaId, String title, String trackUrl, int bitmapResource) {
            this.artist = artist;
            this.imageUrl = imageUrl;
            this.mediaId = mediaId;
            this.title = title;
            this.trackUrl = trackUrl;
            this.bitmapResource = bitmapResource;
        }

        @Override
        public String toString() {
            return title;
        }
    }

}
