/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.freemusic.download.mp3juice.service;

public final class C {

    public static final String PLAYBACK_CHANNEL_ID = "music_downloader_playback_channel";
    public static final int PLAYBACK_NOTIFICATION_ID = 89189;
    public static final String MEDIA_SESSION_TAG = "music_downloader_session";
    public static final String DOWNLOAD_CHANNEL_ID = "music_downloader_channel";
    public static final int DOWNLOAD_NOTIFICATION_ID = 998;

}
