package com.freemusic.download.mp3juice.utils;


import android.content.Context;

import com.freemusic.download.mp3juice.api.ConfApp;
import com.freemusic.download.mp3juice.api.IpResponse;
import com.freemusic.download.mp3juice.ext.utils.LogUtils;
import com.freemusic.download.mp3juice.model.Download;
import com.freemusic.download.mp3juice.model.Favorite;
import com.freemusic.download.mp3juice.model.History;
import com.freemusic.download.mp3juice.model.MediaItem;
import com.freemusic.download.mp3juice.model.PlaylistItem;
import com.freemusic.download.mp3juice.model.PlaylistPost;

import java.util.UUID;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

//import com.mp3player.mp3player.model.gdrive.ItemsItem;

public class DBHelper {

    public final static String MEDIA_ID = "mediaId";
    public final static String PLAYLIST_ID = "id";
    public final static String PLAYLIST_DATE = "date";
    private static final String RECENTLY_PLAYED = "recentlyPlayed";

    public static ConfApp getConfApp() {

        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            ConfApp confApp = realm.where(ConfApp.class).findFirst();
            if (confApp != null) {
                return confApp;
            } else {
                return null;
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static IpResponse getConfAppIp() {
        Realm realm = null;
        try { // I could use try-with-resources here
            realm = Realm.getDefaultInstance();
            IpResponse ipResponse = realm.where(IpResponse.class).findFirst();
            try {
                if (ipResponse != null) {
                    return ipResponse;
                } else {
                    return null;
                }
            } catch (Exception e) {
                return null;
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }


    public static Favorite getFavorite(String id) {

        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            Favorite favorite = realm.where(Favorite.class).contains(MEDIA_ID, id).findFirst();
            if (favorite != null) {
                return favorite;
            } else {
                return null;
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static RealmList<MediaItem> getSearchResults(String s) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            RealmResults<MediaItem> items = realm.where(MediaItem.class)
                    .contains("title", s, Case.INSENSITIVE)
                    .findAll().sort("title", Sort.ASCENDING);
            ;
            RealmList<MediaItem> contentsItems = new RealmList<>();
            contentsItems.addAll(items.subList(0, items.size()));
            return contentsItems;

        } finally {
            if (realm != null) {
                realm.close();
            }
        }

    }

    public static Download getDownload(String id) {

        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            Download download = realm.where(Download.class).contains(MEDIA_ID, id).findFirst();
            if (download != null) {
                return download;
            } else {
                return null;
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static RealmList<Download> getSearchResultsDownloads(String s) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            RealmResults<Download> items = realm.where(Download.class)
                    .contains("title", s, Case.INSENSITIVE)
                    .findAll().sort("title", Sort.ASCENDING);
            RealmList<Download> contentsItems = new RealmList<>();
            contentsItems.addAll(items.subList(0, items.size()));
            return contentsItems;

        } finally {
            if (realm != null) {
                realm.close();
            }
        }

    }

    public static RealmList<Download> getAllDownloads() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            RealmResults<Download> items = realm.where(Download.class)
                    .findAll().sort("recentlyPlayed", Sort.DESCENDING);
            if (items.size() > 0) {
                RealmList<Download> contentsItems = new RealmList<>();
                contentsItems.addAll(items.subList(0, items.size()));
                return contentsItems;
            } else {
                return null;
            }


        } finally {
            if (realm != null) {
                realm.close();
            }
        }

    }

    public static RealmList<History> getSearchResultsHistory(String s) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            RealmResults<History> items = realm.where(History.class)
                    .contains("title", s, Case.INSENSITIVE)
                    .findAll().sort("recentlyPlayed", Sort.DESCENDING);
            RealmList<History> contentsItems = new RealmList<>();
            contentsItems.addAll(items.subList(0, items.size()));
            return contentsItems;

        } finally {
            if (realm != null) {
                realm.close();
            }
        }

    }

    public static History getHistory(String id) {

        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            History history = realm.where(History.class).contains(MEDIA_ID, id).findFirst();
            if (history != null) {
                return history;
            } else {
                return null;
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static RealmList<Favorite> getSearchResultsFavorites(String s) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            RealmResults<Favorite> items = realm.where(Favorite.class)
                    .contains("title", s, Case.INSENSITIVE)
                    .findAll().sort("recentlyPlayed", Sort.DESCENDING);
            RealmList<Favorite> contentsItems = new RealmList<>();
            contentsItems.addAll(items.subList(0, items.size()));
            return contentsItems;

        } finally {
            if (realm != null) {
                realm.close();
            }
        }

    }

    public static void setOrDeleteFavorite(final MediaItem mediaItem) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            final Favorite favorite = realm.where(Favorite.class).contains(MEDIA_ID, mediaItem.getMediaId()).findFirst();
            if (favorite != null) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        favorite.deleteFromRealm();
                    }
                });
            } else {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        //Log.d("TwitterDl", "Write favorite item");
                        realm.copyToRealmOrUpdate(mediaItem.getFavorite());
                    }
                });
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static void deleteFavorite(final MediaItem mediaItem) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            final Favorite favorite = realm.where(Favorite.class).contains(MEDIA_ID, mediaItem.getMediaId()).findFirst();
            if (favorite != null) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        favorite.deleteFromRealm();
                    }
                });
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }


    public static void deleteDownload(final MediaItem mediaItem) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            final Download download = realm.where(Download.class).contains(MEDIA_ID, mediaItem.getMediaId()).findFirst();
            if (download != null) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        download.deleteFromRealm();
                    }
                });
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static void deleteAllDownloads() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.where(Download.class).findAll().deleteAllFromRealm();
                }
            });

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static void deleteAllHistory() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.where(History.class).findAll().deleteAllFromRealm();
                }
            });

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static void deleteAllFavorites() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.where(Favorite.class).findAll().deleteAllFromRealm();
                }
            });

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }


    public static void setOrDeleteHistory(final MediaItem mediaItem) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            final History history = realm.where(History.class).contains(MEDIA_ID, mediaItem.getMediaId()).findFirst();
            if (history != null) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        history.deleteFromRealm();
                    }
                });
            } else {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        //Log.d("TwitterDl", "Write history item");
                        realm.copyToRealmOrUpdate(mediaItem.getHistory());
                    }
                });
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static void deleteHistory(MediaItem mediaItem) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            final History history = realm.where(History.class).contains(MEDIA_ID, mediaItem.getMediaId()).findFirst();
            if (history != null) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        history.deleteFromRealm();
                    }
                });
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static void setHistory(final MediaItem mediaItem) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    //Log.d("TwitterDl", "Write history item");
                    realm.copyToRealmOrUpdate(mediaItem.getHistory());
                }
            });

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static void setDownload(final Context context, final MediaItem mediaItem) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    //Log.d("TwitterDl", "Write download item");
                    realm.copyToRealmOrUpdate(mediaItem.getDownload(context));
                }
            });

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static RealmList<History> getHistoryList() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            RealmResults<History> items = realm.where(History.class).findAll().sort(RECENTLY_PLAYED, Sort.DESCENDING);
            if (items.size() > 0) {
                RealmList<History> trackItems = new RealmList<History>();
                trackItems.addAll(items.subList(0, items.size()));
                return trackItems;
            } else {
                return null;
            }
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static RealmList<Favorite> getFavoriteList() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            RealmResults<Favorite> items = realm.where(Favorite.class).findAll().sort(RECENTLY_PLAYED, Sort.DESCENDING);
            if (items.size() > 0) {
                RealmList<Favorite> trackItems = new RealmList<Favorite>();
                trackItems.addAll(items.subList(0, items.size()));
                return trackItems;
            } else {
                return null;
            }


        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static RealmList<Download> getDownloadList() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            RealmResults<Download> item = realm.where(Download.class).findAll().sort(RECENTLY_PLAYED, Sort.DESCENDING);
            RealmList<Download> trackItems = new RealmList<Download>();
            trackItems.addAll(item.subList(0, item.size()));
            return trackItems;

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static PlaylistItem getPlaylistItem(String id) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            PlaylistItem playlistItem = realm.where(PlaylistItem.class).equalTo(MEDIA_ID, id).findFirst();
            if (playlistItem != null) {
                return playlistItem;
            } else {
                return null;
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static RealmList<PlaylistItem> getSearchResultsPlaylists(PlaylistPost playlistPost) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            PlaylistPost playlistPostRealm = realm.where(PlaylistPost.class).contains(PLAYLIST_ID, playlistPost.getId()).findFirst();
            return playlistPostRealm.getPlaylistItems();

        } finally {
            if (realm != null) {
                realm.close();
            }
        }

    }

    public static RealmResults<PlaylistPost> getPlaylistList() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            RealmResults<PlaylistPost> items = realm.where(PlaylistPost.class).findAll().sort(PLAYLIST_DATE, Sort.DESCENDING);

            return items;

        } finally {
            if (realm != null) {
                realm.close();
            }
        }

    }

    public static void createPlaylist(String title) {
        String uniqueID = UUID.randomUUID().toString();
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            final PlaylistPost playlistPost = new PlaylistPost(uniqueID, title);
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(playlistPost);
                }
            });

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static void editPlaylist(String uid, final String title) {
        String uniqueID = uid;
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            final PlaylistPost playlistPost = realm.where(PlaylistPost.class).contains(PLAYLIST_ID, uniqueID).findFirst();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    playlistPost.setTitle(title);
                    realm.copyToRealmOrUpdate(playlistPost);
                }
            });

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static void deletePlaylist(String uid) {
        String uniqueID = uid;
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            final PlaylistPost playlistPost = realm.where(PlaylistPost.class).contains(PLAYLIST_ID, uniqueID).findFirst();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    playlistPost.deleteFromRealm();
                }
            });

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static PlaylistPost getPlaylistPost(String uid) {
        String uniqueID = uid;
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            PlaylistPost playlistPost = realm.where(PlaylistPost.class).contains(PLAYLIST_ID, uniqueID).findFirst();
            if (playlistPost != null) {
                return playlistPost;
            } else {
                return null;
            }
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public static String createItemPlaylist(final PlaylistPost playlistPost, final PlaylistItem playlistItem) {
        //LogUtils.log(playlistPost.getTitle() + " - " + playlistItem.getTitle());
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
//            final PlaylistPost playlistPostRealm = realm.where(PlaylistPost.class).contains(PLAYLIST_ID, playlistPost.getId()).findFirst();
            if (!playlistPost.getPlaylistItems().contains(playlistItem)) {
                //LogUtils.log("Playlistpost is not contains media item");
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
//                        PlaylistPost playlistPostRealm = realm.where(PlaylistPost.class).contains(PLAYLIST_ID, playlistPost.getId()).findFirst();
//                        PlaylistItem playlistItemRealm = realm.where(PlaylistItem.class).contains(Constants.QUERY_MEDIA_ID, playlistItem.getMediaId()).findFirst();
                        playlistPost.addPlaylistItemObject(playlistItem);
                        realm.copyToRealmOrUpdate(playlistPost);
                    }
                });

                return "Added to " + playlistPost.getTitle();

            } else {
                //LogUtils.log("Playlistpost is contains media item");
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
//                        PlaylistPost playlistPostRealm = realm.where(PlaylistPost.class).contains(PLAYLIST_ID, playlistPost.getId()).findFirst();
//                        PlaylistPost playlistPostRealm = realm.where(PlaylistPost.class).contains(PLAYLIST_ID, playlistPost.getId()).findFirst();
                        playlistPost.removePlaylistItemObject(playlistItem);
                        realm.copyToRealmOrUpdate(playlistPost);
                    }
                });

                return "Removed from " + playlistPost.getTitle();
            }

        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

}
