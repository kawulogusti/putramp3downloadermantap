package com.freemusic.download.mp3juice.api;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SearchSuggestionsApi {

    /*@GET("complete/search")
    Observable<SearchSuggestionsResponse> getSuggestionsRx(@Query("output") String output, @Query("ds") String ds, @Query("q") String query);
*/
    @GET("complete/search")
    Call<SearchSuggestionsResponse> getSuggestions(@Query("output") String output, @Query("ds") String ds, @Query("q") String query);
}
