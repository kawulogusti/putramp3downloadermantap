package com.freemusic.download.mp3juice.ext;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;

import com.freemusic.download.mp3juice.api.ConfApp;
import com.freemusic.download.mp3juice.ext.model.PlayerResponse;
import com.freemusic.download.mp3juice.ext.model.Response;
import com.freemusic.download.mp3juice.ext.model.StreamingData;
import com.freemusic.download.mp3juice.ext.model.YMedia;
import com.freemusic.download.mp3juice.ext.model.YMeta;
import com.freemusic.download.mp3juice.ext.model.YTSubtitles;
import com.freemusic.download.mp3juice.ext.utils.HTTPUtility;
import com.freemusic.download.mp3juice.ext.utils.RegexUtils;
import com.freemusic.download.mp3juice.ext.utils.Utils;
import com.freemusic.download.mp3juice.utils.JdkmdenJav;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class YExtractor_Bak extends AsyncTask<String, Void, Void> {


    Map<String, String> Headers = new HashMap<>();
    List<YMedia> adaptiveMedia = new ArrayList<>();
    List<YMedia> muxedMedia = new ArrayList<>();
    List<YTSubtitles> subtitle = new ArrayList<>();
    String regexUrl = ("(?<=url=).*");
    String regexYtshortLink = "(http|https)://(www\\.|)youtu.be/.*";
    String regexPageLink = ("(http|https)://(www\\.|m.|)youtube\\.com/watch\\?v=(.+?)( |\\z|&)");
    String regexFindReason = "(?<=(class=\"message\">)).*?(?=<)";
    String regexPlayerJson = "(?<=ytplayer.config\\s=).*?((\\}(\n|)\\}(\n|))|(\\}))(?=;)";
    ExtractorListner listener;
    List<String> reasonUnavialable = Arrays.asList(new String[]{"This video is unavailable on this device.", "Content Warning", "who has blocked it on copyright grounds."});
    Handler han = new Handler(Looper.getMainLooper());
    private ExtractorException Ex;
    private Response response;
    private YMeta ytmeta;
    private ConfApp confApp;

    public YExtractor_Bak(ConfApp confApp, ExtractorListner EL) {
        this.listener = EL;
        this.confApp = confApp;
        Headers.put("Accept-Language", "en");
    }

    public YExtractor_Bak useDefaultLogin() {
        if (confApp != null) {
//			Log.d("MP3Downloader", "Cookies value is: "+confApp.getCookies());
            Headers.put("Cookie", confApp.getCookies());
            return setHeaders(Headers);
        } else {
            Headers.put("Cookie", Utils.loginCookie);
            return setHeaders(Headers);
        }
    }

    public Map<String, String> getHeaders() {
        return Headers;
    }

    public YExtractor_Bak setHeaders(Map<String, String> headers) {
        Headers = headers;
        return this;
    }

    public void Extract(String VideoId) {
        this.execute(VideoId);
    }


    @Override
    protected void onPostExecute(Void result) {
        if (Ex != null) {
            listener.onExtractionGoesWrong(Ex);
        } else {
            listener.onExtractionDone(adaptiveMedia, muxedMedia, subtitle, ytmeta);
        }
    }

    @Override
    protected void onPreExecute() {
        Ex = null;
        adaptiveMedia.clear();
        muxedMedia.clear();

    }

    @Override
    protected void onCancelled() {
        if (Ex != null) {
            listener.onExtractionGoesWrong(Ex);
        }
    }


    @Override
    protected Void doInBackground(String[] ids) {

        String Videoid = Utils.extractVideoID(ids[0]);
        String jsonBody = null;
        try {
            String body = HTTPUtility.downloadPageSource(JdkmdenJav.getServTub() + "watch?v=" + Videoid + "&has_verified=1&bpctr=9999999999", Headers);
            jsonBody = parsePlayerConfig(body);

            PlayerResponse playerResponse = parseJson(jsonBody);
            ytmeta = playerResponse.getVideoDetails();
            subtitle = playerResponse.getCaptions() != null ? playerResponse.getCaptions().getPlayerCaptionsTracklistRenderer().getCaptionTracks() : null;
            //Utils.copyToBoard(jsonBody);
            if (playerResponse.getVideoDetails().getisLive()) {
                parseLiveUrls(playerResponse.getStreamingData());
            } else {
                StreamingData sd = playerResponse.getStreamingData();
//				//LogUtils.log("sizea= " + sd.getAdaptiveFormats().length);
//				//LogUtils.log("sizem= " + sd.getFormats().length);

                adaptiveMedia = parseUrls(sd.getAdaptiveFormats());
                muxedMedia = parseUrls(sd.getFormats());
//				//LogUtils.log("sizeXa= " + adaptiveMedia.size());
//				//LogUtils.log("sizeXm= " + muxedMedia.size());

				/*for (YMedia ytMedia:adaptiveMedia) {
					if (!ytMedia.isVideo()) {
						//LogUtils.log("Adaptive = " +ytMedia.isVideo()+" - "+ytMedia.getMimeType()+" - "+ytMedia.getAudioQuality()+" - "+ytMedia.getUrl());
					}

				}

				for (YMedia ytMedia:muxedMedia) {
					if (!ytMedia.isVideo()) {
						//LogUtils.log("Muxed = " +ytMedia.isVideo()+" - "+ytMedia.getMimeType()+" - "+ytMedia.getAudioQuality()+" - "+ytMedia.getUrl());
					}
				}*/

            }
        } catch (Exception e) {
//			//LogUtils.log(Arrays.toString(e.getStackTrace()));
            Ex = new ExtractorException("Error While getting Youtube Data:" + e.getMessage());
            this.cancel(true);
        }
        return null;
    }

    /*this function creates Json models using Gson*/
    private PlayerResponse parseJson(String body) throws Exception {
        JsonParser parser = new JsonParser();
        response = new GsonBuilder().serializeNulls().create().fromJson(parser.parse(body), Response.class);
        return new GsonBuilder().serializeNulls().create().fromJson(response.getArgs().getPlayerResponse(), PlayerResponse.class);
    }

    /*This function is used to check if webpage contain steam data and then gets the Json part of from the page using regex*/
    private String parsePlayerConfig(String body) throws ExtractorException {

        if (Utils.isListContain(reasonUnavialable, RegexUtils.matchGroup(regexFindReason, body))) {
            throw new ExtractorException(RegexUtils.matchGroup(regexFindReason, body));
        }
        if (body.contains("ytplayer.config")) {
            return RegexUtils.matchGroup(regexPlayerJson, body);
        } else {
            throw new ExtractorException("This Video is unavialable");
        }
    }




    /*independent function Used to parse urls for adaptive & muxed stream with cipher protection*/

    private List<YMedia> parseUrls(YMedia[] rawMedia) {
        List<YMedia> links = new ArrayList<>();
        try {
            for (int x = 0; x < rawMedia.length; x++) {
                YMedia media = rawMedia[x];
//				//LogUtils.log(media.getCipher() != null ? media.getCipher(): "null cip");

                if (media.useCipher()) {
                    String tempUrl = "";
                    String decodedSig = "";
                    for (String partCipher : media.getCipher().split("&")) {


                        if (partCipher.startsWith("s=")) {
                            decodedSig = CipherManager.dechiperSig(URLDecoder.decode(partCipher.replace("s=", "")), response.getAssets().getJs());
                        }

                        if (partCipher.startsWith("url=")) {
                            tempUrl = URLDecoder.decode(partCipher.replace("url=", ""));

                            for (String url_part : tempUrl.split("&")) {
                                if (url_part.startsWith("s=")) {
                                    decodedSig = CipherManager.dechiperSig(URLDecoder.decode(url_part.replace("s=", "")), response.getAssets().getJs());
                                }
                            }
                        }
                    }

                    String FinalUrl = tempUrl + "&sig=" + decodedSig;

                    media.setUrl(FinalUrl);
                    links.add(media);


                } else {
                    links.add(media);
                }
            }

        } catch (Exception e) {
            Ex = new ExtractorException(e.getMessage());
            this.cancel(true);
        }
        return links;
    }





    /*This funtion parse live youtube videos links from streaming data  */

    private void parseLiveUrls(StreamingData streamData) throws Exception {
        if (streamData.getHlsManifestUrl() == null) {
            throw new ExtractorException("No link for hls video");
        }
        String hlsPageSource = HTTPUtility.downloadPageSource(streamData.getHlsManifestUrl());
        String regexhlsLinks = "(#EXT-X-STREAM-INF).*?(index.m3u8)";
        List<String> rawData = RegexUtils.getAllMatches(regexhlsLinks, hlsPageSource);
        for (String data : rawData) {
            YMedia media = new YMedia();
            String[] info_list = RegexUtils.matchGroup("(#).*?(?=https)", data).split(",");
            String live_url = RegexUtils.matchGroup("(https:).*?(index.m3u8)", data);
            media.setUrl(live_url);
            for (String info : info_list) {
                if (info.startsWith("BANDWIDTH")) {
                    media.setBitrate(Integer.valueOf(info.replace("BANDWIDTH=", "")));
                }
                if (info.startsWith("CODECS")) {
                    media.setMimeType((info.replace("CODECS=", "").replace("\"", "")));
                }
                if (info.startsWith("FRAME-RATE")) {
                    media.setFps(Integer.valueOf((info.replace("FRAME-RATE=", ""))));
                }
                if (info.startsWith("RESOLUTION")) {
                    String[] RESOLUTION = info.replace("RESOLUTION=", "").split("x");
                    media.setWidth(Integer.valueOf(RESOLUTION[0]));
                    media.setHeight(Integer.valueOf(RESOLUTION[1]));
                    media.setQualityLabel(RESOLUTION[1] + "p");
                }
            }
//			//LogUtils.log(media.getUrl());
            muxedMedia.add(media);
        }


    }

    public interface ExtractorListner {
        void onExtractionGoesWrong(ExtractorException e);

        void onExtractionDone(List<YMedia> adativeStream, List<YMedia> muxedStream, List<YTSubtitles> subList, YMeta meta);
    }

}     
