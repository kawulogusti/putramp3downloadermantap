/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.freemusic.download.mp3juice.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.freemusic.download.mp3juice.R;
import com.freemusic.download.mp3juice.activity.PlayerActivity;
import com.freemusic.download.mp3juice.ext.utils.LogUtils;
import com.freemusic.download.mp3juice.model.Download;
import com.freemusic.download.mp3juice.model.History;
import com.freemusic.download.mp3juice.model.MediaItem;
import com.freemusic.download.mp3juice.utils.Constants;
import com.freemusic.download.mp3juice.utils.DBHelper;
import com.freemusic.download.mp3juice.utils.SharedPref;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector;
import com.google.android.exoplayer2.ext.mediasession.TimelineQueueNavigator;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerNotificationManager;
import com.google.android.exoplayer2.ui.PlayerNotificationManager.BitmapCallback;
import com.google.android.exoplayer2.ui.PlayerNotificationManager.MediaDescriptionAdapter;
import com.google.android.exoplayer2.ui.PlayerNotificationManager.NotificationListener;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.ArrayList;
import java.util.List;

import static com.freemusic.download.mp3juice.service.C.MEDIA_SESSION_TAG;
import static com.freemusic.download.mp3juice.service.C.PLAYBACK_CHANNEL_ID;
import static com.freemusic.download.mp3juice.service.C.PLAYBACK_NOTIFICATION_ID;
import static com.freemusic.download.mp3juice.service.Samples.SAMPLES;


public class AudioPlayerService extends Service {

    public static final String BROADCAST_STOP_MUSIC_PLAYER = "stop_music_player_mp3downloader";
    public static final String BROADCAST_STOP_MUSIC_PLAYER_SERVICE = "stop_music_player_service_mp3downloader";
    public static final String BROADCAST_ADD_NEW_AUDIO = "add_new_audio_mp3downloader";
    public static final String BROADCAST_REMOVE_AUDIO = "remove_audio_mp3downloader";
    private final IBinder mBinder = new MyBinder();
    private SimpleExoPlayer player;
    private PlayerNotificationManager playerNotificationManager;
    private MediaSessionCompat mediaSession;
    private MediaSessionConnector mediaSessionConnector;
    private ArrayList<MediaItem> songItemsList;
    private String songTitle = "";
    private ConcatenatingMediaSource concatenatingMediaSource;
    private BroadcastReceiver addNewAudio = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            MediaItem mediaItem = intent.getParcelableExtra(BROADCAST_ADD_NEW_AUDIO);
            addNewSongtoQuee(mediaItem);
            Toast.makeText(context, "Added to queue", Toast.LENGTH_SHORT).show();
        }
    };
    private BroadcastReceiver removeAudio = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int index = intent.getIntExtra(BROADCAST_REMOVE_AUDIO, 0);
            removeAudio(index);
            Intent intentSendBroadcastRemovedAudio = new Intent(PlayerActivity.BROADCAST_RECEIVE_REMOVE_AUDIO);
            context.sendBroadcast(intentSendBroadcastRemovedAudio);
            Toast.makeText(context, "Removed from queue", Toast.LENGTH_SHORT).show();
        }
    };
    private BroadcastReceiver stopMusicPlayer = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //LogUtils.log("Trying to stop music player");
            if (player != null) {
                player.setPlayWhenReady(false);
            }
        }
    };
    private BroadcastReceiver stopMusicPlayerService = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //LogUtils.log("Trying to stop music player service");
            player.stop();
            playerNotificationManager.setPlayer(null);
            stopForeground(true);
        }
    };

    public SimpleExoPlayer getplayerInstance(ArrayList<MediaItem> songItems, int index_position) {
        if (player == null) {
            startPlayer(songItems, index_position);
        } else {
            startPlayer(songItems, index_position);
        }
        return player;
    }

    public SimpleExoPlayer getPlayer() {
        return player;
    }

    public String getTitleSongNowPlaying() {
        return songTitle;
    }

    public ArrayList<MediaItem> currentSongList() {
        return this.songItemsList;
    }

    public int currentIndexPosition() {
        return player.getCurrentWindowIndex();
    }

    public int getRepeatMode() {
        return player.getRepeatMode();
    }

    public void setRepeatMode(int repeatMode) {
        player.setRepeatMode(repeatMode);
    }

    public boolean getShuffleMode() {
        return player.getShuffleModeEnabled();
    }

    public void setShuffleMode(boolean shuffleMode) {
        player.setShuffleModeEnabled(shuffleMode);
    }

    public void seekToIndex(int position) {
        player.seekTo(position, 0);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startPlayer(null, 0);
        register_addNewAudio();
        register_removeAudio();
        register_StopMusicPlayer();
        register_stopMusicPlayerService();
    }

    @Override
    public void onDestroy() {
        //LogUtils.log("Audio player service is destroyed");
        player.release();
        playerNotificationManager.setPlayer(null);
        player = null;
        mediaSession.release();
        mediaSessionConnector.setPlayer(null, null);
        unregisterReceiver(addNewAudio);
        unregisterReceiver(stopMusicPlayer);
        unregisterReceiver(stopMusicPlayerService);
        unregisterReceiver(removeAudio);
        stopSelf();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return START_NOT_STICKY;
    }

    public void startPlayer(final ArrayList<MediaItem> songItems, int index_position) {

        songItemsList = new ArrayList<>();
        if (songItems == null) {
            final Context context = this;

            player = ExoPlayerFactory.newSimpleInstance(context, new DefaultTrackSelector());
            setRepeatMode(SharedPref.getInt(Constants.PLAYER_OPTIONS_REPEAT));
            setShuffleMode(SharedPref.getBol(Constants.PLAYER_OPTIONS_SHUFFLE));

            String userAgent = Util.getUserAgent(context, getString(R.string.app_name));
            // Default parameters, except allowCrossProtocolRedirects is true
            DefaultHttpDataSourceFactory httpDataSourceFactory = new DefaultHttpDataSourceFactory(
                    userAgent,
                    null /* listener */,
                    DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS,
                    DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS,
                    true /* allowCrossProtocolRedirects */
            );

            DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(
                    context, null, httpDataSourceFactory);
            CacheDataSourceFactory cacheDataSourceFactory = new CacheDataSourceFactory(
                    DownloadUtil.getCache(context),
                    dataSourceFactory,
                    CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);
            List<History> historyList = DBHelper.getHistoryList();

            if (historyList == null) {
                for (Samples.Sample sample : SAMPLES) {
                    songItemsList.add(new MediaItem(sample.artist, sample.imageUrl, sample.mediaId, sample.title, sample.trackUrl));
                }

            } else {
//                Log.e("ERRORDEBUG", "value historylist = tidak null");
                for (History history : historyList) {
                    songItemsList.add(history.getMediaItem());
                }
            }

            setupSourcesStart(cacheDataSourceFactory, songItemsList, index_position);
            playerNotificationManager = new PlayerNotificationManager(
                    context,
                    PLAYBACK_CHANNEL_ID,
                    PLAYBACK_NOTIFICATION_ID,
                    new MediaDescriptionAdapter() {
                        @Override
                        public String getCurrentContentTitle(Player player) {
                            try {
                                return songItemsList.get(player.getCurrentWindowIndex()).getTitle();
                            } catch (Exception e) {
                                return "Play a song!";
                            }
                        }

                        @Nullable
                        @Override
                        public PendingIntent createCurrentContentIntent(Player player) {
                            Intent intent = new Intent(context, PlayerActivity.class);

                            return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        }

                        @Nullable
                        @Override
                        public String getCurrentContentText(Player player) {
                            try {
                                return songItemsList.get(player.getCurrentPeriodIndex()).getArtist();
                            } catch (Exception e) {
                                return "Play a song!";
                            }

                        }

                        @Nullable
                        @Override
                        public Bitmap getCurrentLargeIcon(Player player, BitmapCallback callback) {
                            try {
                                return MediaItem.getBitmap(context);
                            } catch (Exception e) {
                                return Samples.getDefaultBitmap(AudioPlayerService.this);
                            }

                        }
                    },
                    new CustomActionNotification(context, songItemsList)
            );
            /*playerNotificationManager = PlayerNotificationManager.createWithNotificationChannel(
                    context,
                    PLAYBACK_CHANNEL_ID,
                    R.string.playback_channel_name,
                    PLAYBACK_NOTIFICATION_ID,
                    new MediaDescriptionAdapter() {
                        @Override
                        public String getCurrentContentTitle(Player player) {
                            try {
                                return songItemsList.get(player.getCurrentWindowIndex()).getTitle();
                            } catch (Exception e) {
                                return "Play a song!";
                            }
                        }

                        @Nullable
                        @Override
                        public PendingIntent createCurrentContentIntent(Player player) {
                            Intent intent = new Intent(context, PlayerActivity.class);

                            return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        }

                        @Nullable
                        @Override
                        public String getCurrentContentText(Player player) {
                            try {
                                return songItemsList.get(player.getCurrentPeriodIndex()).getArtist();
                            } catch (Exception e) {
                                return "Play a song!";
                            }

                        }

                        @Nullable
                        @Override
                        public Bitmap getCurrentLargeIcon(Player player, BitmapCallback callback) {
                            try {
                                return MediaItem.getBitmap(context);
                            } catch (Exception e) {
                                return Samples.getDefaultBitmap(AudioPlayerService.this);
                            }

                        }
                    }
                    );*/
            playerNotificationManager.setNotificationListener(new NotificationListener() {
                @Override
                public void onNotificationStarted(int notificationId, Notification notification) {
                    startForeground(notificationId, notification);
                }

                @Override
                public void onNotificationCancelled(int notificationId) {
//                    stopForeground(true);
                    stopSelf();
                }
            });
            playerNotificationManager.setSmallIcon(R.drawable.ic_notification_music);
            playerNotificationManager.setFastForwardIncrementMs(-1);
            playerNotificationManager.setRewindIncrementMs(-1);
            playerNotificationManager.setStopAction(null);
            playerNotificationManager.setPlayer(player);

            try {
                songTitle = songItemsList.get(player.getCurrentWindowIndex()).getTitle();
            } catch (Exception ignored) {

            }

            mediaSession = new MediaSessionCompat(context, MEDIA_SESSION_TAG);
            mediaSession.setActive(true);
            playerNotificationManager.setMediaSessionToken(mediaSession.getSessionToken());

            mediaSessionConnector = new MediaSessionConnector(mediaSession);
            mediaSessionConnector.setQueueNavigator(new TimelineQueueNavigator(mediaSession) {
                @Override
                public MediaDescriptionCompat getMediaDescription(Player player, int windowIndex) {
                    return Samples.getMediaDescriptionFromMediaItem(context, songItemsList.get(windowIndex));
                }
            });
            mediaSessionConnector.setPlayer(player, null);

            try {
                songTitle = songItemsList.get(player.getCurrentWindowIndex()).getTitle();
            } catch (Exception ignored) {

            }

            player.addListener(new Player.EventListener() {
                @Override
                public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

                }

                @Override
                public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                    try {
                        songTitle = songItemsList.get(player.getCurrentWindowIndex()).getTitle();
                    } catch (Exception ignored) {

                    }

                    ////Log.d("MP3Downloader", "Playback track berganti dan nilainya = " + songTitle);
                }

                @Override
                public void onLoadingChanged(boolean isLoading) {

                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    try {
                        songTitle = songItemsList.get(player.getCurrentWindowIndex()).getTitle();
                    } catch (Exception ignored) {

                    }

                }

                @Override
                public void onRepeatModeChanged(int repeatMode) {

                }

                @Override
                public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                }

                @Override
                public void onPlayerError(ExoPlaybackException error) {

                }

                @Override
                public void onPositionDiscontinuity(int reason) {

                }

                @Override
                public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                }

                @Override
                public void onSeekProcessed() {
                    try {
                        songTitle = songItemsList.get(player.getCurrentWindowIndex()).getTitle();
                    } catch (Exception ignored) {

                    }

                    ////Log.d("MP3Downloader", "Playback seek berganti nilainya = " + songTitle);

                }
            });
        } else {

            songItemsList = songItems;

            final Context context = this;
            if (player != null) {
                player.release();
                mediaSession.release();
                mediaSessionConnector.setPlayer(null, null);
                playerNotificationManager.setPlayer(null);
            }
            player = ExoPlayerFactory.newSimpleInstance(context, new DefaultTrackSelector());
            setRepeatMode(SharedPref.getInt(Constants.PLAYER_OPTIONS_REPEAT));
            setShuffleMode(SharedPref.getBol(Constants.PLAYER_OPTIONS_SHUFFLE));

            String userAgent = Util.getUserAgent(context, getString(R.string.app_name));
            // Default parameters, except allowCrossProtocolRedirects is true
            DefaultHttpDataSourceFactory httpDataSourceFactory = new DefaultHttpDataSourceFactory(
                    userAgent,
                    null /* listener */,
                    DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS,
                    DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS,
                    true /* allowCrossProtocolRedirects */
            );
            DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(
                    context, null, httpDataSourceFactory);
          /*DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(
                  context, Util.getUserAgent(context, getString(R.string.app_name)));*/
            CacheDataSourceFactory cacheDataSourceFactory = new CacheDataSourceFactory(
                    DownloadUtil.getCache(context),
                    dataSourceFactory,
                    CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);
            setupSources(cacheDataSourceFactory, songItems, index_position);
            playerNotificationManager = new PlayerNotificationManager(
                    context,
                    PLAYBACK_CHANNEL_ID,
                    PLAYBACK_NOTIFICATION_ID,
                    new MediaDescriptionAdapter() {
                        @Override
                        public String getCurrentContentTitle(Player player) {
                            try {
                                return songItemsList.get(player.getCurrentWindowIndex()).getTitle();
                            } catch (Exception e) {
                                return "Play a song!";
                            }
                        }

                        @Nullable
                        @Override
                        public PendingIntent createCurrentContentIntent(Player player) {
                            Intent intent = new Intent(context, PlayerActivity.class);

                            return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        }

                        @Nullable
                        @Override
                        public String getCurrentContentText(Player player) {
                            try {
                                return songItemsList.get(player.getCurrentPeriodIndex()).getArtist();
                            } catch (Exception e) {
                                return "Play a song!";
                            }

                        }

                        @Nullable
                        @Override
                        public Bitmap getCurrentLargeIcon(Player player, BitmapCallback callback) {
                            try {
                                return MediaItem.getBitmap(context);
                            } catch (Exception e) {
                                return Samples.getDefaultBitmap(AudioPlayerService.this);
                            }

                        }
                    },
                    new CustomActionNotification(context, songItemsList)
            );
            /*playerNotificationManager = PlayerNotificationManager.createWithNotificationChannel(
                    context,
                    PLAYBACK_CHANNEL_ID,
                    R.string.playback_channel_name,
                    PLAYBACK_NOTIFICATION_ID,
                    new MediaDescriptionAdapter() {
                        @Override
                        public String getCurrentContentTitle(Player player) {
                            try {
                                return songItems.get(player.getCurrentWindowIndex()).getTitle();
                            } catch (Exception e) {
                                return "Play a song!";
                            }
                        }

                        @Nullable
                        @Override
                        public PendingIntent createCurrentContentIntent(Player player) {
                            Intent intent = new Intent(context, PlayerActivity.class);

                            return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        }

                        @Nullable
                        @Override
                        public String getCurrentContentText(Player player) {
                            try {
                                return songItemsList.get(player.getCurrentPeriodIndex()).getArtist();
                            } catch (Exception e) {
                                return "Play a song!";
                            }
                        }

                        @Nullable
                        @Override
                        public Bitmap getCurrentLargeIcon(Player player, BitmapCallback callback) {

                            try {
                                return MediaItem.getBitmap(context);
                            } catch (Exception e) {
                                return Samples.getDefaultBitmap(AudioPlayerService.this);
                            }
                        }

                    }
            );*/
            playerNotificationManager.setNotificationListener(new NotificationListener() {

                @Override
                public void onNotificationStarted(int notificationId, Notification notification) {
                    startForeground(notificationId, notification);
                }


                @Override
                public void onNotificationCancelled(int notificationId) {
//                    stopForeground(true);
                    stopSelf();
                }
            });

            playerNotificationManager.setSmallIcon(R.drawable.ic_notification_music);
            playerNotificationManager.setFastForwardIncrementMs(-1);
            playerNotificationManager.setRewindIncrementMs(-1);
            playerNotificationManager.setStopAction(null);
            playerNotificationManager.setPlayer(player);

            mediaSession = new MediaSessionCompat(context, MEDIA_SESSION_TAG);
            mediaSession.setActive(true);
            playerNotificationManager.setMediaSessionToken(mediaSession.getSessionToken());

            mediaSessionConnector = new MediaSessionConnector(mediaSession);
            mediaSessionConnector.setQueueNavigator(new TimelineQueueNavigator(mediaSession) {
                @Override
                public MediaDescriptionCompat getMediaDescription(Player player, int windowIndex) {
                    return MediaItem.getMediaDescription(context, songItemsList.get(windowIndex));
                }
            });
            mediaSessionConnector.setPlayer(player, null);

            try {
                songTitle = songItemsList.get(player.getCurrentWindowIndex()).getTitle();
            } catch (Exception ignored) {

            }

            player.addListener(new Player.EventListener() {
                @Override
                public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

                }

                @Override
                public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                    try {
                        songTitle = songItemsList.get(player.getCurrentWindowIndex()).getTitle();
                    } catch (Exception ignored) {

                    }

                    ////Log.d("MP3Downloader", "Playback track berganti dan nilainya = " + songTitle);
                }

                @Override
                public void onLoadingChanged(boolean isLoading) {

                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    try {
                        songTitle = songItemsList.get(player.getCurrentWindowIndex()).getTitle();
                    } catch (Exception ignored) {

                    }

                }

                @Override
                public void onRepeatModeChanged(int repeatMode) {

                }

                @Override
                public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                }

                @Override
                public void onPlayerError(ExoPlaybackException error) {

                }

                @Override
                public void onPositionDiscontinuity(int reason) {

                }

                @Override
                public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                }

                @Override
                public void onSeekProcessed() {
                    try {
                        songTitle = songItemsList.get(player.getCurrentWindowIndex()).getTitle();
                    } catch (Exception ignored) {

                    }

                    ////Log.d("MP3Downloader", "Playback seek berganti nilainya = " + songTitle);

                }
            });
        }
    }

    private void setupSources(CacheDataSourceFactory cacheDataSourceFactory, ArrayList<MediaItem> songItems, int index_position) {

        concatenatingMediaSource = new ConcatenatingMediaSource();
        for (MediaItem songItem : songItems) {
            //LogUtils.log("URL track Url = " + songItem.getTrackUrl());
            try {
                Download download = DBHelper.getDownload(songItem.getMediaId());
                if (download != null) {
                    //LogUtils.log("Download is available = " + songItem.getDownload(this).getFilePath());
                    MediaSource mediaSource = new ExtractorMediaSource.Factory(cacheDataSourceFactory)
                            .createMediaSource(Uri.parse(songItem.getDownload(this).getFilePath()));
                    concatenatingMediaSource.addMediaSource(mediaSource);
                } else {
                    //LogUtils.log("Download is not available = " + songItem.getTrackUrl());
                    MediaSource mediaSource = new ExtractorMediaSource.Factory(cacheDataSourceFactory)
                            .createMediaSource(Uri.parse(songItem.getTrackUrl()));
                    concatenatingMediaSource.addMediaSource(mediaSource);
                }

            } catch (Exception e) {
                ////Log.d("MP3Downloader", "Error = " + e.getLocalizedMessage());
            }

        }
        player.prepare(concatenatingMediaSource);
        player.seekTo(index_position, 0);
        player.setPlayWhenReady(true);

    }

    private void setupSourcesStart(CacheDataSourceFactory cacheDataSourceFactory, ArrayList<MediaItem> songItems, int index_position) {

        concatenatingMediaSource = new ConcatenatingMediaSource();
        for (MediaItem songItem : songItems) {
//            ////Log.d("MP3Downloader", "URL download = " + songItem.uri);
            try {
                MediaSource mediaSource = new ExtractorMediaSource.Factory(cacheDataSourceFactory)
                        .createMediaSource(Uri.parse(songItem.getTrackUrl()));
                concatenatingMediaSource.addMediaSource(mediaSource);
            } catch (Exception e) {
                ////Log.d("MP3Downloader", "Error = " + e.getLocalizedMessage());
            }
        }
        player.prepare(concatenatingMediaSource);
        player.seekTo(index_position, 0);
        player.setPlayWhenReady(false);
    }

    public void addNewSongtoQuee(MediaItem mediaItem) {
        try {
            final Context context = this;

            String userAgent = Util.getUserAgent(context, getString(R.string.app_name));
            // Default parameters, except allowCrossProtocolRedirects is true
            DefaultHttpDataSourceFactory httpDataSourceFactory = new DefaultHttpDataSourceFactory(
                    userAgent,
                    null /* listener */,
                    DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS,
                    DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS,
                    true /* allowCrossProtocolRedirects */
            );

            DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(
                    context, null, httpDataSourceFactory);
            CacheDataSourceFactory cacheDataSourceFactory = new CacheDataSourceFactory(
                    DownloadUtil.getCache(context),
                    dataSourceFactory,
                    CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);
            Download download = DBHelper.getDownload(mediaItem.getMediaId());
            if (download != null) {
                MediaSource mediaSource = new ExtractorMediaSource.Factory(cacheDataSourceFactory)
                        .createMediaSource(Uri.parse(mediaItem.getDownload(this).getFilePath()));
                concatenatingMediaSource.addMediaSource(mediaSource);
            } else {
                MediaSource mediaSource = new ExtractorMediaSource.Factory(cacheDataSourceFactory)
                        .createMediaSource(Uri.parse(mediaItem.getTrackUrl()));
                concatenatingMediaSource.addMediaSource(mediaSource);
            }
            songItemsList.add(mediaItem);

        } catch (Exception e) {
//            Log.d("MP3Downloader", "Error from service = " + e.getLocalizedMessage());
        }
    }

    public void removeAudio(int index) {
        concatenatingMediaSource.removeMediaSource(index);
        songItemsList.remove(index);
    }

    public void playSingleSong(final MediaItem mediaItem) {
        songItemsList.clear();
        songItemsList.add(mediaItem);
        if (player != null) {
            player.release();
            mediaSession.release();
            mediaSessionConnector.setPlayer(null, null);
            playerNotificationManager.setPlayer(null);
        }
        final Context context = this;
        player = ExoPlayerFactory.newSimpleInstance(context, new DefaultTrackSelector());

        String userAgent = Util.getUserAgent(context, getString(R.string.app_name));
        // Default parameters, except allowCrossProtocolRedirects is true
        DefaultHttpDataSourceFactory httpDataSourceFactory = new DefaultHttpDataSourceFactory(
                userAgent,
                null /* listener */,
                DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS,
                DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS,
                true /* allowCrossProtocolRedirects */
        );

        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(
                context, null, httpDataSourceFactory);
        CacheDataSourceFactory cacheDataSourceFactory = new CacheDataSourceFactory(
                DownloadUtil.getCache(context),
                dataSourceFactory,
                CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);

        MediaSource mediaSource = new ExtractorMediaSource.Factory(cacheDataSourceFactory)
                .createMediaSource(Uri.parse(mediaItem.getTrackUrl()));
        concatenatingMediaSource.addMediaSource(mediaSource);

        playerNotificationManager = PlayerNotificationManager.createWithNotificationChannel(
                context,
                PLAYBACK_CHANNEL_ID,
                R.string.playback_channel_name,
                PLAYBACK_NOTIFICATION_ID,
                new MediaDescriptionAdapter() {
                    @Override
                    public String getCurrentContentTitle(Player player) {
                        return mediaItem.getTitle();
                    }

                    @Nullable
                    @Override
                    public PendingIntent createCurrentContentIntent(Player player) {
                        Intent intent = new Intent(context, PlayerActivity.class);

                        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    }

                    @Nullable
                    @Override
                    public String getCurrentContentText(Player player) {
                        return mediaItem.getArtist();
                    }

                    @Nullable
                    @Override
                    public Bitmap getCurrentLargeIcon(Player player, BitmapCallback callback) {
                        try {
                            return MediaItem.getBitmap(context);
                        } catch (Exception e) {
                            return Samples.getDefaultBitmap(AudioPlayerService.this);
                        }

                    }
                }
        );
        playerNotificationManager.setNotificationListener(new NotificationListener() {
            @Override
            public void onNotificationStarted(int notificationId, Notification notification) {
                startForeground(notificationId, notification);
            }

            @Override
            public void onNotificationCancelled(int notificationId) {
                stopForeground(false);
                stopSelf();
            }
        });
        playerNotificationManager.setPlayer(player);

        mediaSession = new MediaSessionCompat(context, MEDIA_SESSION_TAG);
        mediaSession.setActive(true);
        playerNotificationManager.setMediaSessionToken(mediaSession.getSessionToken());

        mediaSessionConnector = new MediaSessionConnector(mediaSession);
        mediaSessionConnector.setQueueNavigator(new TimelineQueueNavigator(mediaSession) {
            @Override
            public MediaDescriptionCompat getMediaDescription(Player player, int windowIndex) {
                return Samples.getMediaDescriptionFromMediaItem(context, mediaItem);
            }
        });
        mediaSessionConnector.setPlayer(player, null);

        player.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                songTitle = mediaItem.getTitle();
                ////Log.d("MP3Downloader", "Playback track berganti dan nilainya = " + songTitle);
            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {
                songTitle = mediaItem.getTitle();
                ////Log.d("MP3Downloader", "Playback seek berganti nilainya = " + songTitle);

            }
        });
    }

    public void removeMediaPlaylistByIndex(int index) {
        concatenatingMediaSource.removeMediaSource(index);
    }

    private void register_stopMusicPlayerService() {
        //Register playNewMedia receiver
        IntentFilter filter = new IntentFilter(BROADCAST_STOP_MUSIC_PLAYER_SERVICE);
        registerReceiver(stopMusicPlayerService, filter);
    }

    private void register_addNewAudio() {
        //Register playNewMedia receiver
        IntentFilter filter = new IntentFilter(BROADCAST_ADD_NEW_AUDIO);
        registerReceiver(addNewAudio, filter);
    }

    private void register_removeAudio() {
        //Register playNewMedia receiver
        IntentFilter filter = new IntentFilter(BROADCAST_REMOVE_AUDIO);
        registerReceiver(removeAudio, filter);
    }

    private void register_StopMusicPlayer() {
        //Register stop music player receiver
        IntentFilter filter = new IntentFilter(BROADCAST_STOP_MUSIC_PLAYER);
        registerReceiver(stopMusicPlayer, filter);
    }

    public class MyBinder extends Binder {
        public AudioPlayerService getService() {
            return AudioPlayerService.this;
        }
    }

}
