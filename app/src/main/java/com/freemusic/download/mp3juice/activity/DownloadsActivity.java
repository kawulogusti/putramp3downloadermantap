package com.freemusic.download.mp3juice.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.freemusic.download.mp3juice.R;
import com.freemusic.download.mp3juice.adapter.DownloadsMediaItemAdapter;
import com.freemusic.download.mp3juice.adapter.ItemAddToPlaylistAdapter;
import com.freemusic.download.mp3juice.api.ConfApp;
import com.freemusic.download.mp3juice.api.IpResponse;
import com.freemusic.download.mp3juice.ext.ExtractorException;
import com.freemusic.download.mp3juice.ext.YExtractor;
import com.freemusic.download.mp3juice.ext.model.YMedia;
import com.freemusic.download.mp3juice.ext.utils.LogUtils;
import com.freemusic.download.mp3juice.model.Download;
import com.freemusic.download.mp3juice.model.MediaItem;
import com.freemusic.download.mp3juice.model.PlaylistPost;
import com.freemusic.download.mp3juice.service.AudioPlayerService;
import com.freemusic.download.mp3juice.service.DownloadService;
import com.freemusic.download.mp3juice.utils.AppRaterHelper;
import com.freemusic.download.mp3juice.utils.Constants;
import com.freemusic.download.mp3juice.utils.DBHelper;
import com.freemusic.download.mp3juice.utils.HelperUtils;
import com.freemusic.download.mp3juice.utils.JdkmdenJav;
import com.freemusic.download.mp3juice.utils.SharedPref;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.startapp.sdk.ads.banner.Banner;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.StartAppSDK;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

public class DownloadsActivity extends AppCompatActivity implements ServiceConnection {

    static {
        System.loadLibrary("realm-lib-sok");
    }

    //data
    List<MediaItem> mediaItems = new ArrayList<>();
    //exoplayer
    private PlayerView playerView;
    private SimpleExoPlayer player;
    private TextView textViewSongTitle;
    private ImageView iv_thumbnail_playnow;
    private boolean mBound = false;
    private AudioPlayerService mService;
    private MediaItem currentMediaItem;
    //ads
    private AdView mAdView;
    private AdRequest adRequest;
    private InterstitialAd mInterstitialAd;
    //startapp ads
    private Banner banner;
    //views
    private Toolbar toolbar;
    private SearchView searchView;
    private RecyclerView recyclerView;
    private DownloadsMediaItemAdapter adapter;
    private TextView textViewNoData;
    //conf
    private ConfApp confApp;
    private IpResponse ipResponse;
    private boolean isPlaySourceAfterServiceEnd = false;
    private ArrayList<MediaItem> currentPlayMediaItems = new ArrayList<>();
    private int currentPosition = 0;
    private BottomSheetDialog mBottomSheetDialog;
    private RecyclerView recyclerViewPlayList;
    private ItemAddToPlaylistAdapter itemAddToPlaylistAdapter;
    private MaterialAlertDialogBuilder dialogAddPlaylist;
    private MediaItem localTracItemforPlaylist;

    @Override
    public void onServiceConnected(ComponentName name, IBinder binder) {
        AudioPlayerService.MyBinder audioServiceBiner = (AudioPlayerService.MyBinder) binder;
        mService = audioServiceBiner.getService();
        mBound = true;
        player = mService.getPlayer();
        setupPlayer();

        if (isPlaySourceAfterServiceEnd) {
            if (currentPlayMediaItems.size() > 0) {
                initializePlayer(currentPlayMediaItems, currentPosition);
            }
        }
    }

    //add new playlist dialog

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mBound = false;
        mService = null;
    }

    @Override
    protected void onStart() {
        super.onStart();
        runAudioPlayerService();
    }

    private void runAudioPlayerService() {
        try {
            Intent intent = new Intent(this, AudioPlayerService.class);
//        Util.startForegroundService(this, intent);
            bindService(intent, this, Context.BIND_AUTO_CREATE);
//        startService(intent);
        } catch (Exception e) {
            Toast.makeText(DownloadsActivity.this, "An error occured while running a service", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloads);
        toolbar = findViewById(R.id.toolbar);
        searchView = findViewById(R.id.searchView);
        setupSearchView();
        recyclerView = findViewById(R.id.recyclerView);
        textViewNoData = findViewById(R.id.textViewNoData);

        //toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //add to playlist dialog
        setupAddToPlaylistDialog();

        confApp = DBHelper.getConfApp();
        ipResponse = DBHelper.getConfAppIp();

        //player
        playerView = findViewById(R.id.playerView);

        //ads
        mAdView = findViewById(R.id.adView);

        //startapp ads
        banner = findViewById(R.id.startAppBanner);

        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser()) {
            //Log.d("TwitterDl", "Ads is: "+confApp.isAdEnabled());
            initializeAdSDK();

            if (confApp.isAdDownloadsEnabled()) {
                initializeBannerAds();
            } else {
                mAdView.setVisibility(View.GONE);
                banner.hideBanner();
                banner.setVisibility(View.GONE);
            }

            if (confApp.isAdIntersialDownloadsEnabled()) {
                initializeIntersialAds();
            }

        } else {
            //Log.d("TwitterDl", "Ads is: "+confApp.isAdEnabled());
            mAdView.setVisibility(View.GONE);
            banner.hideBanner();
            banner.setVisibility(View.GONE);
        }

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setNestedScrollingEnabled(true);

        try {
            bindDataToRecyclerViewAllDownloads();
        } catch (Exception ignored) {

        }


    }

    private void bindDataToRecyclerViewAllDownloads() {
        RealmList<Download> downloadRealmList = DBHelper.getAllDownloads();
        if (downloadRealmList != null) {
            convertRealmListToMediaItemList(downloadRealmList);
            if (mediaItems.size() <= 0) {
                textViewNoData.setVisibility(View.VISIBLE);
                textViewNoData.setText("Currently, there is no data on Downloads. 😥 Try to download a song.");
            } else {
                textViewNoData.setVisibility(View.VISIBLE);
                textViewNoData.setText("Found: " + mediaItems.size() + " items");
            }
            adapter = new DownloadsMediaItemAdapter(this, mediaItems);
            recyclerView.setAdapter(adapter);
        } else {
            textViewNoData.setVisibility(View.VISIBLE);
            textViewNoData.setText("Currently, there is no data on Downloads. 😥 Try to download a song.");
            adapter = new DownloadsMediaItemAdapter(this, mediaItems);
            recyclerView.setAdapter(adapter);
        }

    }

    private void setupAddToPlaylistDialog() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        View sheetView = getLayoutInflater().inflate(R.layout.layout_playlist_options_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        Button btn_addToPlaylist = (Button) sheetView.findViewById(R.id.btn_new_playlist);
        recyclerViewPlayList = (RecyclerView) sheetView.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewPlayList.setLayoutManager(linearLayoutManager);


        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                // Do something
            }
        });

        btn_addToPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showAddNewPlaylistDialog(false);

            }
        });

    }

    public void showAddNewPlaylistDialog(boolean isFromPplaylistFragment) {
        dialogAddPlaylist = new MaterialAlertDialogBuilder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_playlist_create_new, null);
        dialogAddPlaylist.setView(dialogView);
        dialogAddPlaylist.setCancelable(true);
        dialogAddPlaylist.setTitle(R.string.text_add_new_playlist);

        final EditText editText_playlist_title = (EditText) dialogView.findViewById(R.id.editText_playlist_title);

        dialogAddPlaylist.setPositiveButton("Create", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String playlistTitle = editText_playlist_title.getText().toString();
                DBHelper.createPlaylist(playlistTitle);
//                GeneralHelper.reduceCoinForPlaylist();
                mBottomSheetDialog.dismiss();
                showAddToPlaylistDialog(localTracItemforPlaylist);

            }
        });

        dialogAddPlaylist.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        dialogAddPlaylist.show();

    }

    public void showAddToPlaylistDialog(MediaItem mediaItem) {
        localTracItemforPlaylist = mediaItem;
        List<PlaylistPost> playlistList = DBHelper.getPlaylistList();

        itemAddToPlaylistAdapter = new ItemAddToPlaylistAdapter(this, playlistList, localTracItemforPlaylist);
        recyclerViewPlayList.setAdapter(itemAddToPlaylistAdapter);

        mBottomSheetDialog.show();
    }

    //search

    private void setupSearchView() {
        searchView.setIconifiedByDefault(false);
        searchView.clearFocus();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                bindDataToRecyclerView(DBHelper.getSearchResultsDownloads(s));
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                bindDataToRecyclerView(DBHelper.getSearchResultsDownloads(s));
                searchView.clearFocus();
                return false;
            }
        });
    }

    public void bindDataToRecyclerView(RealmList<Download> downloadRealmList) {
        try {
            if (downloadRealmList.size() <= 0) {
                textViewNoData.setVisibility(View.VISIBLE);
                textViewNoData.setText("Currently, there is no data on Downloads. 😥 Try to download a song.");
            } else {
                textViewNoData.setVisibility(View.VISIBLE);
                textViewNoData.setText("Found: " + downloadRealmList.size() + " items");
            }
            convertRealmListToMediaItemList(downloadRealmList);
            recyclerView.setNestedScrollingEnabled(true);
            adapter = new DownloadsMediaItemAdapter(this, mediaItems);
            recyclerView.setAdapter(adapter);
        } catch (Exception ignored) {

        }
    }

    private void convertRealmListToMediaItemList(RealmList<Download> downloadRealmList) {
        mediaItems.clear();
        for (Download download : downloadRealmList) {
            mediaItems.add(download.getMediaItem());
        }
    }

    //ads
    private void initializeAdSDK() {
        if (confApp != null && confApp.isAdTypeAdmobSecondActivity()) {
            MobileAds.initialize(this, getString(R.string.google_admob_app_id));
        } else {
            if (confApp != null && confApp.getAdSaId() != null) {
                StartAppSDK.init(this, confApp.getAdSaId(), confApp.getAdSaReturnEnabled());
            } else {
                StartAppSDK.init(this, getResources().getString(R.string.startapp_app_id), confApp.getAdSaReturnEnabled());
            }
            if (!confApp.getAdSaSplashEnabled()) {
                StartAppAd.disableSplash();
            }

           /* if (!SharedPref.getBol(Constants.GDPR_AGREE)) {
                if (ipResponse != null & Constants.GDPR_COUNTRIES.toLowerCase().contains(ipResponse.getName().toLowerCase())) {
                    StartAppSDK.setUserConsent (this,
                            "pas",
                            System.currentTimeMillis(),
                            true);
                    SharedPref.saveBol(Constants.GDPR_AGREE, true);
                } else {
                    SharedPref.saveBol(Constants.GDPR_AGREE, true);
                }
            }*/
        }
    }

    private void initializeIntersialAds() {
        if (confApp != null && confApp.isAdTypeAdmobSecondActivity()) {
            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(getString(R.string.google_admob_intersial_id));
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    requestIntersialAds();
                    if (HelperUtils.getRandomBoolean()) {
                        AppRaterHelper.showReviewDialog(DownloadsActivity.this);
//                        AppRaterHelper.showUpgradeDialog(DownloadsActivity.this);
                    }
                }
            });

        } else {

        }
    }

    private void initializeBannerAds() {
        if (confApp != null && confApp.isAdTypeAdmobSecondActivity()) {
            mAdView.setVisibility(View.VISIBLE);
            banner.hideBanner();
            banner.setVisibility(View.GONE);
            adRequest = new AdRequest.Builder()
                    //.addTestDevice("33BE2250B43518CCDA7DE426D04EE231")
                    .build();
            mAdView.loadAd(adRequest);
        } else {
            banner.showBanner();
            banner.setVisibility(View.VISIBLE);
            mAdView.setVisibility(View.GONE);
        }
    }

    public void showIntersialAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser() && confApp.isAdIntersialDownloadsEnabled()) {
            if (confApp.isAdTypeAdmobSecondActivity()) {
                if (mInterstitialAd.isLoaded() && HelperUtils.getRandomBoolean()) {
                    mInterstitialAd.show();
                } else {
                    requestIntersialAds();
                    if (HelperUtils.getRandomBoolean()) {
                        AppRaterHelper.showReviewDialog(DownloadsActivity.this);
                    }
                    //Log.d("TAG", "The interstitial wasn't loaded yet.");
                }
            } else {
                StartAppAd.showAd(this);
            }

        }
    }

    public void showIntersialAdsWithoutRandom() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser() && confApp.isAdIntersialDownloadsEnabled()) {
            if (confApp.isAdTypeAdmobSecondActivity()) {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    requestIntersialAds();
                    //Log.d("TAG", "The interstitial wasn't loaded yet.");
                }
            } else {
                StartAppAd.showAd(this);
            }

        }
    }

    public void requestIntersialAds() {
        if (confApp != null && confApp.isAdEnabled() && !confApp.isPremiumUser() && confApp.isAdIntersialDownloadsEnabled()) {
            if (confApp.isAdTypeAdmobSecondActivity()) {
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            } else {

            }

        }
    }


    //media player

    private void setupPlayer() {
        //player
        playerView.setUseController(true);
        playerView.showController();
        playerView.setControllerAutoShow(true);
        playerView.setControllerHideOnTouch(false);
        playerView.setPlayer(player);
        if (player == null) {
            playerView.setVisibility(View.GONE);
        } else {
            playerView.setVisibility(View.VISIBLE);
        }

        textViewSongTitle = findViewById(R.id.textViewSongTitle);
        textViewSongTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        textViewSongTitle.setSelected(true);
        textViewSongTitle.setSingleLine(true);

        textViewSongTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPlayer();
            }
        });

        iv_thumbnail_playnow = findViewById(R.id.iv_thumbnail_playnow);

        refreshNowPlaying();

        iv_thumbnail_playnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPlayer();
            }
        });

        setupPlayerEventListener();
        setTextViewNowPlaying(mService.getTitleSongNowPlaying());

        boolean shuffleMode = SharedPref.getBol(Constants.PLAYER_OPTIONS_SHUFFLE);
        player.setShuffleModeEnabled(shuffleMode);
        int repeatMode = SharedPref.getInt(Constants.PLAYER_OPTIONS_REPEAT);
        player.setRepeatMode(repeatMode);
    }

    private void refreshNowPlaying() {
        setTextViewNowPlaying(mService.getTitleSongNowPlaying());
        refreshThumbnailImage();
    }

    public SimpleExoPlayer getPlayer() {
        return player;
    }

    public void showPlayer() {
        showIntersialAds();
        startActivity(new Intent(DownloadsActivity.this, PlayerActivity.class));
        setPlayerTitleMarqueeText(player.getPlaybackState());
    }

    public void refreshThumbnailImage() {
        try {
            if (player != null && mService.currentSongList() != null && mService.currentSongList().size() > 0) {
//            Log.d("MP3Downloader", "Value thumbnail is: " + songLIst.get(player.getCurrentWindowIndex()).bitmapResource + " or " + songLIst.get(player.getCurrentWindowIndex()).getThumbnail());
                Glide.with(this)
                        .load(mService.currentSongList().get(player.getCurrentWindowIndex()).getImageUrl())
                        .placeholder(R.drawable.ic_musical_note)
                        .transform(new CircleCrop())
                        .transition(DrawableTransitionOptions.withCrossFade(100))
                        .into(iv_thumbnail_playnow);
            } else {
                Glide.with(this)
                        .load(R.drawable.iconmusic)
                        .placeholder(R.drawable.ic_musical_note)
                        .transform(new CircleCrop())
                        .transition(DrawableTransitionOptions.withCrossFade(100))
                        .into(iv_thumbnail_playnow);
            }
        } catch (Exception ignored) {

        }


    }

    public void setupPlayerEventListener() {
        player.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {
                ////Log.d("MP3Downloader", "Timeline Ganti");

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                ////Log.d("MP3Downloader", "Track Ganti");
                setPlayerTitleMarqueeText(player.getPlaybackState());
                DBHelper.setHistory(mService.currentSongList().get(player.getCurrentWindowIndex()));
            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

//                Log.d("MP3Downloader", "Playback onloadingchanged: " + isLoading);
            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (mService.currentSongList() != null) {
//                    Log.d("MP3Downloader", "Playback state: " + playbackState);
                    setPlayerTitleMarqueeText(playbackState);
                } else {
                    textViewSongTitle.setText(mService.getTitleSongNowPlaying());
                }
                ////Log.d("MP3Downloader", "XPlayerState Ganti " + playbackState);
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {
                ////Log.d("MP3Downloader", "Repeat mode Ganti " + repeatMode);
            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {
                ////Log.d("MP3Downloader", "Shuffle mode Ganti " + shuffleModeEnabled);
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
//                Log.d("MP3Downloader", "Error exo: " + error.getLocalizedMessage());

            }

            @Override
            public void onPositionDiscontinuity(int reason) {
                //Log.d("MP3Downloader", "Position Discontinuity = " + reason);

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
                ////Log.d("MP3Downloader", "Parameter Ganti = " + playbackParameters.speed);
            }

            @Override
            public void onSeekProcessed() {
                ////Log.d("MP3Downloader", "Seek processed Ganti");
                if (mService.currentSongList() != null) {
                    setPlayerTitleMarqueeText(player.getPlaybackState());
                }
            }
        });
    }

    public void initializePlayer(ArrayList<MediaItem> songItems, int index_position) {
        if (mBound) {

            playerView.setVisibility(View.VISIBLE);
            player = mService.getplayerInstance(songItems, index_position);
            playerView.setPlayer(player);

            setupPlayerEventListener();

        } else {
            runAudioPlayerService();
            isPlaySourceAfterServiceEnd = true;
            currentPlayMediaItems = songItems;
            currentPosition = index_position;
        }
    }

    public void setPlayerTitleMarqueeText(int status) {
        try {
            if (status == Player.STATE_BUFFERING) {
                textViewSongTitle.setText("Buffering.. " +
                        mService.getTitleSongNowPlaying());
                Glide.with(this)
                        .load(mService.currentSongList().get(player.getCurrentWindowIndex()).getImageUrl())
                        .placeholder(R.drawable.ic_musical_note)
                        .transform(new CircleCrop())
                        .transition(DrawableTransitionOptions.withCrossFade(100))
                        .into(iv_thumbnail_playnow);
            } else if (status == Player.STATE_READY) {
                setTextViewNowPlaying(mService.getTitleSongNowPlaying());
                Glide.with(this)
                        .load(mService.currentSongList().get(player.getCurrentWindowIndex()).getImageUrl())
                        .placeholder(R.drawable.ic_musical_note)
                        .transform(new CircleCrop())
                        .transition(DrawableTransitionOptions.withCrossFade(100))
                        .into(iv_thumbnail_playnow);
            } else if (status == Player.STATE_IDLE) {
                setTextViewNowPlaying("Player is idle. Try to play a song!");

            } else {
//                Log.d("MP3Downloader", "Error code: " + status);
                setTextViewNowPlaying("Player stopped.");
            }
//            updatePlayerFragmentViews();
            ////Log.d("MP3Downloader", "PlayerState Ganti " + status);
        } catch (Exception ignored) {
            ////Log.d("MP3Downloader", "Exception =  " + e.getLocalizedMessage());

        }
    }

    public void downloadFile(final MediaItem mediaItem) {
        Toast.makeText(DownloadsActivity.this, "Starting download: " + mediaItem.getTitle() + ".\nSee download progress on the notification.", Toast.LENGTH_LONG).show();
        new YExtractor(confApp, new YExtractor.ExtractorListner() {
            @Override
            public void onExtractionGoesWrong(ExtractorException e) {
                Intent intent = new Intent(DownloadService.BROADCAST_START_DOWNLOAD_FILE);
                intent.putExtra(DownloadService.MEDIA_ITEM, mediaItem);
                sendBroadcast(intent);
            }

            @Override
            public void onExtractionDone(List<YMedia> adativeStream) {
                String url = "";
                for (YMedia media : adativeStream) {
                    url = media.getUrl();
                }

                mediaItem.setDownloadLinkFast(url);
                mediaItem.setTrackUrl(url);
                mediaItem.setFastDl(true);
                Intent intent = new Intent(DownloadService.BROADCAST_START_DOWNLOAD_FILE);
                intent.putExtra(DownloadService.MEDIA_ITEM, mediaItem);
                sendBroadcast(intent);

                //LogUtils.log("Url is: " + url);
            }
        }).useDefaultLogin().Extract(JdkmdenJav.getServTubURL(mediaItem.getMediaId()));

    }

    public void playSingleSong(final ArrayList<MediaItem> songItems, final int index_position) {
        setCurrentMediaItem(songItems.get(index_position));
        Download download = DBHelper.getDownload(currentMediaItem.getMediaId());
        if (download != null) {
            setTextViewNowPlaying("Buffering.. - " + currentMediaItem.getTitle());
            initializePlayer(songItems, index_position);
        } else {
            setTextViewNowPlaying("Buffering.. " + currentMediaItem.getTitle());
            new YExtractor(confApp, new YExtractor.ExtractorListner() {
                @Override
                public void onExtractionGoesWrong(ExtractorException e) {
//                        Log.d("MP3Downloader", "Cannot play from another way.");
                    setTextViewNowPlaying("Buffering.. - " + currentMediaItem.getTitle());
                    initializePlayer(songItems, index_position);
                }


                @Override
                public void onExtractionDone(List<YMedia> adativeStream) {
                    String url = "";
                    for (YMedia media : adativeStream) {
                        url = media.getUrl();
                    }

                    songItems.get(index_position).setTrackUrl(url);
                    songItems.get(index_position).setDownloadLinkFast(url);
                    initializePlayer(songItems, index_position);

                    //LogUtils.log("Url is: " + url);

               /* new YMultiExtractor(confApp, new YMultiExtractor.ExtractorListner() {
                    @Override
                    public void onExtractionGoesWrong(ExtractorException e) {

                    }

                    @Override
                    public void onExtractionDone(List<YMedia> urlsmp3Media) {
                        for (YMedia media:urlsmp3Media) {
                            //LogUtils.log("Success media url: "+media.getUrl());
                        }
                    }
                }).useDefaultLogin().Extract(listMediaId(songItems));*/

//                        Log.d("MP3Downloader", "Success play from another way. - " + url);
                }

            }).useDefaultLogin().Extract(JdkmdenJav.getServTubURL(songItems.get(index_position).getMediaId()));

        }

    }

    private void setTextViewNowPlaying(String string) {
        textViewSongTitle.setText(string);
    }

    public MediaItem getCurrentMediaItem() {
        return currentMediaItem;
    }

    public void setCurrentMediaItem(MediaItem currentMediaItem) {
        this.currentMediaItem = currentMediaItem;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        runAudioPlayerService();
        bindDataToRecyclerViewAllDownloads();
        if (mService != null && player != null) {
            player = mService.getPlayer();
            if (player != null) {
                try {
                    setupPlayer();
                } catch (Exception e) {
                    Toast.makeText(DownloadsActivity.this, "An error occured.", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }
}
