package com.freemusic.download.mp3juice.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.CountDownTimer;
import android.os.IBinder;

import com.freemusic.download.mp3juice.ext.utils.LogUtils;

public class TimerService extends Service {

    public static final String BROADCAST_START_TIMER = "start_broadcast_timer";
    public static final String BROADCAST_STOP_TIMER = "stop_broadcast_timer";
    public static final String TIMER_MINUTE_VALUE = "timer_minute_value";

    private final IBinder mBinder = new MyBinder();
    private CountDownTimer start;
    private BroadcastReceiver startTimer = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            int minute = intent.getIntExtra(TIMER_MINUTE_VALUE, 0);
            //LogUtils.log("Timer is started in: " + minute + " minutes");
            setTimer(minute);
        }
    };
    private BroadcastReceiver stopTimer = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            //LogUtils.log("Trying to stop timer");
            stopTimerAction();
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {

        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        register_startTimer();
        register_stopTimer();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(startTimer);
        unregisterReceiver(stopTimer);
        //LogUtils.log("Timer service is destroyed");
    }

    public void setTimer(int m) {
        int minutes = m * 60;
        int total = minutes * 1000;
        start = new CountDownTimer(total, 1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                //LogUtils.log("Sending broadcast to stop music player");
                sendBroadcastStopMusicPlayer();
                start = null;
            }
        }.start();
    }

    public void stopTimerAction() {
        start.cancel();
        start = null;
    }

    private void register_startTimer() {
        //Register startdownloadfile receiver
        //LogUtils.log("Register start timer receiver");
        IntentFilter filter = new IntentFilter(BROADCAST_START_TIMER);
        registerReceiver(startTimer, filter);
    }

    private void register_stopTimer() {
        //Register startdownloadfile receiver
        //LogUtils.log("Register stop timer receiver");
        IntentFilter filter = new IntentFilter(BROADCAST_STOP_TIMER);
        registerReceiver(stopTimer, filter);
    }

    private void sendBroadcastStopMusicPlayer() {
        //LogUtils.log("Sending stop music player broadcast");
        Intent new_intent = new Intent();
        new_intent.setAction(AudioPlayerService.BROADCAST_STOP_MUSIC_PLAYER);
        sendBroadcast(new_intent);
    }

    public class MyBinder extends Binder {
        public TimerService getService() {
            return TimerService.this;
        }
    }
}
