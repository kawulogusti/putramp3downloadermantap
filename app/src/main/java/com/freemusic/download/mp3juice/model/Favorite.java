package com.freemusic.download.mp3juice.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Favorite extends RealmObject {

    @SerializedName("artist")
    private String artist;

    @SerializedName("imageUrl")
    private String imageUrl;

    @PrimaryKey
    @SerializedName("mediaId")
    private String mediaId;

    @SerializedName("title")
    private String title;

    @SerializedName("trackUrl")
    private String trackUrl;

    private Date recentlyPlayed;
    private boolean isFastDl;
    private String downloadLinkFast;

    public Favorite() {
        this.recentlyPlayed = new Date();
    }

    public Favorite(String artist, String imageUrl, String mediaId, String title, String trackUrl) {
        this.artist = artist;
        this.imageUrl = imageUrl;
        this.mediaId = mediaId;
        this.title = title;
        this.trackUrl = trackUrl;
        this.recentlyPlayed = new Date();
    }

    public Favorite(String artist, String imageUrl, String mediaId, String title, String trackUrl, boolean isFastDl, String downloadLinkFast) {
        this.artist = artist;
        this.imageUrl = imageUrl;
        this.mediaId = mediaId;
        this.title = title;
        this.trackUrl = trackUrl;
        this.recentlyPlayed = new Date();
        this.isFastDl = isFastDl;
        this.downloadLinkFast = downloadLinkFast;
    }

    public boolean isFastDl() {
        return isFastDl;
    }

    public void setFastDl(boolean fastDl) {
        isFastDl = fastDl;
    }

    public String getDownloadLinkFast() {
        return downloadLinkFast;
    }

    public void setDownloadLinkFast(String downloadLinkFast) {
        this.downloadLinkFast = downloadLinkFast;
    }

    public MediaItem getMediaItem() {
        return new MediaItem(this.artist, this.imageUrl, this.mediaId, this.title, this.trackUrl, this.isFastDl, this.downloadLinkFast);
    }

    public Date getRecentlyPlayed() {
        return recentlyPlayed;
    }

    public void setRecentlyPlayed(Date recentlyPlayed) {
        this.recentlyPlayed = recentlyPlayed;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTrackUrl() {
        return trackUrl;
    }

    public void setTrackUrl(String trackUrl) {
        this.trackUrl = trackUrl;
    }

    @Override
    public String toString() {
        return
                "ResultsItem{" +
                        "artist = '" + artist + '\'' +
                        ",imageUrl = '" + imageUrl + '\'' +
                        ",mediaId = '" + mediaId + '\'' +
                        ",title = '" + title + '\'' +
                        ",trackUrl = '" + trackUrl + '\'' +
                        "}";
    }
}