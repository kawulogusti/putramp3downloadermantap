package com.freemusic.download.mp3juice.api;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ConfApp extends RealmObject {

    @PrimaryKey
    @SerializedName("appName")
    private String appName;

    @SerializedName("serverUrl")
    private String serverUrl;

    @SerializedName("isAdTypeAdmob")
    private boolean isAdTypeAdmob;

    @SerializedName("isAdTypeAdmobSecondActivity")
    private boolean isAdTypeAdmobSecondActivity;

    @SerializedName("adSaId")
    private String adSaId;

    @SerializedName("adSaReturnEnabled")
    private boolean adSaReturnEnabled;

    @SerializedName("adSaSplashEnabled")
    private boolean adSaSplashEnabled;

    @SerializedName("isAdEnabled")
    private boolean isAdEnabled;

    @SerializedName("isAdMainEnabled")
    private boolean isAdMainEnabled;

    @SerializedName("isAdSearchEnabled")
    private boolean isAdSearchEnabled;

    @SerializedName("isAdDownloadsEnabled")
    private boolean isAdDownloadsEnabled;

    @SerializedName("isAdLibraryEnabled")
    private boolean isAdLibraryEnabled;

    @SerializedName("isAdIntersialMainEnabled")
    private boolean isAdIntersialMainEnabled;

    @SerializedName("isAdIntersialSearchEnabled")
    private boolean isAdIntersialSearchEnabled;

    @SerializedName("isAdIntersialDownloadsEnabled")
    private boolean isAdIntersialDownloadsEnabled;

    @SerializedName("isAdIntersialLibraryEnabled")
    private boolean isAdIntersialLibraryEnabled;

    @SerializedName("isAdPlayerEnabled")
    private boolean isAdPlayerEnabled;

    @SerializedName("version")
    private String version;

    @SerializedName("blackCountries")
    private String blackCountries;

    @SerializedName("lockPreviewThreshold")
    private int lockPreviewThreshold;

    @SerializedName("main_search")
    private boolean mainSearch;

    @SerializedName("cookies")
    private String cookies;

    @SerializedName("force_dialog")
    private boolean force_dialog;

    @SerializedName("new_url")
    private String new_url;

    @SerializedName("force_dialog_message")
    private String force_dialog_message;
    private boolean isPremiumUser;

    public ConfApp() {

    }

    public String getAdSaId() {
        return adSaId;
    }

    public void setAdSaId(String adSaId) {
        this.adSaId = adSaId;
    }

    public boolean getAdSaReturnEnabled() {
        return adSaReturnEnabled;
    }

    public void setAdSaReturnEnabled(boolean adSaReturnEnabled) {
        this.adSaReturnEnabled = adSaReturnEnabled;
    }

    public boolean getAdSaSplashEnabled() {
        return adSaSplashEnabled;
    }

    public void setAdSaSplashEnabled(boolean adSaSplashEnabled) {
        this.adSaSplashEnabled = adSaSplashEnabled;
    }

    public boolean isAdTypeAdmobSecondActivity() {
        return isAdTypeAdmobSecondActivity;
    }

    public void setAdTypeAdmobSecondActivity(boolean adTypeAdmobSecondActivity) {
        isAdTypeAdmobSecondActivity = adTypeAdmobSecondActivity;
    }

    public boolean isAdSaReturnEnabled() {
        return adSaReturnEnabled;
    }

    public boolean isAdSaSplashEnabled() {
        return adSaSplashEnabled;
    }

    public boolean isAdTypeAdmob() {
        return isAdTypeAdmob;
    }

    public void setAdTypeAdmob(boolean adTypeAdmob) {
        isAdTypeAdmob = adTypeAdmob;
    }

    public boolean isForce_dialog() {
        return force_dialog;
    }

    public void setForce_dialog(boolean force_dialog) {
        this.force_dialog = force_dialog;
    }

    public String getNew_url() {
        return new_url;
    }

    public void setNew_url(String new_url) {
        this.new_url = new_url;
    }

    public boolean isAdIntersialMainEnabled() {
        return isAdIntersialMainEnabled;
    }

    public void setAdIntersialMainEnabled(boolean adIntersialMainEnabled) {
        isAdIntersialMainEnabled = adIntersialMainEnabled;
    }

    public boolean isAdIntersialSearchEnabled() {
        return isAdIntersialSearchEnabled;
    }

    public void setAdIntersialSearchEnabled(boolean adIntersialSearchEnabled) {
        isAdIntersialSearchEnabled = adIntersialSearchEnabled;
    }

    public boolean isAdIntersialDownloadsEnabled() {
        return isAdIntersialDownloadsEnabled;
    }

    public void setAdIntersialDownloadsEnabled(boolean adIntersialDownloadsEnabled) {
        isAdIntersialDownloadsEnabled = adIntersialDownloadsEnabled;
    }

    public boolean isAdIntersialLibraryEnabled() {
        return isAdIntersialLibraryEnabled;
    }

    public void setAdIntersialLibraryEnabled(boolean adIntersialLibraryEnabled) {
        isAdIntersialLibraryEnabled = adIntersialLibraryEnabled;
    }

    public String getForce_dialog_message() {
        return force_dialog_message;
    }

    public void setForce_dialog_message(String force_dialog_message) {
        this.force_dialog_message = force_dialog_message;
    }

    public String getBlackCountries() {
        return blackCountries;
    }

    public void setBlackCountries(String blackCountries) {
        this.blackCountries = blackCountries;
    }

    public boolean isAdMainEnabled() {
        return isAdMainEnabled;
    }

    public void setAdMainEnabled(boolean adMainEnabled) {
        isAdMainEnabled = adMainEnabled;
    }

    public boolean isAdSearchEnabled() {
        return isAdSearchEnabled;
    }

    public void setAdSearchEnabled(boolean adSearchEnabled) {
        isAdSearchEnabled = adSearchEnabled;
    }

    public boolean isAdDownloadsEnabled() {
        return isAdDownloadsEnabled;
    }

    public void setAdDownloadsEnabled(boolean adDownloadsEnabled) {
        isAdDownloadsEnabled = adDownloadsEnabled;
    }

    public boolean isAdLibraryEnabled() {
        return isAdLibraryEnabled;
    }

    public void setAdLibraryEnabled(boolean adLibraryEnabled) {
        isAdLibraryEnabled = adLibraryEnabled;
    }

    public boolean isAdPlayerEnabled() {
        return isAdPlayerEnabled;
    }

    public void setAdPlayerEnabled(boolean adPlayerEnabled) {
        isAdPlayerEnabled = adPlayerEnabled;
    }

    public String getCookies() {
        return cookies;
    }

    public void setCookies(String cookies) {
        this.cookies = cookies;
    }

    public boolean isMainSearch() {
        return mainSearch;
    }

    public void setMainSearch(boolean mainSearch) {
        this.mainSearch = mainSearch;
    }

    public boolean isPremiumUser() {
        return isPremiumUser;
    }

    public void setPremiumUser(boolean premiumUser) {
        isPremiumUser = premiumUser;
    }

    public int getLockPreviewThreshold() {
        return lockPreviewThreshold;
    }

    public void setLockPreviewThreshold(int lockPreviewThreshold) {
        this.lockPreviewThreshold = lockPreviewThreshold;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public void setIsAdEnabled(boolean isAdEnabled) {
        this.isAdEnabled = isAdEnabled;
    }

    public boolean isAdEnabled() {
        return isAdEnabled;
    }

    public void setAdEnabled(boolean adEnabled) {
        isAdEnabled = adEnabled;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return
                "ConfApp{" +
                        "app_name = '" + appName + '\'' +
                        ",serverUrl = '" + serverUrl + '\'' +
                        ",version = '" + version + '\'' +
                        ",mainsearch = '" + mainSearch + '\'' +
                        ",version = '" + version + '\'' +
                        ",isAdEnabled = '" + isAdEnabled + '\'' +
                        ",isAdMainEnabled = '" + isAdMainEnabled + '\'' +
                        ",isAdSearchEnabled = '" + isAdSearchEnabled + '\'' +
                        ",isAdDownloadsEnabled = '" + isAdDownloadsEnabled + '\'' +
                        ",isAdLibraryEnabled = '" + isAdLibraryEnabled + '\'' +
                        ",isAdPlayerEnabled = '" + isAdPlayerEnabled + '\'' +
                        ",blackCountries = '" + blackCountries + '\'' +
                        ",isPremiumUser = '" + isPremiumUser + '\'' +
                        ",adSaId = '" + adSaId + '\'' +
                        ",adSaReturnEnabled = '" + adSaReturnEnabled + '\'' +
                        ",adSaSplashEnabled = '" + adSaSplashEnabled + '\'' +
                        ",isAdTypeAdmob = '" + isAdTypeAdmob + '\'' +
                        ",isAdTypeAdmobSecondActivity = '" + isAdTypeAdmobSecondActivity + '\'' +
                        "}";
    }
}
