package com.freemusic.download.mp3juice.ext.model;

public class StreamingData {
    private String hlsManifestUrl;
    private long expiresInSeconds;
    private YMedia[] formats;
    private YMedia[] adaptiveFormats;

    public YMedia[] getFormats() {
        return formats;
    }

    public void setFormats(YMedia[] formats) {
        this.formats = formats;
    }

    public YMedia[] getAdaptiveFormats() {
        return adaptiveFormats;
    }

    public void setAdaptiveFormats(YMedia[] adaptiveFormats) {
        this.adaptiveFormats = adaptiveFormats;
    }

    public YMedia getAudioYMedia() {
        YMedia yMedia = null;
        for (YMedia media : adaptiveFormats) {
            if (!media.isVideo() && media.getMimeType().contains("audio/mp4") && media.getAudioQuality().contains("AUDIO_QUALITY_MEDIUM")) {
                yMedia = media;
            }
        }
        return yMedia;
    }

    public long getExpiresInSeconds() {
        return expiresInSeconds;
    }

    public void setExpiresInSeconds(long expiresInSeconds) {
        this.expiresInSeconds = expiresInSeconds;
    }

    public String getHlsManifestUrl() {
        return hlsManifestUrl;
    }

    public void setHlsManifestUrl(String hlsManifestUrl) {
        this.hlsManifestUrl = hlsManifestUrl;
    }
}
