package com.freemusic.download.mp3juice.activity;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.airbnb.lottie.LottieAnimationView;
import com.freemusic.download.mp3juice.R;
import com.freemusic.download.mp3juice.api.ConfApp;
import com.freemusic.download.mp3juice.api.IpResponse;
import com.freemusic.download.mp3juice.api.RequestAPI;
import com.freemusic.download.mp3juice.api.ResponseLogs;
import com.freemusic.download.mp3juice.model.MediaItem;
import com.freemusic.download.mp3juice.model.PlaylistPost;
import com.freemusic.download.mp3juice.service.C;
import com.freemusic.download.mp3juice.utils.Constants;
import com.freemusic.download.mp3juice.utils.DBHelper;
import com.freemusic.download.mp3juice.utils.JdkmdenJav;
import com.freemusic.download.mp3juice.utils.SharedPref;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.StartAppSDK;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

import io.realm.Realm;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.freemusic.download.mp3juice.utils.Constants.FIRST_RUN_APP_CONF;

public class SplashActivity extends AppCompatActivity {

    static {
        System.loadLibrary("realm-lib-sok");
    }

    private LottieAnimationView animationView;
    private CountDownTimer waitTimer;
    private ArrayList<String> listString;
    private IpResponse ipResponse;
    private ConfApp confApp;
    private String appVersion = "emptyAppVersion";
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        animationView = findViewById(R.id.animation_view);
        animationView.playAnimation();
        textView = findViewById(R.id.tv_loading);

        StartAppSDK.init(this, getResources().getString(R.string.startapp_app_id), false);
        StartAppAd.disableSplash();

        //debug keys encryption
//        JdkmdenJav.getServ();
//        JdkmdenJav.getWorker();

        //create notification channel
        createNotificationChannelAudioPlayerNotification();
        createNotificationChannelDownloadsNotification();

        String uniqueID = UUID.randomUUID().toString();
        if (SharedPref.getString(Constants.UID) == null) {
            SharedPref.saveString(Constants.UID, uniqueID);
        }

        if (SharedPref.getString(Constants.FIRST_CREATE_PLAYLIST) == null) {
            Realm realm = null;
            try { // I could use try-with-resources here
                realm = Realm.getDefaultInstance();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        //Log.d("TwitterDl", "Write succesfull configuration to realm");
                        String uniqueID = UUID.randomUUID().toString();
                        PlaylistPost playlistPost = new PlaylistPost(uniqueID, "Liked Songs");
                        realm.insertOrUpdate(playlistPost);
                        SharedPref.saveString(Constants.FIRST_CREATE_PLAYLIST, uniqueID);
                    }

                });

            } finally {
                realm.close();
            }
        }

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            appVersion = pInfo.versionName;
        } catch (Exception ignored) {
            appVersion = "0.0";
        }


        ipResponse = DBHelper.getConfAppIp();
        confApp = DBHelper.getConfApp();
        /*if (confApp != null) {
            //LogUtils.log("Conf app splash: " + confApp.toString());
        } else {
            //LogUtils.log("Conf app is null");
        }*/

        if (SharedPref.getString(Constants.MY_PB_IP) == null) {
            getPublicIp();
        } else if (SharedPref.getString(Constants.MY_PB_IP).equalsIgnoreCase("1.1.1.1")) {
            getPublicIp();
        } else {
            getAppConf();
        }
    }

    private void getPublicIp() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.ipify.org/")
                .build();
        final RequestAPI service = retrofit.create(RequestAPI.class);

        Call<ResponseBody> call = service.getPublicIp();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null) {
                    try {
                        String pbIp = response.body().string();
//                            Log.d("MP3Downloader", "Ip response: "+pbIp);
                        SharedPref.saveString(Constants.MY_PB_IP, pbIp);
                    } catch (IOException e) {

                    } finally {
                        getAppConf();
                    }
                } else {
                    SharedPref.saveString(Constants.MY_PB_IP, "1.1.1.1");
                    getAppConf();
//                        Log.d("MP3Downloader", "Ip response null : "+response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                SharedPref.saveString(Constants.MY_PB_IP, "1.1.1.1");
                getAppConf();
//                    Log.d("MP3Downloader", "Ip response error: "+t.getLocalizedMessage());
            }
        });
    }

    private void createNotificationChannelAudioPlayerNotification() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel channel = new NotificationChannel(C.PLAYBACK_CHANNEL_ID, name, importance);
            channel.setDescription(description);
            channel.enableLights(false);
            channel.enableVibration(false);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void createNotificationChannelDownloadsNotification() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(Constants.CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void startMainActivity() {
        IpResponse ipResponse = DBHelper.getConfAppIp();
        if (!SharedPref.getBol(Constants.FIRST_RUN)) {
            SharedPref.saveBol(Constants.FIRST_RUN, true);
            if (ipResponse != null) {
                try {
                    saveLogs(ipResponse, "NEW");
                } catch (Exception ignored) {
//                    Log.d("MP3Downloader", "Error: "+ignored.getLocalizedMessage());
                }

            }
        } else {
            try {
                saveLogs(ipResponse, "EXIST");
            } catch (Exception ignored) {
//                Log.d("MP3Downloader", "Error: "+ignored.getLocalizedMessage());
            }

        }
    }

    private void runMainActivity() {
        SharedPref.saveBol(Constants.IS_JSON_INSERTED_TO_DB, true);
        final Intent intent = new Intent(SplashActivity.this, MainActivity.class);

        waitTimer = new CountDownTimer(3000, 1000) {

            public void onTick(long millisUntilFinished) {
                textView.setText(getResources().getText(R.string.ready_to_lauch));
            }

            public void onFinish() {
                startActivity(intent);
                finish();
            }
        }.start();
    }

    private void saveLogs(IpResponse ipResponse, final String message) {
        if (ipResponse != null) {
            Retrofit retrofit2 = new Retrofit.Builder()
                    .baseUrl(JdkmdenJav.getWorker())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            final RequestAPI service2 = retrofit2.create(RequestAPI.class);

            Call<ResponseLogs> call2 = service2.getLogs(SharedPref.getString(Constants.UID), ipResponse.getName(), ipResponse.getPackageName(), ipResponse.getAppVersion(), message);
            call2.enqueue(new Callback<ResponseLogs>() {
                @Override
                public void onResponse(Call<ResponseLogs> call, Response<ResponseLogs> response) {
                    //LogUtils.log("Success to write logs message: "+message);
                    runMainActivity();
                }

                @Override
                public void onFailure(Call<ResponseLogs> call, Throwable t) {
                    //LogUtils.log("Failed to write logs message: "+message);
                    runMainActivity();
                }
            });
        }
//

    }

    private void startTimer() {
        waitTimer = new CountDownTimer(200, 1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {

                startMainActivity();

            }
        }.start();
    }

    private void getAppConf() {
        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(Constants.BASE_URL_DB)
                .baseUrl(JdkmdenJav.getWorker())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final RequestAPI service = retrofit.create(RequestAPI.class);

        Call<ConfApp> callConfApp = service.getConfApp(appVersion);
        callConfApp.enqueue(new Callback<ConfApp>() {
            @Override
            public void onResponse(Call<ConfApp> call, final Response<ConfApp> response) {
                //LogUtils.log("Success to get app conf.. "+ response.toString());
                if (response.body() != null) {
                    Realm realm = null;
                    try { // I could use try-with-resources here
                        realm = Realm.getDefaultInstance();
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                //Log.d("TwitterDl", "Write succesfull configuration to realm");
                                //LogUtils.log("Value of response body is: "+response.body().toString());
                                ConfApp confAppLocal = response.body();
                                if (confApp != null && confApp.isPremiumUser()) {
                                    confAppLocal.setMainSearch(true);
                                    confAppLocal.setForce_dialog(false);
                                    confAppLocal.setPremiumUser(confApp.isPremiumUser());
                                }
                                realm.insertOrUpdate(confAppLocal);
                                SharedPref.saveBol(FIRST_RUN_APP_CONF, true);
                                //LogUtils.log("Success write app conf: "+confAppLocal.toString());

                            }

                        });

                    } catch (Exception e) {
                        realm = Realm.getDefaultInstance();
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                //Log.d("TwitterDl", "Write failed configuration to realm");
                                ConfApp confApp = new ConfApp();
                                confApp.setAppName(getResources().getString(R.string.app_slug));
                                confApp.setLockPreviewThreshold(15);
                                confApp.setServerUrl("http://mp3downloaderforfree.com/api/");
                                confApp.setVersion("1.0");
                                confApp.setMainSearch(true);
                                confApp.setIsAdEnabled(true);
                                confApp.setAdIntersialMainEnabled(true);
                                confApp.setAdMainEnabled(true);
                                confApp.setAdSearchEnabled(false);
                                confApp.setAdIntersialSearchEnabled(false);
                                confApp.setAdDownloadsEnabled(false);
                                confApp.setAdIntersialDownloadsEnabled(false);
                                confApp.setAdLibraryEnabled(false);
                                confApp.setAdIntersialLibraryEnabled(false);
                                confApp.setAdPlayerEnabled(false);
                                confApp.setAdTypeAdmob(true);
                                confApp.setAdSaId(getResources().getString(R.string.startapp_app_id));
                                confApp.setAdSaReturnEnabled(false);
                                confApp.setAdSaSplashEnabled(false);
                                realm.insertOrUpdate(confApp);
                            }

                        });
                    } finally {
                        realm.close();
                        if (ipResponse == null) {
                            getAppConfIp();
                        } else if(ipResponse.getCity().equalsIgnoreCase("Francisco")) {
                            getAppConfIp();
                        } else {
                            launchProcess();
                        }
                    }
                } else {
                    if (!SharedPref.getBol(FIRST_RUN_APP_CONF)) {
                        Realm realm = null;
                        try { // I could use try-with-resources here
                            realm = Realm.getDefaultInstance();
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    //Log.d("TwitterDl", "Write failed configuration to realm");
                                    ConfApp confApp = new ConfApp();
                                    confApp.setAppName(getResources().getString(R.string.app_slug));
                                    confApp.setLockPreviewThreshold(15);
                                    confApp.setServerUrl("http://mp3downloaderforfree.com/api/");
                                    confApp.setVersion("1.0");
                                    confApp.setMainSearch(true);
                                    confApp.setIsAdEnabled(true);
                                    confApp.setAdIntersialMainEnabled(true);
                                    confApp.setAdMainEnabled(true);
                                    confApp.setAdSearchEnabled(false);
                                    confApp.setAdIntersialSearchEnabled(false);
                                    confApp.setAdDownloadsEnabled(false);
                                    confApp.setAdIntersialDownloadsEnabled(false);
                                    confApp.setAdLibraryEnabled(false);
                                    confApp.setAdIntersialLibraryEnabled(false);
                                    confApp.setAdPlayerEnabled(false);
                                    confApp.setAdTypeAdmob(true);
                                    confApp.setAdSaId(getResources().getString(R.string.startapp_app_id));
                                    confApp.setAdSaReturnEnabled(false);
                                    confApp.setAdSaSplashEnabled(false);
                                    realm.insertOrUpdate(confApp);
                                }

                            });

                        } finally {
                            realm.close();
                            if (ipResponse == null) {
                                getAppConfIp();
                            } else if(ipResponse.getCity().equalsIgnoreCase("Francisco")) {
                                getAppConfIp();
                            } else {
                                launchProcess();
                            }
                        }
                    } else {
                        if (ipResponse == null) {
                            getAppConfIp();
                        } else if(ipResponse.getCity().equalsIgnoreCase("Francisco")) {
                            getAppConfIp();
                        } else {
                            launchProcess();
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<ConfApp> call, final Throwable t) {
                //LogUtils.log("Failed to get app conf.. - " + t.getLocalizedMessage());
                if (!SharedPref.getBol(FIRST_RUN_APP_CONF)) {
                    Realm realm = null;
                    try { // I could use try-with-resources here
                        realm = Realm.getDefaultInstance();
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                //Log.d("TwitterDl", "Write failed configuration to realm");
                                ConfApp confApp = new ConfApp();
                                confApp.setAppName(getResources().getString(R.string.app_slug));
                                confApp.setLockPreviewThreshold(15);
                                confApp.setServerUrl("http://mp3downloaderforfree.com/api/");
                                confApp.setVersion("1.0");
                                confApp.setMainSearch(true);
                                confApp.setIsAdEnabled(true);
                                confApp.setAdIntersialMainEnabled(true);
                                confApp.setAdMainEnabled(true);
                                confApp.setAdSearchEnabled(false);
                                confApp.setAdIntersialSearchEnabled(false);
                                confApp.setAdDownloadsEnabled(false);
                                confApp.setAdIntersialDownloadsEnabled(false);
                                confApp.setAdLibraryEnabled(false);
                                confApp.setAdIntersialLibraryEnabled(false);
                                confApp.setAdPlayerEnabled(false);
                                confApp.setAdTypeAdmob(true);
                                confApp.setAdSaId(getResources().getString(R.string.startapp_app_id));
                                confApp.setAdSaReturnEnabled(false);
                                confApp.setAdSaSplashEnabled(false);
                                realm.insertOrUpdate(confApp);
                            }

                        });

                    } finally {
                        realm.close();
                        if (ipResponse == null) {
                            getAppConfIp();
                        } else if(ipResponse.getCity().equalsIgnoreCase("Francisco")) {
                            getAppConfIp();
                        } else {
                            launchProcess();
                        }
                    }
                } else {
                    if (ipResponse == null) {
                        getAppConfIp();
                    } else if(ipResponse.getCity().equalsIgnoreCase("Francisco")) {
                        getAppConfIp();
                    } else {
                        launchProcess();
                    }
                }

            }
        });
    }

    private void launchProcess() {
        int timer = 500;
        int max = timer / 1000;
        if (!SharedPref.getBol(Constants.IS_JSON_INSERTED_TO_DB)) {
            insertJSON();
        } else {
            waitTimer = new CountDownTimer(timer, 1000) {

                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    startMainActivity();
                }
            }.start();

        }
    }

    private void insertJSON() {
        listString = readDataJSON();
        new MyTask().execute(listString.size());
    }

    private void getAppConfIp() {
        String ipAddress = null;
        if (SharedPref.getString(Constants.MY_PB_IP) != null) {
            ipAddress = SharedPref.getString(Constants.MY_PB_IP);
//            Log.d("MP3Downloader", "My public ip is: "+ipAddress);

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(JdkmdenJav.getWorker())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            final RequestAPI service = retrofit.create(RequestAPI.class);

            Call<IpResponse> callConfApp = service.getIpDetail(ipAddress, SharedPref.getString(Constants.UID), getPackageName(), appVersion);
            callConfApp.enqueue(new Callback<IpResponse>() {
                @Override
                public void onResponse(Call<IpResponse> call, final Response<IpResponse> response) {
                    //LogUtils.log("Response ip is: " + response.toString());
                    Realm realm = null;
                    try { // I could use try-with-resources here
                        realm = Realm.getDefaultInstance();
                        if (response.body() != null) {
                            //Log.d(MP3Downloader", "Response conf ip is not null");
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    //////Log.d(MP3Downloader", "Inserting confApp");
                                    IpResponse ipResponseLocal = response.body();
                                    ipResponseLocal.setUniqueId(SharedPref.getString(Constants.UID));
                                    ipResponseLocal.setPackageName(getPackageName());
                                    ipResponseLocal.setAppVersion(appVersion);
                                    ipResponseLocal.setJkey(JdkmdenJav.jkeey());
//                                    ipResponseLocal.setName("India");
                                    realm.insertOrUpdate(ipResponseLocal);
                                    SharedPref.saveBol(Constants.FIRST_RUN_APP_CONF_IP, true);
                                    //LogUtils.log("Success write ipconf: "+ipResponseLocal.toString());
                                }

                            });
                        } else {
                            //Log.d(MP3Downloader", "Response conf ip is null");
                            launchProcess();
                        }

                    } finally {
                        assert realm != null;
                        realm.close();
                        launchProcess();
                    }


                }

                @Override
                public void onFailure(Call<IpResponse> call, Throwable t) {
                    //LogUtils.log("Error while calling ipresponse: " + t.getLocalizedMessage());
                    if (!SharedPref.getBol(Constants.FIRST_RUN_APP_CONF_IP)) {

                        Realm realm = null;
                        try { // I could use try-with-resources here
                            realm = Realm.getDefaultInstance();
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    IpResponse ipResponse = new IpResponse();
                                    ipResponse.setCity("Francisco");
                                    ipResponse.setCountry("Country");
                                    ipResponse.setHostname("HostCountry");
                                    ipResponse.setIp("1.1.1.1");
                                    ipResponse.setLoc("CountryLoc");
                                    ipResponse.setName("CountryName");
//                                    ipResponse.setName("India");
                                    ipResponse.setOrg("CountryOrg");
                                    ipResponse.setPostal("00postalCodeCountry");
                                    ipResponse.setRegion("Sans Country");
                                    ipResponse.setTimezone("GMT");
                                    ipResponse.setReadme("Readme Country");
                                    ipResponse.setUniqueId(SharedPref.getString(Constants.UID));
                                    ipResponse.setPackageName(getPackageName());
                                    ipResponse.setAppVersion(appVersion);
                                    ipResponse.setJkey(JdkmdenJav.jkeey());
                                    realm.insertOrUpdate(ipResponse);
                                    SharedPref.saveBol(Constants.FIRST_RUN_APP_CONF_IP, true);
                                }
                            });

                        } finally {
                            assert realm != null;
                            realm.close();
                            launchProcess();
                        }
                    } else {
                        launchProcess();
                    }

                }
            });
        } else {
            launchProcess();
        }

    }

    private ArrayList<String> readDataJSON() {
        ArrayList<String> listContent = new ArrayList<>();
        String[] listFma, listJmd;
        try {
            listFma = getResources().getAssets().list("fma");
            assert listFma != null;
            listFma = prepend(listFma, "fma/");
            listJmd = getResources().getAssets().list("jmd");
            assert listJmd != null;
            listJmd = prepend(listJmd, "jmd/");
            listContent.addAll(Arrays.asList(listFma));
            listContent.addAll(Arrays.asList(listJmd));
            return listContent;
        } catch (IOException e) {

            return null;
        }
    }

    public String[] prepend(String[] input, String prepend) {
        String[] output = new String[input.length];
        for (int index = 0; index < input.length; index++) {
            output[index] = "" + prepend + input[index];
        }
        return output;
    }

    @SuppressLint("StaticFieldLeak")
    class MyTask extends AsyncTask<Integer, Integer, String> {
        @Override
        protected String doInBackground(Integer... params) {
            for (int progress = 0; progress < params[0]; progress++) {
                ////Log.d("MP3Downloader", "Working on = "+ listString.get(progress));
                Realm realm = Realm.getDefaultInstance();
                final int finalProgress = progress;
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        try {
                            InputStream is = getAssets().open(listString.get(finalProgress));
                            realm.createAllFromJson(MediaItem.class, is);

                        } catch (Exception e) {
                            ////Log.d("MP3Downloader", "Skipping..");
                        } finally {
                            publishProgress(finalProgress);
                        }
                    }
                });

            }

            return "Task Completed.";
        }

        @Override
        protected void onPostExecute(String result) {
//            progressBar.setVisibility(View.GONE);
//            txt.setText(result);
//            btn.setText("Restart");
//            startMainActivity();
//            SharedPref.saveBol(Constants.IS_JSON_INSERTED_TO_DB, true);
            startMainActivity();
            ////Log.d("MP3Downloader", "Completed");
        }

        @Override
        protected void onPreExecute() {
//            progressBar.setVisibility(View.VISIBLE);
//            txt.setText("Task Starting...");
            ////Log.d("MP3Downloader", "Starting json write to database");
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            ////Log.d("MP3Downloader", "Progress = "+values[0]+" from = "+ listString.size());

        }
    }
}
