package com.freemusic.download.mp3juice.ext;

public class ExtractorException extends Exception {
    public ExtractorException(String msg) {
        super(msg);
    }
}
