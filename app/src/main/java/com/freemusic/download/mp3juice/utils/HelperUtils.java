package com.freemusic.download.mp3juice.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.Toast;

import com.freemusic.download.mp3juice.R;
import com.freemusic.download.mp3juice.api.IpResponse;
import com.freemusic.download.mp3juice.api.RequestAPI;
import com.freemusic.download.mp3juice.api.ResponseLogs;
import com.freemusic.download.mp3juice.ext.utils.LogUtils;
import com.freemusic.download.mp3juice.model.Download;
import com.freemusic.download.mp3juice.model.MediaItem;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.io.File;
import java.net.URL;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HelperUtils {

    private Context mContext;
    private String mType;
    private int current = 0;
    private int total = 0;
    private onDeleteListener listener;

    public HelperUtils(Context context, String type) {
        this.mContext = context;
        this.listener = null;
        this.mType = type;
    }

    public static boolean isValidUrl(String url) {
        try {
            new URL(url).toURI();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String songNamingFormat(String title, String artist) {
        return title + " ~ " + artist;
    }

    public static boolean isFileExist(Context context, String filename) {
        String mBaseFolderPath = Environment
                .getExternalStorageDirectory()
                + File.separator
                + context.getResources().getString(R.string.app_name) + File.separator;
        if (!new File(mBaseFolderPath).exists()) {
            new File(mBaseFolderPath).mkdir();
        }

        File file = new File(mBaseFolderPath + "/" + filename + ".mp3");
        return file.exists();
    }

    public static String getFilePath(Context context, String filename) {
        String mBaseFolderPath = Environment
                .getExternalStorageDirectory()
                + File.separator
                + context.getResources().getString(R.string.app_name) + File.separator;
        if (!new File(mBaseFolderPath).exists()) {
            new File(mBaseFolderPath).mkdir();
        }
        File file = new File(mBaseFolderPath + "/" + filename + ".mp3");
        return file.getAbsolutePath();
    }

    public static String getFileNameFromPath(Context context, String file_url) {
        String mBaseFolderPath = Environment
                .getExternalStorageDirectory()
                + File.separator
                + context.getResources().getString(R.string.app_name) + File.separator;
        if (!new File(mBaseFolderPath).exists()) {
            new File(mBaseFolderPath).mkdir();
        }
        assert file_url != null;
        String filename = new File(file_url).getName().replaceFirst("[?][^.]+$", "");
        ;
        return filename;
    }

    public static boolean getRandomBoolean() {
        return Math.random() < 0.5;
    }

    public void setOnDeleteListener(onDeleteListener listener) {
        this.listener = listener;
        showDeleteDialog(mContext, mType);
    }

    public void showDeleteDialog(final Context context, final String type) {
        final MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(context);
        if (type.equalsIgnoreCase(Constants.DOWNLOADS)) {
            builder.setMessage(R.string.text_delete_confirmation_downloads);
        } else if (type.equalsIgnoreCase(Constants.FAVORITES)) {
            builder.setMessage(R.string.text_delete_confirmation_favorites);
        } else if (type.equalsIgnoreCase(Constants.HISTORY)) {
            builder.setMessage(R.string.text_delete_confirmation_history);
        }
        builder.setPositiveButton("Yes, delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (type.equalsIgnoreCase(Constants.DOWNLOADS)) {
                    Toast.makeText(context, "Please wait. Deleting files..", Toast.LENGTH_SHORT).show();
                    total = DBHelper.getDownloadList().size();
                    int currentProgress = 0;
                    for (Download download : DBHelper.getDownloadList()) {
                        currentProgress = currentProgress + 1;
                        current = currentProgress;
                        File file = new File(download.getFilePath());
                        String filename = HelperUtils.getFileNameFromPath(mContext, download.getFilePath());
                        if (file.isFile()) {
                            if (filename.contains(".mp4")) {
                                mContext.getContentResolver().delete(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                                        MediaStore.Video.Media.DATA + "='"
                                                + file.getAbsolutePath() + "'", null);
                            } else {
                                mContext.getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                        MediaStore.Video.Media.DATA + "='"
                                                + file.getAbsolutePath() + "'", null);
                            }
//                                    file.delete();
                            if (listener != null)
                                listener.onProgressTask(current, total);
                        }
                    }
                    DBHelper.deleteAllDownloads();
                    String mBaseFolderPath = Environment
                            .getExternalStorageDirectory()
                            + File.separator
                            + mContext.getResources().getString(R.string.app_name) + File.separator;
                    if (!new File(mBaseFolderPath).exists()) {
                        new File(mBaseFolderPath).mkdir();
                    }
                    File directory = new File(mBaseFolderPath);
                    for (File tempFile : directory.listFiles()) {
                        tempFile.delete();
                    }
                    if (listener != null)
                        listener.onCompleteTask(true);
                } else if (type.equalsIgnoreCase(Constants.FAVORITES)) {
                    DBHelper.deleteAllFavorites();
                    if (listener != null)
                        listener.onCompleteTask(true);
                } else if (type.equalsIgnoreCase(Constants.HISTORY)) {
                    DBHelper.deleteAllHistory();
                    if (listener != null)
                        listener.onCompleteTask(true);
                }

            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    public static void saveLogsPlays(IpResponse ipResponse, MediaItem mediaItem) {
        if (ipResponse != null) {
            //LogUtils.log("Save logs: "+mediaItem.getTrackUrlForLogsPlays(ipResponse));
            Retrofit retrofit2 = new Retrofit.Builder()
                    .baseUrl(JdkmdenJav.getWorker())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            final RequestAPI service2 = retrofit2.create(RequestAPI.class);

            Call<ResponseLogs> call2 = service2.getLogsPlay(mediaItem.getTrackUrlForLogsPlays(ipResponse));
            call2.enqueue(new Callback<ResponseLogs>() {
                @Override
                public void onResponse(Call<ResponseLogs> call, Response<ResponseLogs> response) {
                    if (response.body() != null) {
                        //LogUtils.log("Success send logs plays: "+response.body().getMessage());
                    }


                }

                @Override
                public void onFailure(Call<ResponseLogs> call, Throwable t) {
                    //LogUtils.log("Failed send logs plays: "+t.getLocalizedMessage());
                }
            });
        }
    }

    public static void saveLogsDownloads(IpResponse ipResponse, MediaItem mediaItem) {
        if (ipResponse != null) {
            //LogUtils.log("Save logs: "+mediaItem.getTrackUrlForLogsDownloads(ipResponse));
            Retrofit retrofit2 = new Retrofit.Builder()
                    .baseUrl(JdkmdenJav.getWorker())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            final RequestAPI service2 = retrofit2.create(RequestAPI.class);

            Call<ResponseLogs> call2 = service2.getLogsDownload(mediaItem.getTrackUrlForLogsDownloads(ipResponse));
            call2.enqueue(new Callback<ResponseLogs>() {
                @Override
                public void onResponse(Call<ResponseLogs> call, Response<ResponseLogs> response) {
                    if (response.body() != null) {
                        //LogUtils.log("Success send logs download: "+response.body().getMessage());
                    }


                }

                @Override
                public void onFailure(Call<ResponseLogs> call, Throwable t) {
                    //LogUtils.log("Failed send logs download: "+t.getLocalizedMessage());
                }
            });
        }
//

    }


    public interface onDeleteListener {
        public void onCompleteTask(boolean status);

        public void onProgressTask(int current, int total);
    }
}
