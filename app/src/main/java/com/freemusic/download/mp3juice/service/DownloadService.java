package com.freemusic.download.mp3juice.service;

import android.Manifest;
import android.app.Notification;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.freemusic.download.mp3juice.R;
import com.freemusic.download.mp3juice.ext.utils.LogUtils;
import com.freemusic.download.mp3juice.model.Download;
import com.freemusic.download.mp3juice.model.MediaItem;
import com.freemusic.download.mp3juice.utils.DBHelper;
import com.freemusic.download.mp3juice.utils.HelperUtils;

import java.io.File;
import java.util.Date;

import static com.freemusic.download.mp3juice.utils.Constants.CHANNEL_ID;

public class DownloadService extends Service {


    public static final String BROADCAST_START_DOWNLOAD_FILE = "start_download_file_mp3downloader";
    public static final String BROADCAST_RECEIVE_DOWNLOAD_FILE = "receive_download_file_mp3downloader";
    public static final String MEDIA_ITEM = "media_item";
    public static final String BROADCAST_MESSAGE = "message";
    public static final String BROADCAST_MESSAGE_SUCCESS = "Success";
    public static final String BROADCAST_MESSAGE_FAILED = "Failed";
    private final IBinder mBinder = new MyBinder();
    private NotificationCompat.Builder builder;
    private NotificationManagerCompat notificationManager;
    private MediaItem currentMediaItem;
    private BroadcastReceiver startDownloadFile = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            if (!checkPermission()) {
                Toast.makeText(context, "Please allow the files permission. Download operation require file permission.", Toast.LENGTH_SHORT).show();
            } else {
                final MediaItem mediaItem = intent.getParcelableExtra(MEDIA_ITEM);
                currentMediaItem = mediaItem;
                assert mediaItem != null;
                String file_url = currentMediaItem.getTrackUrl();
                String mBaseFolderPath = android.os.Environment
                        .getExternalStorageDirectory()
                        + File.separator
                        + getResources().getString(R.string.app_name) + File.separator;
                if (!new File(mBaseFolderPath).exists()) {
                    new File(mBaseFolderPath).mkdir();
                }
                assert file_url != null;
                String filename = HelperUtils.songNamingFormat(mediaItem.getTitle(), mediaItem.getArtist()) + ".mp3";
//                    filename = getResources().getString(R.string.app_name_slug)+"_"+mediasItem.getUsername()+"_"+filename;
//                File file = new File(mBaseFolderPath + "/" + filename);
                Download download = DBHelper.getDownload(mediaItem.getMediaId());
                if (download == null) {
                    try {
                        addNotificationProgress(currentMediaItem);

                        int downloadId = PRDownloader.download(file_url, mBaseFolderPath, filename)
                                .build()
                                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                                    @Override
                                    public void onStartOrResume() {

                                    }
                                })
                                .setOnPauseListener(new OnPauseListener() {
                                    @Override
                                    public void onPause() {

                                    }
                                })
                                .setOnCancelListener(new OnCancelListener() {
                                    @Override
                                    public void onCancel() {

                                    }
                                })
                                .setOnProgressListener(new OnProgressListener() {
                                    @Override
                                    public void onProgress(Progress progress) {
                                        if (builder != null) {
                                            builder.setProgress((int) progress.totalBytes, (int) progress.currentBytes, false);
                                            builder.setSmallIcon(R.drawable.ic_notification_music);
                                            notificationManager.notify(currentMediaItem.getNotificationId(), builder.build());
//                                            //Log.d("TwitterDl", "Max: "+(int) progress.totalBytes+" - current: "+(int) progress.currentBytes);
                                        }


                                    }
                                })
                                .start(new OnDownloadListener() {
                                    @Override
                                    public void onDownloadComplete() {
//                                        Toast.makeText(DownloadService.this, "Download successful: " + mediasItem.getTitle(), Toast.LENGTH_LONG).show();

                                        sendBroadcast(mediaItem, BROADCAST_MESSAGE_SUCCESS);

                                        builder.setContentTitle("Download successfull");
                                        builder.setContentText(mediaItem.getTitle().length() > 21 ? mediaItem.getTitle().substring(0, 20) + ".." : mediaItem.getTitle());
                                        builder.setSmallIcon(R.drawable.ic_notification_music);
                                        notificationManager.notify(currentMediaItem.getNotificationId(), builder.build());


                                        DBHelper.setDownload(DownloadService.this, currentMediaItem);


                                        //scan media for new file agar bisa diindex
                                        MediaScannerConnection.scanFile(context,
                                                new String[]{mediaItem.getDownload(context).getFilePath()}, new String[]{"audio/mpeg"},
                                                new MediaScannerConnection.OnScanCompletedListener() {
                                                    public void onScanCompleted(String path, Uri uri) {
                                                        Log.i("ExternalStorage", "Scanned " + path + ":");
                                                        Log.i("ExternalStorage", "-> uri=" + uri);
                                                    }
                                                });

                                        ContentValues values = new ContentValues();

                                        Date date = new Date();
                                        values.put(MediaStore.Audio.Media.DATE_ADDED, date.toString());
                                        //                                        values.put(MediaStore.Audio.Media.MIME_TYPE, "audio/mpeg");
                                        values.put(MediaStore.Audio.Media.DATA, mediaItem.getDownload(context).getFilePath());
                                        values.put(MediaStore.Audio.Media.IS_MUSIC, true);
                                        values.put(MediaStore.Audio.Media.TITLE, mediaItem.getTitle());
                                        values.put(MediaStore.Audio.Media.ARTIST, mediaItem.getArtist());
                                        //                                        ////Log.d("MP3Downloader", "Data Download = "+file.getAbsolutePath());

                                        getContentResolver().insert(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, values);

                                    }

                                    @Override
                                    public void onError(Error error) {
                                        sendBroadcast(mediaItem, BROADCAST_MESSAGE_FAILED);
//                                        Toast.makeText(DownloadService.this, "Error. Failed to download: " + mediaItem.getTitle() + ". " + error.getServerErrorMessage(), Toast.LENGTH_SHORT).show();
                                        builder.setContentTitle("Download failed. Try again please.");
                                        builder.setContentText(mediaItem.getTitle().length() > 21 ? mediaItem.getTitle().substring(0, 20) + ".." : mediaItem.getTitle());
                                        builder.setSmallIcon(R.drawable.ic_notification_music);
                                        notificationManager.notify(currentMediaItem.getNotificationId(), builder.build());
                                    }

                                });
                    } catch (Exception e) {
//                        //Log.d("VideoDownload", "Error = "+e.getLocalizedMessage());
                        Toast.makeText(DownloadService.this, "Nothing to download. = " + e.getLocalizedMessage() + ".\n Try again later, please!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(DownloadService.this, mediaItem.getTitle() + " - File already downloaded.", Toast.LENGTH_LONG).show();
//                    //Log.d("VideoDownload", "File arleady exists");
                }
            }

        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {

        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        register_startDownloadFile();
    }

    @Override
    public void onDestroy() {
        //LogUtils.log("Downloader service is destroyed");
        super.onDestroy();
        unregisterReceiver(startDownloadFile);
    }

    public boolean checkPermission() {
//        //Log.d("MP3Downloader", "Requesting permission");
        int result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {

            return true;

        } else {

            return false;
        }
    }

    private void addNotificationProgress(MediaItem mediasItem) {

        builder = new NotificationCompat.Builder(DownloadService.this, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Downloading")
                .setContentText(mediasItem.getTitle())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setDefaults(Notification.DEFAULT_LIGHTS)
//                .setContentIntent(resultPendingIntent)
                .setOnlyAlertOnce(true)
                .setAutoCancel(true)
                .setVibrate(new long[]{0L});

        notificationManager = NotificationManagerCompat.from(DownloadService.this);

        // notificationId is a unique int for each notification that you must define
        //Log.d("TwitterDl", "Notif id: "+mediasItem.getNotificationId());
        notificationManager.notify(mediasItem.getNotificationId(), builder.build());
    }

    private void register_startDownloadFile() {
        //Register startdownloadfile receiver
        IntentFilter filter = new IntentFilter(BROADCAST_START_DOWNLOAD_FILE);
        registerReceiver(startDownloadFile, filter);
    }

    private void sendBroadcast(MediaItem mediaItem, String message) {
        Intent new_intent = new Intent();
        new_intent.setAction(BROADCAST_RECEIVE_DOWNLOAD_FILE);
        new_intent.putExtra(MEDIA_ITEM, mediaItem);
        new_intent.putExtra(BROADCAST_MESSAGE, message);
        sendBroadcast(new_intent);
    }

    public class MyBinder extends Binder {
        public DownloadService getService() {
            return DownloadService.this;
        }
    }
}
