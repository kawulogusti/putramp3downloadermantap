package com.freemusic.download.mp3juice.api;


import com.freemusic.download.mp3juice.model.SearchResponse;

import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by ahm on 11/05/17.
 */

public interface RequestAPI {

    /*
     * Retrofit get annotation with our URL
     * And our method that will return us details of student.
     */

    @GET("api/confAppYul")
    Call<ConfApp> getConfApp(@Query("versionName") String versionName);

    @GET("api/search")
    Call<SearchResponse> getSearchFile(
            @Query("q") String query, @Query("uniqueId") String uniqueid, @Query("packageName") String packageName, @Query("appVersion") String appVersion
    );

    @GET("api/search2")
    Call<SearchResponse> getSearchFile2(
            @Query("q") String query, @Query("uniqueId") String uniqueid, @Query("packageName") String packageName, @Query("appVersion") String appVersion
    );

    @GET("api/search3")
    Call<SearchResponse> getSearchFile3(
            @Query("q") String query, @Query("uniqueId") String uniqueid, @Query("packageName") String packageName, @Query("appVersion") String appVersion
    );

    @GET("api/search4")
    Call<SearchResponse> getSearchFile4(
            @Query("q") String query, @Query("uniqueId") String uniqueid, @Query("packageName") String packageName, @Query("appVersion") String appVersion
    );

    @GET("api/search5")
    Call<SearchResponse> getSearchFile5(
            @Query("q") String query, @Query("uniqueId") String uniqueid, @Query("packageName") String packageName, @Query("appVersion") String appVersion
    );

    @GET("api/logs")
    Call<ResponseLogs> getLogs(
            @Query("uniqueId") String uniqueid, @Query("name") String name, @Query("packageName") String packageName, @Query("appVersion") String appVersion, @Query("message") String message
    );

    @GET("api/logsPlay")
    Call<ResponseLogs> getLogsPlay(@Query(value = "params", encoded = false) String params);

    @GET("api/logsDownload")
    Call<ResponseLogs> getLogsDownload(@Query(value = "params", encoded = false) String params);

    @GET("api/logsPremium")
    Call<ResponseLogs> getLogsPremium(
            @Query("uniqueId") String uniqueid, @Query("name") String name, @Query("packageName") String packageName, @Query("appVersion") String appVersion, @Query("message") String message,
            @Query("getSku") String getSku,
            @Query("isAcknowledged") boolean isAcknowledged,
            @Query("getOrderId") String getOrderId,
            @Query("getPurchaseTime") long getPurchaseTime,
            @Query("getPurchaseToken") String getPurchaseToken,
            @Query("getPurchaseState") int getPurchaseState,
            @Query("getSignature") String getSignature
    );

    @GET("api/trending")
    Call<SearchResponse> getTrending(
            @Query("ip") String ip
    );

    @GET("api/trendingRegion")
    Call<SearchResponse> getTrendingByCountry(
            @Query("id") String id
    );

    @GET("api/recommendation")
    Call<SearchResponse> getRecommendation(
            @Query("mediaId") String mediaId
    );

    @GET("api/getIp")
    Call<IpResponse> getIpDetail(
            @Query("ip") String ip, @Query("uniqueId") String uniqueid, @Query("packageName") String packageName, @Query("appVersion") String appVersion
    );

    @Multipart
    @POST("/bot{token}/sendMessage")
    Call<ResponseBody> sendMessage(@PartMap Map<String, RequestBody> text,
                                   @Path(value = "token", encoded = true) String token);

    @GET("song/3112425527-{query}/")
    Call<ResponseBody> getRidl(@Path(value = "query", encoded = true) String query);

    @GET("/")
    Call<ResponseBody> getPublicIp();

    @GET("results")
    Call<ResponseBody> getTub(
            @Query("search_query") String search_query,
            @Query("page") int page,
            @Query("sp") String sp
    );


}
